'''
Created on Apr 30, 2020

@author: consultit
'''

from enum import Enum, unique, auto
import re
from .bvhHierarchy import BVHHierarchy, Root, Joint, EndSite, ChannelType, \
    Skeleton, Animation

'''
SYNTAX DIRECTED TRANSLATION.
GRAMMAR:

bvh -> zeromorenl hierarchy motion
hierarchy -> HIERARCHY onemorenl rootdescrseq
rootdescrseq -> rootdescr rootdescrseq | EMPTY
rootdescr -> ROOT ID onemorenl LCURL onemorenl offsetdescr channelsdescr jointdescrseq RCURL onemorenl
offsetdescr -> OFFSET floatseq onemorenl
channeldecr -> CHANNELS INT posrotdescr onemorenl
posrotdescr -> posdescr posrotdescr | rotdescr posrotdescr | EMPTY
posdescr -> xyzpos moreposdescr
moreposdescr -> xyzpos moreposdescr | EMPTY
xyzpos -> XPOS | YPOS | ZPOS
rotdescr -> xyzrot morerotdescr
morerotdescr -> xyzrot morerotdescr | EMPTY
xyzrot -> XROT | YROT | ZROT
jointdescrseq -> jointdescr jointdescrseq | EMPTY
jointdescr -> JOINT ID onemorenl LCURL onemorenl offsetdescr channeldecr jointnesteddescr RCURL onemorenl
jointnesteddescr -> jointdescrseq | END SITE onemorenl LCURL onemorenl offsetdescr RCURL onemorenl
motion -> motionheader frameseq
motionheader -> MOTION onemorenl FRAMES COLON INT onemorenl FRAME TIME COLON FLOAT onemorenl
frameseq -> frame frameseq | EMPTY
frame -> floatseq frameend
frameend -> onemorenl | EOF
floatseq -> FLOAT floatseq | INT floatseq | EMPTY
onemorenl -> NL zeromorenl
zeromorenl -> NL zeromorenl | EMPTY
'''


@unique
class Tag(Enum):
    EOF = ''  # reserverd word: End-Of-File
    NL = '\n'  # reserverd word: new line
    EMPTY = auto()  # empty production
    ID = auto()  # identifier
    HIERARCHY = 'HIERARCHY'  # reserverd word: 'HIERARCHY'
    ROOT = 'ROOT'  # reserverd word: 'ROOT'
    LCURL = '{'  # reserverd word: '{'
    RCURL = '}'  # reserverd word: '}'
    OFFSET = 'OFFSET'  # reserverd word: 'OFFSET'
    FLOAT = auto()  # float
    INT = auto()  # int
    CHANNELS = 'CHANNELS'  # reserverd word: 'CHANNELS'
    XPOS = 'Xposition'  # reserverd word: 'Xposition'
    YPOS = 'Yposition'  # reserverd word: 'Yposition'
    ZPOS = 'Zposition'  # reserverd word: 'Zposition'
    XROT = 'Xrotation'  # reserverd word: 'Xrotation'
    YROT = 'Yrotation'  # reserverd word: 'Yrotation'
    ZROT = 'Zrotation'  # reserverd word: 'Zrotation'
    JOINT = 'JOINT'  # reserverd word: 'JOINT'
    END = 'End'  # reserverd word: 'End'
    SITE = 'Site'  # reserverd word: 'Site'
    MOTION = 'MOTION'  # reserverd word: 'MOTION'
    FRAMES = 'Frames'  # reserverd word: 'Frames'
    COLON = ':'  # reserverd word: ':'
    FRAME = 'Frame'  # reserverd word: 'Frame'
    TIME = 'Time'  # reserverd word: 'Time'


class Token(object):
    def __init__(self, tag):
        self._tag = tag
        self._data = None

    @property
    def tag(self):
        return self._tag

    def __str__(self):
        return '({}, {!r})'.format(self._tag, self._data)

    def __hash__(self):
        return hash(str(self._data) + str(self._tag))

    def __eq__(self, other):
        return (self._data == other._data) and (self._tag == other._tag)


class Float(Token):
    def __init__(self, value):
        super().__init__(Tag.FLOAT)
        self._data = value

    @property
    def value(self):
        return self._data


class Int(Token):
    def __init__(self, value):
        super().__init__(Tag.INT)
        self._data = value

    @property
    def value(self):
        return self._data


class Word(Token):
    def __init__(self, tag, lexeme):
        super().__init__(tag)
        self._data = lexeme

    @property
    def lexeme(self):
        return self._data


class SimpleLexer(object):

    def __init__(self, charStream, tagEnumClass, raiseOnError=False):
        self._charStream = charStream
        self._raiseOnError = raiseOnError
        self._wordTable = dict()
        # preload reserverd words
        if issubclass(tagEnumClass, Tag):
            for tag in tagEnumClass:
                if isinstance(tag.value, str):
                    # reserverd word found
                    self._wordTable[tag.value] = Word(tag, tag.value)
        # precompile regexp
        self._blanks = ' \t'
        # reserved punctuation words must be treated especially: they can be
        # contiguous to other words
        self._punctuations = (
            Tag.NL.value + Tag.LCURL.value + Tag.RCURL.value + Tag.COLON.value)
        self._intRe = re.compile('^[-+]?[0-9]+$')
        self._floatRe = re.compile(
            '^([-+]?[0-9]+)?\.[0-9]+([eE][-+]?[0-9]+)?$')
        self._idRe = re.compile('^[a-zA-Z_][a-zA-Z0-9_]*$')
        self._lineNum = 1

    def scan(self):
        for word in self._wordStream():
            # check type of converted word: use regexp
            if self._intRe.fullmatch(word):
                # word is an int
                token = Int(int(word))
                yield token
            elif self._floatRe.fullmatch(word):
                # word is an float
                token = Float(float(word))
                yield token
            elif self._wordTable.get(word, None):
                # word is in table: identifier already recognized
                # or reserverd word
                token = self._wordTable[word]
                if token.tag == Tag.NL:
                    self._lineNum += 1
                yield token
            elif self._idRe.fullmatch(word):
                # word is a new identifier: insert new word in table
                token = Word(Tag.ID, word)
                self._wordTable[word] = token
                yield token
            else:
                # no previous match: error
                msg = 'Lexical Error: this lexeme \'' + word + '\' doesn\'t match any token.'
                self._error(msg)

    @property
    def wordTable(self):
        return self._wordTable

    @property
    def lineNum(self):
        return self._lineNum

    def _wordStream(self):
        '''Returns words or  empty string on EOF'''

        word = ''
        for ch in self._charStream:
            if ch in self._blanks:
                if not word:
                    # skip blank prefix
                    continue
                else:
                    # encountered first blank after a word
                    yield word
                    # reset word
                    word = ''
            elif ch in self._punctuations:
                if word:
                    # recognized word: return it first
                    yield word
                    word = ''
                # return the reserved punctuation
                yield ch
            else:
                # recognizing a word
                word += ch
        # executed when returning last word or EOF (empty string)
        yield word

    def _error(self, msg):
        msg += msg + ' - (line: ' + str(self._lexer.lineNum) + ')'
        if self._raiseOnError:
            raise ValueError(msg)
        else:
            print(msg)


SymbolTable = dict


class Env(object):
    '''
    Implements chained symbol tables.
    '''

    def __init__(self, prev):
        self._symbolTable = SymbolTable()
        self._prev = prev

    def putSymbol(self, string, symbol):
        self._symbolTable[string] = symbol

    def getSymbol(self, string):
        for env in self._walkChain():
            symbol = env._symbolTable.get(string, None)
            if symbol:
                return symbol
        return None

    def _walkChain(self):
        curr = self
        while curr:
            yield curr
            curr = curr._prev

    @property
    def symbolTable(self):
        return self._symbolTable

    @property
    def prev(self):
        return self._prev


class BVHParser(object):
    '''
    BVHParser
    '''

    def __init__(self, bvhFile, tagEnumClass=Tag, raiseOnError=False):
        '''
        Constructor
        '''

        with open(bvhFile) as f:
            charStream = f.read()
        self._raiseOnError = raiseOnError
        self._lexer = SimpleLexer(charStream, tagEnumClass, self._raiseOnError)
        self._tokenIter = self._lexer.scan()
        self._lookahead = next(self._tokenIter)
        self._symbolTable = None  # not used
        self._floats = None
        self._channels = None
        # auxiliary variables
        self._hierarchy = None
        self._currSkeleton = None
        self._currParent = None
        self._frameIdx = 0
        self._errContext = None

    @property
    def tokenIter(self):
        return self._tokenIter

    @property
    def bhvHierarchy(self):
        return self._hierarchy

    def parse(self):
        self.bvh()
        return self._hierarchy

    def bvh(self):
        '''zeromorenl hierarchy motion'''
        self.zeromorenl()
        self.hierarchy()
        self.motion()

    def hierarchy(self):
        '''HIERARCHY onemorenl rootdescrseq'''
        self._match(Tag.HIERARCHY)
        #{
        self._hierarchy = BVHHierarchy()
        #}
        self._match(Tag.NL)
        self.rootdescrseq()

    def rootdescrseq(self):
        '''rootdescr rootdescrseq | EMPTY'''
        if self._lookahead.tag == Tag.ROOT:
            self.rootdescr()
            self.rootdescrseq()
        else:
            pass

    def rootdescr(self):
        '''ROOT ID onemorenl LCURL onemorenl offsetdescr channelsdescr jointdescrseq RCURL onemorenl'''
        self._match(Tag.ROOT)
        #{
        rootName = self._lookahead.lexeme
        #}
        self._match(Tag.ID)
        #{
        self._hierarchy.addSkeleton(Skeleton(Root(rootName)))
        self._currSkeleton = self._hierarchy.getSkeleton(rootName)
        self._errContext = self._hierarchy.getSkeleton(rootName).root
        #}
        self.onemorenl()
        self._match(Tag.LCURL)
        self.onemorenl()
        #{
        self._floats = []
        #}
        self.offsetdescr()
        #{
        self._hierarchy.getSkeleton(rootName).root.offset.extend(self._floats)
        #}
        #{
        self._channels = []
        #}
        self.channelsdescr()
        #{
        self._hierarchy.getSkeleton(
            rootName).root.channels.extend(self._channels)
        #}
        #{
        oldParent = self._currParent
        self._currParent = self._hierarchy.getSkeleton(rootName).root
        #}
        self.jointdescrseq()
        #{
        self._currParent = oldParent
        self._errContext = self._hierarchy.getSkeleton(rootName).root
        #}
        self._match(Tag.RCURL)
        self.onemorenl()

    def offsetdescr(self):
        '''OFFSET floatseq onemorenl'''
        self._match(Tag.OFFSET)
        self.floatseq()
        self.onemorenl()

    def channelsdescr(self):
        '''CHANNELS INT posrotdescr onemorenl'''
        self._match(Tag.CHANNELS)
        #{
        lookahead = self._lookahead
        #}
        self._match(Tag.INT)
        #{
        if lookahead.value < 0:
            msg = 'Hierarchy Error: the number of declared channels is negative (' + \
                str(lookahead.value) + ').'
            self._error(msg, goOn=False)
        numDeclaredChannels = lookahead.value
        #}
        self.posrotdescr()
        #{
        numChannels = len(self._channels)
        if numDeclaredChannels != numChannels:
            msg = 'Hierarchy Error: number of declared channels (' + str(
                numDeclaredChannels) + ') are different from current ones (' + str(numChannels) + ')'
            self._error(msg, goOn=False)
        #}
        self.onemorenl()

    def posrotdescr(self):
        '''posdescr posrotdescr | rotdescr posrotdescr | EMPTY'''
        if ((self._lookahead.tag == Tag.XPOS) or
            (self._lookahead.tag == Tag.YPOS) or
                (self._lookahead.tag == Tag.ZPOS)):
            self.posdescr()
            self.posrotdescr()
        elif ((self._lookahead.tag == Tag.XROT) or
              (self._lookahead.tag == Tag.YROT) or
              (self._lookahead.tag == Tag.ZROT)):
            self.rotdescr()
            self.posrotdescr()
        else:
            pass

    def posdescr(self):
        '''xyzpos moreposdescr'''
        self.xyzpos()
        self.moreposdescr()

    def moreposdescr(self):
        '''xyzpos moreposdescr | EMPTY'''
        if ((self._lookahead.tag == Tag.XPOS) or
            (self._lookahead.tag == Tag.YPOS) or
                (self._lookahead.tag == Tag.ZPOS)):
            self.xyzpos()
            self.moreposdescr()
        else:
            pass

    def xyzpos(self):
        '''XPOS | YPOS | ZPOS'''
        if self._lookahead.tag == Tag.XPOS:
            #{
            self._channels.append(ChannelType.XPOS)
            #}
            self._match(Tag.XPOS)
        elif self._lookahead.tag == Tag.YPOS:
            #{
            self._channels.append(ChannelType.YPOS)
            #}
            self._match(Tag.YPOS)
        elif self._lookahead.tag == Tag.ZPOS:
            #{
            self._channels.append(ChannelType.ZPOS)
            #}
            self._match(Tag.ZPOS)

    def rotdescr(self):
        '''xyzrot morerotdescr'''
        self.xyzrot()
        self.morerotdescr()

    def morerotdescr(self):
        '''xyzrot morerotdescr | EMPTY'''
        if ((self._lookahead.tag == Tag.XROT) or
            (self._lookahead.tag == Tag.YROT) or
                (self._lookahead.tag == Tag.ZROT)):
            self.xyzrot()
            self.morerotdescr()
        else:
            pass

    def xyzrot(self):
        '''XROT | YROT | ZROT'''
        if self._lookahead.tag == Tag.XROT:
            #{
            self._channels.append(ChannelType.XROT)
            #}
            self._match(Tag.XROT)
        elif self._lookahead.tag == Tag.YROT:
            #{
            self._channels.append(ChannelType.YROT)
            #}
            self._match(Tag.YROT)
        elif self._lookahead.tag == Tag.ZROT:
            #{
            self._channels.append(ChannelType.ZROT)
            #}
            self._match(Tag.ZROT)

    def jointdescrseq(self):
        '''jointdescr jointdescrseq | EMPTY'''
        if ((self._lookahead.tag == Tag.JOINT) or
                (self._lookahead.tag == Tag.END)):
            self.jointdescr()
            self.jointdescrseq()
        else:
            pass

    def jointdescr(self):
        '''JOINT ID onemorenl LCURL onemorenl offsetdescr channeldecr jointnesteddescr RCURL onemorenl'''
        self._match(Tag.JOINT)
        #{
        jointName = self._lookahead.lexeme
        #}
        self._match(Tag.ID)
        #{
        self._currSkeleton.addJoint(Joint(jointName, self._currParent))
        self._errContext = self._currSkeleton.getJoint(jointName)
        #}
        self.onemorenl()
        self._match(Tag.LCURL)
        self.onemorenl()
        #{
        self._floats = []
        #}
        self.offsetdescr()
        #{
        self._currSkeleton.getJoint(jointName).offset.extend(self._floats)
        #}
        #{
        self._channels = []
        #}
        self.channelsdescr()
        #{
        self._currSkeleton.getJoint(
            jointName).channels.extend(self._channels)
        #}
        #{
        oldParent = self._currParent
        self._currParent = self._currSkeleton.getJoint(jointName)
        #}
        self.jointnesteddescr()
        #{
        self._currParent = oldParent
        self._errContext = self._currSkeleton.getJoint(jointName)
        #}
        self._match(Tag.RCURL)
        self.onemorenl()

    def jointnesteddescr(self):
        '''jointdescrseq | END SITE onemorenl LCURL onemorenl offsetdescr RCURL onemorenl'''
        if self._lookahead.tag == Tag.JOINT:
            self.jointdescrseq()
        elif self._lookahead.tag == Tag.END:
            self._match(Tag.END)
            self._match(Tag.SITE)
            #{
            endSiteName = 'EndSite_' + self._currParent.name
            self._currSkeleton.addJoint(EndSite(endSiteName, self._currParent))
            #}
            self.onemorenl()
            self._match(Tag.LCURL)
            self.onemorenl()
            #{
            self._floats = []
            #}
            self.offsetdescr()
            #{
            self._currSkeleton.getJoint(
                endSiteName).offset.extend(self._floats)
            #}
            self._match(Tag.RCURL)
            self.onemorenl()

    def motion(self):
        '''motionheader frameseq'''
        self.motionheader()
        #{
        for skel in self._hierarchy.skeletons.values():
            skel.animation = Animation(skel, self._hierarchy.frames)
        self._frameIdx = 0
        #}
        self.frameseq()
        #{
        if self._frameIdx < self._hierarchy.frames:
            msg = 'Motion Error: there are fewer (' + \
                str(self._frameIdx) + \
                ') frames than declared (' + str(self._hierarchy.frames) + ').'
            self._error(msg, goOn=False)
        #}

    def motionheader(self):
        '''MOTION onemorenl FRAMES COLON INT onemorenl FRAME TIME COLON FLOAT onemorenl'''
        self._match(Tag.MOTION)
        self.onemorenl()
        self._match(Tag.FRAMES)
        self._match(Tag.COLON)
        #{
        lookahead = self._lookahead
        #}
        self._match(Tag.INT)
        #{
        self._hierarchy.frames = lookahead.value
        #}
        self.onemorenl()
        self._match(Tag.FRAME)
        self._match(Tag.TIME)
        self._match(Tag.COLON)
        #{
        lookahead = self._lookahead
        #}
        self._match(Tag.FLOAT)
        #{
        self._hierarchy.frameTime = lookahead.value
        #}
        self.onemorenl()

    def frameseq(self):
        '''frame frameseq | EMPTY'''
        # This recursive method has been optimized in an iterative way.
        # For a function without parameters, a tail-recursive call can be
        # replaced simply by a jump to the beginning of the procedure.
        while True:
            if self._lookahead.tag == Tag.FLOAT:
                #{
                self._floats = []
                #}
                self.frame()
                #{
                self._frameIdx += 1
                #}
                continue  # self.frameseq()
            else:
                break  # pass

    def frame(self):
        '''floatseq frameend'''
        self.floatseq()
        self.frameend()
        #{
        floatIdx = 0
        for skel in self._hierarchy.skeletons.values():
            try:
                frame = skel.animation.frames[self._frameIdx]
            except IndexError:
                msg = 'Motion Error: there are more frames (' + \
                    str(self._frameIdx + 1) + \
                    ') than declared (' + str(self._hierarchy.frames) + ').'
                self._error(msg, goOn=False)
            numChannels = skel.numChannels
            for i in range(numChannels):
                try:
                    frame[i] = self._floats[floatIdx + i]
                except IndexError:
                    msg = 'Motion Error: at frame ' + \
                        str(self._frameIdx + 1) + \
                        ' the number of data does not correspond to the total number of channels of all skeletons.'
                    self._error(msg, goOn=False)
            floatIdx += numChannels
        #}

    def frameend(self):
        '''onemorenl | EOF'''
        if self._lookahead.tag == Tag.NL:
            self.onemorenl()
        elif self._lookahead.tag == Tag.EOF:
            self._match(Tag.EOF)

    def floatseq(self):
        '''FLOAT floatseq | INT floatseq | EMPTY'''
        # This recursive method has been optimized in an iterative way.
        # For a function without parameters, a tail-recursive call can be
        # replaced simply by a jump to the beginning of the procedure.
        while True:
            if self._lookahead.tag == Tag.FLOAT:
                #{
                self._floats.append(self._lookahead.value)
                #}
                self._match(Tag.FLOAT)
                continue  # self.floatseq()
            if self._lookahead.tag == Tag.INT:
                #{
                self._floats.append(self._lookahead.value)
                #}
                self._match(Tag.INT)
                continue  # self.floatseq()
            else:
                break  # pass

    def onemorenl(self):
        '''NL zeromorenl'''
        self._match(Tag.NL)
        self.zeromorenl()

    def zeromorenl(self):
        '''NL zeromorenl | EMPTY'''
        if self._lookahead.tag == Tag.NL:
            self._match(Tag.NL)
            self.zeromorenl()
        else:
            pass

    def _match(self, tokenTag):
        '''Matches tokenTag and advances token stream.'''
        if self._lookahead.tag == tokenTag:
            try:
                self._lookahead = next(self._tokenIter)
            except StopIteration:
                # we are at the end of token stream: don't advance
                return
        else:
            # no previous match: error
            msg = 'Syntax Error: this token \'' + \
                str(self._lookahead) + \
                '\' here doesn\'t match any grammar construct.'
            self._error(msg)

    def _error(self, msg, goOn=True):
        msg += msg + ' - (line: ' + str(self._lexer.lineNum) + ')'
        if self._errContext:
            msg += ' - ' + str(self._errContext)
        if self._raiseOnError:
            raise ValueError(msg)
        else:
            if goOn:
                self._lookahead = next(self._tokenIter)
            print(msg + '\n')
