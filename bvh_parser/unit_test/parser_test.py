'''
Created on May 01, 2020

@author: consultit
'''

import sys
import pathlib
import os
pDir = pathlib.Path(__file__).parent.absolute()
ppDir = pDir.parent.absolute()
pppDir = ppDir.parent.absolute()
sys.path.append(str(ppDir))
sys.path.append(str(pppDir))
import unittest
import suite
from bvh_parser.bvhParser import SimpleLexer, Tag, Int, Float, Word, BVHParser
from bvh_parser.bvhHierarchy import Node
from Actors.actorsAnim_common import leftToRightDepthFirstTraversal


def setUpModule():
    pass


def tearDownModule():
    pass


class SimpleLexerTEST(unittest.TestCase):

    def setUp(self):
        with open(os.path.join(ppDir, 'test.bvh')) as f:
            self.wordStream = f.read()

    def tearDown(self):
        delattr(self, 'wordStream')

    def test(self):
        lex = SimpleLexer(self.wordStream, Tag)
        # check if only keywords are inside symbol table
        for lexeme, word in lex.wordTable.items():
            self.assertTrue(word.tag in Tag)
            self.assertTrue(isinstance(word.tag.value, str))
            self.assertEqual(lexeme, word.lexeme)
        # read the file
        numInts = numFloats = numIds = numJOINTs = numZrotations = 0
        numCHANNELSs = numOFFSETs = numLCURLs = numRCURLs = 0
        for token in lex.scan():
            print(token)
            if isinstance(token, Int):
                numInts += 1
            if isinstance(token, Float):
                numFloats += 1
            if isinstance(token, Word):
                if token.tag == Tag.ID:
                    numIds += 1
                elif token.tag == Tag.JOINT:
                    numJOINTs += 1
                elif token.tag == Tag.ZROT:
                    numZrotations += 1
                elif token.tag == Tag.CHANNELS:
                    numCHANNELSs += 1
                elif token.tag == Tag.OFFSET:
                    numOFFSETs += 1
                elif token.tag == Tag.LCURL:
                    numLCURLs += 1
                elif token.tag == Tag.RCURL:
                    numRCURLs += 1
        # assertions
        self.assertEqual(numInts, 19)
        self.assertEqual(numFloats, 69 + 1 + 57 * 2)
        self.assertEqual(numIds, 18)
        self.assertEqual(numJOINTs, 17)
        self.assertEqual(numZrotations, 18)
        self.assertEqual(numCHANNELSs, 18)
        self.assertEqual(numOFFSETs, 23)
        self.assertEqual(numLCURLs, 23)
        self.assertEqual(numLCURLs, numRCURLs)


class BVHParserTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        parser = BVHParser(os.path.join(ppDir, 'test.bvh'), Tag)
        parser.parse()
        # HACK: add an method alias for class Node, so we can use
        # leftToRightDepthFirstTraversal to traverse it
        Node.get_children = lambda self: self._children
        hierarchy = parser.bhvHierarchy
        skeleton = hierarchy.getSkeleton('Hips')
        leftToRightDepthFirstTraversal(
            skeleton.root, action=lambda n, l: print('-' * l + '>', n, l), preorder=True)
        self.assertEqual(hierarchy.frames, 2)
        self.assertAlmostEqual(hierarchy.frameTime, 0.033333, 7)
        self.assertEqual(len(skeleton.joints), 22)
        for frame in skeleton.animation.frames:
            self.assertEqual(skeleton.numChannels, len(frame))


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(SimpleLexerTEST('test'))
    suite.addTest(BVHParserTEST('test'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
