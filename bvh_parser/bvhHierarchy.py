'''
Created on May 4, 2020

@author: consultit
'''

from enum import IntEnum, unique, auto
from collections import OrderedDict
from array import array
from numbers import Integral


@unique
class ChannelType(IntEnum):
    XPOS = auto()
    YPOS = auto()
    ZPOS = auto()
    XROT = auto()
    YROT = auto()
    ZROT = auto()
    NONE = auto()


class Node(object):

    def __init__(self, name):
        self._name = name
        self._offset = []
        self._channels = []
        self._parent = None
        self._children = []

    @property
    def name(self):
        return self._name

    @property
    def offset(self):
        return self._offset

    @property
    def channels(self):
        return self._channels

    @property
    def parent(self):
        return self._parent

    @property
    def children(self):
        return self._children

    def __str__(self):
        return self._name


class Root(Node):
    pass


class Joint(Node):

    def __init__(self, name, parent):
        super().__init__(name)
        self._parent = parent
        self._parent._children.append(self)


class EndSite(Joint):
    pass


class Animation():

    def __init__(self, skel, numFrames):
        '''
        Constructor
        '''

        self._frames = [array('d', [0.0] * skel.numChannels)
                        for _ in range(numFrames)]

    @property
    def frames(self):
        return self._frames

    def __str__(self):
        return str(self._frames)


class Skeleton(object):

    def __init__(self, root):
        '''
        Constructor
        '''
        self._root = root
        self._joints = OrderedDict()
        self._animation = None

    @property
    def name(self):
        return self._root.name

    @property
    def root(self):
        return self._root

    @property
    def joints(self):
        return self._joints.values()

    def addJoint(self, node):
        if isinstance(node, Joint):
            self._joints[node.name] = node

    def getJoint(self, name):
        return self._joints.get(name, None)

    @property
    def animation(self):
        return self._animation

    @animation.setter
    def animation(self, animation):
        self._animation = animation

    @property
    def numChannels(self):
        numChannels = len(self._root.channels)
        for joint in self._joints.values():
            numChannels += len(joint.channels)
        return numChannels

    def __str__(self):
        return self._root.name


class BVHHierarchy(object):
    '''
    BVHHierarchy.
    '''

    def __init__(self):
        self._skeletons = OrderedDict()
        self._frames = 0
        self._frameTime = 1.0

    @property
    def skeletons(self):
        return self._skeletons

    @property
    def frames(self):
        return self._frames

    @frames.setter
    def frames(self, frames):
        self._frames = frames

    @property
    def frameTime(self):
        return self._frameTime

    @frameTime.setter
    def frameTime(self, frameTime):
        self._frameTime = frameTime

    @property
    def frameRate(self):
        return 1.0 / self._frameTime

    @property
    def duration(self):
        return self._frames * self._frameTime

    def addSkeleton(self, skel):
        if isinstance(skel, Skeleton):
            self._skeletons[skel.root.name] = skel

    def getSkeleton(self, nameOrIdx):
        if isinstance(nameOrIdx, Integral):
            return list(self._skeletons.items())[nameOrIdx][1]
        return self._skeletons.get(nameOrIdx, None)

    def __str__(self):
        return ''.join(['(' + name + ') ,' for name in self._skeletons]).rstrip(' ,')
