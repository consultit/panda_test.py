'''
Created on Dec 7, 2019

@author: consultit
'''

from panda3d.core import load_prc_file_data, load_prc_file, Filename, LPoint3f, \
    LVecBase3f, LColorf
from direct.showbase.ShowBase import ShowBase
from rpcore import RenderPipeline
from panda3d._rplight import RPPointLight as PointLight, RPSpotLight as SpotLight
import argparse, textwrap
import os

_myScriptName = os.path.basename(__file__)


class MyApp(ShowBase):

    def __init__(self):
        # create the arguments' parser
        parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description=textwrap.dedent('''
        This is the ''' + _myScriptName + ''' test program.
        '''))    
        # set up arguments
        parser.add_argument('modelA', type=str, help='the model A')
        parser.add_argument('modelB', type=str, help='the model B')
        parser.add_argument('-d', '--data-dir', type=str, action='append',
                            help='the data dir(s)')
        # parse arguments
        args = parser.parse_args()
        # do actions
        # set data dir(s)
        if args.data_dir:
            for dataDir in args.data_dir:
                load_prc_file_data('', 'model-path ' + dataDir)

        # Load your configuration
        load_prc_file(Filename('config.prc'))
        
        # ShowBase
        # super().__init__(self)

        # RenderPipeline
        self.render_pipeline = RenderPipeline()
        self.render_pipeline.create(self)
        self.render_pipeline.daytime_mgr.time = '21:15'
        
        # set lights
        light = PointLight()
        light.pos = LPoint3f(0.0, 0.0, 50.0)
        light.color = LVecBase3f(1.0, 1.0, 1.0)
        light.energy = 5000.0
        light.radius = 100.0
        self.render_pipeline.add_light(light)
        
        # load and transform the model A.
        self.modelA = self.loader.load_model(args.modelA)
        
        # analyze textures
        textureStages = self.modelA.find_all_texture_stages()
        for ts in textureStages:
            print(ts.name)
            print(ts.sort)
            print(ts.priority)
            print(ts.texcoord_name)
            print(ts.tangent_name)
            print(ts.binormal_name)
            print(ts.mode)
        
        self.modelA.set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 0.0),
            LVecBase3f(45.0, 30.0, 0.0),
            LVecBase3f(1.0, 1.0, 1.0))
        self.modelA.reparent_to(self.render)
        
        # load and transform the model B.
        self.modelB = self.loader.load_model(args.modelB)
        self.modelB.set_pos_hpr_scale(
            LPoint3f(0.0, 0.0, 10.0),
            LVecBase3f(0.0, 45.0, 0.0),
            LVecBase3f(1.0, 1.0, 1.0))
        self.modelB.reparent_to(self.render)
        
        # set camera's trackball transform
        self.trackball.node().set_pos(LPoint3f(0.0, 100.0, 0.0))
        self.trackball.set_hpr(LVecBase3f(0.0, 0.0, 0.0))


app = MyApp()
app.run()
