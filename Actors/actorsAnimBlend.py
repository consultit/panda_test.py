'''
Created on Mar 17, 2020

@author: consultit
'''

from panda3d.core import load_prc_file_data, LVecBase3f, LVector3f, CardMaker, \
    BitMask32, TextureStage, PartBundle
from actorsAnim_common import getAnimCtrl, movePlayer, handlePlayerUpdate, \
    blend
from actorsAnim_data import actorData
from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor
from ely.samples.direct.driver import Driver
from ely.samples.direct.utilities import Utilities
import argparse
import textwrap

load_prc_file_data('', 'win-size 1024 768')

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test Actor's animations blending
      
    Commands:
        - 'n': to cycle blend type in 'actor' mode.
        - 'arrow_up/down': to increase/decrease blend factor in 'actor' mode.
        - 'arrow_up/down/left/right': to move character in 'driver' mode.
    
    See: '12 Animations Systems.' Game Engine Architecture, by Jason Gregory,
        3rd ed., CRC Press, Taylor &amp; Francis Group, 2019, pp. 721–816.
    '''))
    # set up arguments
    parser.add_argument('model', type=str,
                        choices=[k for k in actorData.keys()],
                        help='the actor\'s model')
    parser.add_argument('-m', '--mode', type=str, choices=['actor', 'driver'],
                        default='driver', help='the analizyng/testing mode')
    parser.add_argument('--anim1', type=str, default='walk',
                        help='the actor\'s first animation')
    parser.add_argument('--anim2', type=str, default='run',
                        help='the actor\'s second animation')
    parser.add_argument('-p', '--play-rate', type=float, default=1.0,
                        help='the animations\' play rate')
    parser.add_argument('-z', '--z-coord', type=float, default=10.0,
                        help='the actor''s z coordinate')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    # set model dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    # some preliminary checks
    anim1 = args.anim1
    anim2 = args.anim2

    # get an actor
    app = ShowBase()
    actor = Actor(actorData[args.model]['fileName'],
                  actorData[args.model]['anims'])

    playRate = args.play_rate
    actor.reparent_to(app.render)
    actor.set_z(args.z_coord)

    if args.mode == 'actor':
        # # ANIMATION BLENDING MANAGEMENT
        blendingModes = [
            (('blend(actor, anim1, anim2, beta=beta[0], blendType=PartBundle.BT_normalized_linear)'),
             'lerp blending'),
            (('blend(actor, anim1, anim2, beta=beta[0], blendType=PartBundle.BT_componentwise_quat)'),
             'slerp blending'),
            (('blend(actor, anim1, anim2, beta=beta[0], frameBlend=True, blendType=PartBundle.BT_normalized_linear)'),
             'lerp blending with temporal interpolation'),
            (('blend(actor, anim1, anim2, beta=beta[0], frameBlend=True, blendType=PartBundle.BT_componentwise_quat)'),
             'slerp blending with temporal interpolation'),
        ]
        currMode = [0]
        beta = [0.5]

        def cycleBlendingModes(currMode, repeat=False):
            idx = currMode[0]
            print(blendingModes[idx][1])
            if not repeat:
                toExec = blendingModes[idx][0]
                currMode[0] = (currMode[0] + 1) % len(blendingModes)
            else:
                # we are repeating the current blend so we don't need to
                # reenable blending
                toExec = blendingModes[idx][0].rstrip(')') + ', enabled=True)'
            exec(toExec, globals())

        def tweakBeta(event, beta):
            if event == 'arrow_up':
                beta[0] += 0.1
                if beta[0] > 1.0:
                    beta[0] = 1.0
            elif event == 'arrow_down':
                beta[0] -= 0.1
                if beta[0] < 0.0:
                    beta[0] = 0.0
            cycleBlendingModes(currMode, repeat=True)
            print('beta', round(beta[0], 1))

        app.accept('n', cycleBlendingModes, [currMode])
        app.accept('arrow_up', tweakBeta, ['arrow_up', beta])
        app.accept('arrow_down', tweakBeta, ['arrow_down', beta])

    elif args.mode == 'driver':
        # handle animation 1 and bind it to the skeleton
        animCtrl = getAnimCtrl(actor, anim1)
        # driver configuration
        playerDriver = Driver(app, actor, 10)
        playerDriver.set_max_angular_speed(200.0)
        playerDriver.set_angular_accel(100.0)
        playerDriver.set_linear_accel(LVecBase3f(0))
        playerDriver.set_linear_friction(1)
        playerDriver.enable()
        #
        playerDriver.set_max_linear_speed(LVector3f(14.0))
        animRateFactor = 0.071428571  # max_linear_speed*animRateFactor=1.0
        playing = [False]

        # synchronize animation and movements
        forwardMove = 1
        forwardMoveStop = -1
        leftMove = 2
        leftMoveStop = -2
        backwardMove = 3
        backwardMoveStop = -3
        rightMove = 4
        rightMoveStop = -4

        # player will be driven by arrows keys
        app.accept('arrow_up', movePlayer, [forwardMove, playerDriver])
        app.accept('arrow_up-up', movePlayer, [forwardMoveStop, playerDriver])
        app.accept('arrow_left', movePlayer, [leftMove, playerDriver])
        app.accept('arrow_left-up', movePlayer, [leftMoveStop, playerDriver])
        app.accept('arrow_down', movePlayer, [backwardMove, playerDriver])
        app.accept('arrow_down-up', movePlayer,
                   [backwardMoveStop, playerDriver])
        app.accept('arrow_right', movePlayer, [rightMove, playerDriver])
        app.accept('arrow_right-up', movePlayer, [rightMoveStop, playerDriver])

        # scene
        TERRAIN_HEIGHT = 0.0
        cm = CardMaker('ground')
        cm.setFrame(1000, -1000, 1000, -1000)
        terrain = app.render.attach_new_node(cm.generate())
        terrain.set_pos(0, 0, 0)
        terrain.set_p(-90)
        terrain.set_texture(app.loader.loadTexture('grass_ground2.jpg'))
        terrain.set_tex_scale(TextureStage.default, 10.0, 10.0)

        # utilities
        mask = BitMask32(0x10)
        utils = Utilities(app.render, mask)
        terrain.set_collide_mask(mask)
        modelDims = LVecBase3f()
        modelDeltaCenter = LVecBase3f()
        utils.get_bounding_dimensions(actor, modelDims, modelDeltaCenter)
        playerHeight = modelDims.get_z()
        #
        actor.set_pos(0, 0, -playerHeight / 2.0)
        animCtrl.pose(0)

        app.task_mgr.add(handlePlayerUpdate, 'handlePlayerUpdate',
                         appendTask=True, sort=5,
                         extraArgs=[playerDriver, animCtrl, actor,
                                    animRateFactor, playing, playerHeight,
                                    utils, app])

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 90.0, -5.0)
    trackball.set_hpr(0.0, 5.0, 0.0)
    # run
    app.run()
