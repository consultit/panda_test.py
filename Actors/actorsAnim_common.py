'''
Created on Sep 7, 2017

@author: consultit
'''

from panda3d.core import LPoint3f, AnimChannelMatrixXfmTable, LVecBase3f, \
    TransformState, LMatrix4f, CardMaker, NodePath, LVector3f, LVecBase4f, \
    TextNode, AnimControlCollection, PandaNode, AnimBundleNode, PartBundleNode,\
    PartGroup, StringStream, CharacterJoint, GeomVertexArrayFormat, InternalName,\
    Geom, GeomVertexFormat, GeomVertexData, GeomVertexRewriter, GeomTriangles,\
    GeomNode
from direct.gui.DirectGui import DGG
from direct.gui.DirectLabel import DirectLabel
from direct.gui.DirectEntry import DirectEntry
from direct.gui.DirectButton import DirectButton
from direct.gui.DirectScrolledList import DirectScrolledList
from direct.gui.OnscreenText import OnscreenText
from matplotlib import pyplot
from scipy import interpolate, ndimage
import numpy
from math import sqrt, modf
from collections import namedtuple
import logging
from panda_test_py.bvh_parser.bvhParser import BVHParser
from panda_test_py.bvh_parser.bvhHierarchy import Node, ChannelType
from collections.abc import Callable
from enum import IntEnum, auto


def getDimensions(actor):
    '''get 'tight' dimensions of actor'''

    minP = LPoint3f()
    maxP = LPoint3f()
    actor.calc_tight_bounds(minP, maxP)
    delta = maxP - minP
    actorDims = LVector3f(abs(delta.get_x()), abs(delta.get_y()),
                          abs(delta.get_z()))
    bodyRadius = ((actorDims.get_x() + actorDims.get_y() + actorDims.get_z())
                  / 150.0)
    xC = (minP.get_x() + maxP.get_x()) / 2.0
    yC = minP.get_y() - actorDims.get_z() * 3.8 / 2.0
    zC = minP.get_z() + actorDims.get_z() * 2.0 / 4.0
    xL = (minP.get_x() + maxP.get_x()) / 2.0
    yL = (minP.get_y() + maxP.get_y()) / 2.0
    zL = (minP.get_z() + maxP.get_z()) / 2.0
    return bodyRadius, xC, yC, zC, xL, yL, zL


def getAnimCtrl(actor, anim, partName=None):
    # remaining modes handle animation and bind it to the skeleton
    animCtrl = actor.get_anim_control(anim, partName=partName,
                                      allowAsyncBind=False)
    if not animCtrl:
        print(actor, 'hasn\'t', anim, 'animation. Exiting...')
        exit(-1)
    return animCtrl


def getInfos(actor, anim=None):

    # generic getInfos
    actor.bind_all_anims()
    actor.pprint()
    print('get_actor_info(): ', actor.get_actor_info())
    print('get_geom_node(): ', actor.get_geom_node())
    print('get_part_names(): ', actor.get_part_names())
    print('get_part_bundle_dict(): ', actor.get_part_bundle_dict())
    print('get_anim_names(): ', actor.get_anim_names())
    print('get_anim_control_dict(): ', actor.get_anim_control_dict())

    if actor.has_lod():
        print('get_lod_node(): ', actor.get_lod_node())
        print('get_lod_names(): ', actor.get_lod_names())

    if anim:
        animCtrl = getAnimCtrl(anim)
        animBundle = animCtrl.get_anim()
        frames = animBundle.get_num_frames()
        getAnimHierarchy(animBundle, frames, '', '*')

        # handle parts
        partBundle = actor.get_part_bundle('modelRoot')
        getPartHierarchy(partBundle, '', '*')


def getAnimCtrlAttr(ctrl, attr):

    attrs = ['frac', 'get_frac', 'frame', 'get_frame', 'frame_rate',
             'get_frame_rate', 'full_fframe', 'get_full_fframe', 'full_frame',
             'get_full_frame', 'next_frame', 'get_next_frame', 'num_frames',
             'get_num_frames', 'play_rate', 'get_play_rate', 'set_play_rate',
             'playing', 'is_playing', 'loop', 'pingpong', 'play', 'pose', 'stop',
             'get_anim', 'has_anim', 'get_anim_model', 'set_anim_model',
             'get_bound_joints', 'get_channel_index', 'get_part',
             'get_pending_done_event', 'set_pending_done_event', 'is_pending',
             'wait_pending']
    if attr in attrs:
        return getattr(ctrl, attr)
    else:
        print('not interesting or not existent attribute: ' + str(attr))
        return None


def animPanelDestroy():
    print('AnimPanel destroyed.')


def handlePlayerUpdate(playerDriver, animCtrl, actor, animRateFactor, playing,
                       playerHeight, utils, app, task):
    '''handles player on every update'''

    # get current forward velocity size
    currentVelSize = abs(playerDriver.get_current_speeds()[0].get_y())
    # handle vehicle's animation
    if playerDriver.is_forward_enabled() or playerDriver.is_backward_enabled():
        animCtrl.set_play_rate(currentVelSize * animRateFactor)
        if not playing[0]:
            animCtrl.loop(False)
            playing[0] = True
    else:
        # stop any animation
        if playing[0]:
            currFrame = animCtrl.get_full_fframe()
            endFrame = animCtrl.get_num_frames()
            animCtrl.play(currFrame, endFrame)
            playing[0] = False
        animCtrl.set_play_rate(currentVelSize * animRateFactor)
    # correct position
    pos = actor.get_pos()
    pOrig = pos + LPoint3f(0, 0, playerHeight)
    gotCollisionZ = utils.get_collision_height(pOrig, app.render)
    if gotCollisionZ[0]:
        deltaHeight = pOrig.get_z() - gotCollisionZ[1]
        actor.set_z(pos.get_z() + playerHeight - deltaHeight - 0.3)
    return task.cont


def getAnimHierarchy(animG, frames, tab, c):
    tab += c
    print(tab, animG.get_name(), type(animG))
    if animG.get_type() == AnimChannelMatrixXfmTable.get_class_type():
        for i in range(frames):
            pos = LPoint3f()
            hpr = LVecBase3f()
            animG.get_pos(i, pos)
            animG.get_hpr(i, hpr)
            print(' ' * (len(tab) + 2), pos, hpr)
        print()

    for child in animG.get_children():
        getAnimHierarchy(child, frames, tab, c)
    tab = tab[:-1]


def getPartHierarchy(part, tab, c):
    tab += c
    print(tab, part.get_name(), type(part))
    if part.is_character_joint():
        for i in range(part.get_max_bound()):
            animChannel = part.get_bound(i)
            print(' ' * (len(tab) + 2), type(animChannel),
                  animChannel.get_name())
        print()

    for child in part.get_children():
        getPartHierarchy(child, tab, c)
    tab = tab[:-1]


def trackAnim(app, render, animCtrl, actor, partBundle, result):
    '''setup track base joint transform's GUI'''

    _currentJointName = ''
    _currentTrackedNode = None

    def _trackCompute(jointName):
        '''compute track base joint transform'''

        nonlocal _currentJointName, _currentTrackedNode, app, animCtrl, actor, \
            partBundle, startFrameEntry, avSpeedText, absDistText, \
            endFrameEntry, result, resultButton

        def _tracking(task):
            '''track base joint transform'''

            nonlocal app, animCtrl, trackedJoint, trackedNode, startFrame, \
                endFrame, result, resultButton, avSpeedText, absDistText
            frame = animCtrl.get_full_fframe()
            channelTrans = []
            for i in range(trackedJoint.get_max_bound()):
                animChannel = trackedJoint.get_bound(i)
                pos = LPoint3f()
                hpr = LVecBase3f()
                animChannel.get_pos(int(frame), pos)
                animChannel.get_hpr(int(frame), hpr)
                channelTrans.append(TransformState.make_pos_hpr(pos, hpr))
            jMat = trackedJoint.get_transform()
            jNetMat = LMatrix4f()
            trackedJoint.get_net_transform(jNetMat)
            # nodeTrans = trackedNode.get_net_transform()
            # nodeTrans = trackedNode.get_transform()
            nodeTrans = trackedNode.get_transform(app.render)
            #
            result.append((frame, task.time, jMat, jNetMat, nodeTrans,
                           channelTrans))
            if animCtrl.is_playing():
                return task.cont
            else:
                resultButton['state'] = DGG.NORMAL
                animCtrl.stop()
                animCtrl.pose(0.0)
                # overall distance and average speed
                distance = [0.0, 0.0, 0.0, 0.0]
                for i in range(len(result) - 1):
                    if i == 0:
                        continue
                    sPos = result[i][4].get_pos()
                    ePos = result[i + 1][4].get_pos()
                    deltaPos = ePos - sPos
                    distance[0] += deltaPos.length()  # overall speed
                    distance[1] += abs(deltaPos.get_x())  # x speed
                    distance[2] += abs(deltaPos.get_y())  # y speed
                    distance[3] += abs(deltaPos.get_z())  # z speed
                dt = result[-1][1] - result[1][1]
                fmt = '{:3.4g} - ({:3.4g}, {:3.4g}, {:3.4g})'
                distsText = fmt.format(*[round(d, 4) for d in distance])
                absDistText.setText(distsText)
                if dt > 0.0:
                    dtInv = 1.0 / dt
                    speedsText = fmt.format(
                        *[round(d * dtInv, 4) for d in distance])
                    avSpeedText.setText(speedsText)
                return task.done

        def _drawJoint(nodeJoint, dimFactor, render, scale, color, drawBin):
            jointCard = CardMaker('nodeJoint')
            cardDim = 0.3 * dimFactor
            jointCard.set_frame(-cardDim, cardDim, -cardDim, cardDim)
            jointSquare = NodePath(jointCard.generate())
            jointSquare.reparent_to(nodeJoint)
            jointSquare.set_hpr(render, 0, 0, 0)
            jointSquare.set_scale(scale)
            jointSquare.set_color(color)
            jointSquare.set_two_sided(True)
            jointSquare.set_bin('fixed', drawBin)
            jointSquare.set_depth_write(False)
            jointSquare.set_depth_test(False)
            jointSquare.set_billboard_point_eye()

        trackedJoint = partBundle.find_child(jointName)
        if not trackedJoint:
            return

        if _currentJointName:
            actor.stop_joint('modelRoot', _currentJointName)
        if _currentTrackedNode:
            _currentTrackedNode.remove_node()
        trackedNode = actor.expose_joint(None, 'modelRoot', jointName)
        _currentJointName = jointName
        _currentTrackedNode = trackedNode

        # append visual reference
        dims = getDimensions(actor)
        _drawJoint(trackedNode, 3.0 * dims[0], app.render, 1.0,
                   LVecBase4f(255.0 / 255.0, 128.0 / 255.0, 0.0 / 255.0, 1), 45)

        numFrames = animCtrl.get_num_frames()
        startFrame = int(startFrameEntry.get()) if startFrameEntry.get() else 0
        endFrame = int(endFrameEntry.get()) if endFrameEntry.get() else 0
        if startFrame >= numFrames:
            startFrame = numFrames - 1
        elif startFrame < 0:
            startFrame = 0
        if endFrame >= numFrames:
            endFrame = numFrames - 1
        elif endFrame < startFrame:
            endFrame = startFrame
        result.clear()
        result.append(jointName)
        avSpeedText.setText('0.0')
        absDistText.setText('0.0')
        resultButton['state'] = DGG.DISABLED
        app.task_mgr.remove('_tracking')
        app.task_mgr.add(_tracking, '_tracking', appendTask=True, sort=15)
        if not animCtrl.is_playing():
            animCtrl.pose(startFrame)
            animCtrl.play(startFrame, endFrame)

    animName = ''
    for name in actor.get_anim_names():
        if actor.get_anim_control(name) == animCtrl:
            animName = name
            break
    animLabel = DirectLabel(parent=render, text='Anim:  ' + animName,
                            scale=0.07, text_fg=(0.2, 0.7, 0, 1),
                            pos=LPoint3f(-0.65, 0.0, 0.9))
    #
    startLabel = DirectLabel(parent=render, text='start frame', scale=0.05,
                             text_fg=(1, 0, 0, 1),
                             pos=LPoint3f(-0.65, 0.0, 0.77))
    startFrameEntry = DirectEntry(parent=render, scale=0.05, initialText='0',
                                  width=3, numLines=1, text_fg=(1, 0, 0, 1),
                                  pos=LPoint3f(-0.77, 0.0, 0.7))
    #
    endLabel = DirectLabel(parent=render, text='end frame', scale=0.05,
                           text_fg=(1, 0, 0, 1),
                           pos=LPoint3f(-0.66, 0.0, 0.60))
    endFrameEntry = DirectEntry(parent=render, scale=0.05, initialText='0',
                                width=3, numLines=1, text_fg=(1, 0, 0, 1),
                                pos=LPoint3f(-0.77, 0.0, 0.53))
    #
    absSpaceLabel = DirectLabel(parent=render, scale=0.05,
                                text='Node traveled distance: D - (Dx,Dy,Dz)',
                                pos=LPoint3f(0.8, 0.0, -0.60))
    absDistText = OnscreenText(parent=render, text='0.0', scale=0.07,
                               fg=(1, 0.5, 0.5, 1),
                               align=TextNode.A_center, mayChange=1,
                               pos=(0.8, -0.70))
    #
    avSpeedLabel = DirectLabel(parent=render, scale=0.05,
                               text='Node average speeds: S - (Sx,Sy,Sz)',
                               pos=LPoint3f(0.8, 0.0, -0.80))
    avSpeedText = OnscreenText(parent=render, text='0.0', scale=0.07,
                               fg=(1, 0.5, 0.5, 1), align=TextNode.A_center,
                               mayChange=1,
                               pos=(0.8, -0.90))
    #
    resultButton = DirectButton(parent=render, text=('plot graphs'),
                                text_fg=(0, 0.6, 0.4, 1),
                                scale=0.05, command=trackResult,
                                extraArgs=[result],
                                pos=LPoint3f(-0.65, 0.0, 0.35))
    #
    jointLabel = DirectLabel(parent=render, text='joints', scale=0.05,
                             pos=LPoint3f(-1.05, 0.0, 0.88))
    jointNames = sorted([j.get_name() for j in actor.get_joints()])
    entries = [DirectButton(parent=render, text=(jointName, 'GO!'),
                            borderWidth=(0.005, 0.005), text_fg=(0, 0, 1, 1),
                            text_scale=0.05, command=_trackCompute,
                            extraArgs=[jointName])
               for jointName in jointNames]
    jointList = DirectScrolledList(parent=render,
                                   decButton_pos=(0.35, 0, 0.55),
                                   decButton_text='up',
                                   decButton_text_scale=0.04,
                                   decButton_borderWidth=(0.01, 0.01),
                                   incButton_pos=(0.35, 0, -1.07),
                                   incButton_text='down',
                                   incButton_text_scale=0.04,
                                   incButton_borderWidth=(0.01, 0.01),
                                   frameSize=(0.12, 0.58, -1.09, 0.59),
                                   # frameColor=(1, 0, 0, 0.5),
                                   pos=(-1.4, 0, 0.25),
                                   items=entries,
                                   numItemsVisible=20,
                                   forceHeight=0.075,
                                   itemFrame_frameSize=(-0.2, 0.2, -0.6, 0.06),
                                   itemFrame_pos=(0.35, 0, 0.45),
                                   )


def trackResult(result):
    '''plot track base joint transform'''

    if not result:
        return

    def _numericDifferentiation(pos, times, dataLen):
        '''use central finite difference formula'''

        s = [0.0] * dataLen
        for i in range(1, dataLen - 1):
            s[i] = (pos[i + 1] - pos[i - 1]) / (times[i + 1] - times[i - 1])
        # adjust first and last
        s[0], s[-1] = s[1], s[-2]
        return s

    def _plotGraph(axes, xlabel, ylabel, title, legend, coords):
        '''track plot'''

        axes.plot(*coords)
        axes.set_xlabel(xlabel)
        axes.set_ylabel(ylabel)
        axes.set_title(title)
        axes.legend(legend, loc='best')

    def _plotGraph3d(axes, xlabel, ylabel, zlabel, title, elevation, rotation,
                     legend, coords):
        '''track plot'''

        for i in range(0, len(coords), 4):
            coord = coords[i:i + 4]
            axes.plot(*coord)
        axes.set_xlabel(xlabel)
        axes.set_ylabel(ylabel)
        axes.view_init(elev=elevation, azim=rotation)
        axes.set_zlabel(zlabel)
        axes.set_title(title)
        axes.legend(legend, loc='best')

    def _interpolate(dataLen, arrays, iarrays, k):
        legend = ['sampled line']
        if dataLen == 2:
            ierp = interpolate.interp1d(*arrays, kind='spline 1 order',
                                        assume_sorted=True)
            vectors = (*arrays, '-', iarrays, ierp(iarrays), '--')
        else:
            try:
                tck, _ = interpolate.splprep(arrays, k=k)
                _, ierp = interpolate.splev(iarrays, tck)
                vectors = (*arrays, '-', iarrays, ierp, '--')
                legend += ['B-spline ' + str(k) + ' degree']
            except ValueError:
                # https://stackoverflow.com/questions/47948453/scipy-interpolate-splprep-error-invalid-inputs
                arrays1 = ndimage.gaussian_filter1d(arrays, 5)
                try:
                    tck, _ = interpolate.splprep([*arrays1], k=k)
                    _, ierp = interpolate.splev(iarrays, tck)
                    vectors = (*arrays, '-', iarrays, ierp, '--')
                    legend += ['B-spline, ' + str(k) + ' degree, filtered']
                except ValueError:
                    ierp = interpolate.interp1d(*arrays, kind='cubic',
                                                assume_sorted=True)
                    vectors = (*arrays, '-', iarrays, ierp(iarrays), '--')
                    legend += ['spline 3 order']
        return (legend, vectors)

    def _interpolate3d(dataLen, arrays, iarrays, k):
        legend = ['sampled line']
        if dataLen == 2:
            vectors = arrays + ['-', ]
        else:
            try:
                tck, _ = interpolate.splprep([*arrays], k=k)
                ix, iy, iz = interpolate.splev(iarrays, tck)
                vectors = [*arrays, '-', ix, iy, iz, '--']
                legend += ['B-spline ' + str(k) + ' degree']
            except ValueError:
                # https://stackoverflow.com/questions/47948453/scipy-interpolate-splprep-error-invalid-inputs
                arrays1 = ndimage.gaussian_filter1d(arrays, 5)
                try:
                    tck, _ = interpolate.splprep([*arrays1], k=k)
                    ix, iy, iz = interpolate.splev(iarrays, tck)
                    vectors = [*arrays, '-', ix, iy, iz, 'g--']
                    legend += ['B-spline, ' + str(k) + ' degree, filtered']
                except ValueError:
                    vectors = arrays + ['-', ]
        return (legend, vectors)

    resultCopy = result[:]
    jointName = resultCopy.pop(0)
    for frame, t, jTrans, jNetTrans, nTrans, chTrans in resultCopy:
        # (frame, task.time, joint.get_transform(), joint.get_net_transform(),
        #    node.get_net_transform(), channelTrans)
        jStr = ('joint: (' + str(frame) + ', ' + str(t) + ', ' +
                str(TransformState.make_mat(jTrans)).rstrip('\n') + ', ' +
                str(TransformState.make_mat(jNetTrans)).rstrip('\n') + ', ' +
                str(nTrans).rstrip('\n') + ')')
        print(jStr)
        for i, trans in enumerate(chTrans):
            cStr = ('\tchannel ' + str(i) + ': (' +
                    str(trans).rstrip('\n') + ')')
            print(cStr)
        print('----')
    # plot data
    # prepare figure
    fig = pyplot.figure(figsize=(16, 12))

    # len(resultCopy) >= 2
    if len(resultCopy) == 1:
        resultCopy += resultCopy
    dataLen = len(resultCopy)
    times, frames = [], []
    for record in resultCopy:
        times.append(record[1])
        frames.append(record[0])
    iRes = 10
    itimes = numpy.linspace(numpy.min(times), numpy.max(times),
                            num=len(times) * iRes, endpoint=True)
    # dataLen >= 2, 1 <= k <= 5, k < dataLen - 1
    k = 5
    if (dataLen - 1) <= 5:
        k = max(1, dataLen - 2)
    # joint traiectory
    jPos = [[], [], []]
    for record in resultCopy:
        trans = TransformState.make_mat(record[3])
        jPos[0].append(trans.get_pos()[0])
        jPos[1].append(trans.get_pos()[1])
        jPos[2].append(trans.get_pos()[2])

    legend, vectors = _interpolate3d(dataLen, jPos, itimes, k)
    ax1 = fig.add_subplot(231, projection='3d')
    _plotGraph3d(ax1, 'x', 'y', 'z', '\'' + jointName + '\' joint traiectory',
                 None, None, legend, vectors)

    # node trajectory
    nPos = [[], [], []]
    for record in resultCopy:
        trans = record[4]
        nPos[0].append(trans.get_pos()[0])
        nPos[1].append(trans.get_pos()[1])
        nPos[2].append(trans.get_pos()[2])

    legend, vectors = _interpolate3d(dataLen, nPos, itimes, k)
    ax2 = fig.add_subplot(232, projection='3d')
    _plotGraph3d(ax2, 'x', 'y', 'z', '\'' + jointName + '\' node traiectory',
                 None, None, legend, vectors)

    # node speeds
    # speed.x
    sx = _numericDifferentiation(nPos[0], times, dataLen)

    legend, vectors = _interpolate(dataLen, (times, sx), itimes, k)
    axX = fig.add_subplot(234)
    _plotGraph(axX, 't', 'sx', '\'' + jointName + '\' node x speed',
               legend, vectors)

    # speed.y
    sy = _numericDifferentiation(nPos[1], times, dataLen)

    legend, vectors = _interpolate(dataLen, (times, sy), itimes, k)
    axY = fig.add_subplot(235)
    _plotGraph(axY, 't', 'sy', '\'' + jointName + '\' node y speed',
               legend, vectors)
    # speed.z
    sz = _numericDifferentiation(nPos[2], times, dataLen)

    legend, vectors = _interpolate(dataLen, (times, sz), itimes, k)
    axZ = fig.add_subplot(236)
    _plotGraph(axZ, 't', 'sz', '\'' + jointName + '\' node z speed',
               legend, vectors)

    # node speed module wrt (time, speed)
    speed = [sqrt(sX ** 2 + sY ** 2 + sZ ** 2)
             for sX, sY, sZ in zip(sx, sy, sz)]

    legend, vectors = _interpolate3d(
        dataLen, [times, frames, speed], itimes, k)
    axS = fig.add_subplot(233, projection='3d')
    _plotGraph3d(axS, 't', 'f', 's', '\'' + jointName + '\' node speed module',
                 None, None, legend, vectors)
    #
    pyplot.tight_layout(pad=1)
    pyplot.show()


class AnimControlManual(object):
    '''Controls manually the timing of a character animation.'''

    def __init__(self, app, animCtrl):
        self._app = app
        self._animCtrl = animCtrl
        self._numFrames = self._animCtrl.get_num_frames()
        self._frameRate = self._animCtrl.get_frame_rate()
        self._fframe = 0
        self._endFrame = self._numFrames - 1
        self._playRate = 1.0
        self._isPlaying = False
        # to save the task without adding we should add -> save -> remove;
        # instead we use this hack:
        self._execAnimTask = self._app.task_mgr._TaskManager__setupTask(
            self._execute_anim, name='_execute_anim', sort=10, appendTask=True,
            extraArgs=None, priority=None, uponDeath=None, taskChain=None,
            owner=None)

    def play_anim(self, startFrame=None, endFrame=None, doLoop=False):
        '''Play animation manually

        :param startFrame: the frame to start the animation, defaults to 0
        :type startFrame: float, optional
        :param endFrame: the frame to stop the animation, defaults to last frame index
        :type endFrame: float, optional
        :param doLoop: flag to indicate if animation loop cycles, defaults to no loop
        :type doLoop: float, optional
        '''

        self.stop_anim()

        if startFrame is not None:
            self._startFrame = startFrame
            if self._startFrame >= self._numFrames:
                self._startFrame = self._numFrames - 1
            elif self._startFrame < 0:
                self._startFrame = 0
        else:
            self._startFrame = 0
        if endFrame is not None:
            self._endFrame = endFrame
            if self._endFrame >= self._numFrames:
                self._endFrame = self._numFrames - 1
            elif self._endFrame < self._startFrame:
                self._endFrame = self._startFrame
        else:
            self._endFrame = self._numFrames - 1
        self._doLoop = doLoop
        #
        self._nativeDuration = ((self._endFrame - self._startFrame)
                                / self._frameRate)
        self._currentDuration = self._nativeDuration * self._playRateInv
        self._currentFrameRate = self._frameRate * self._playRate
        #
        self._loops = 0
        #
        self._app.task_mgr.add(self._execAnimTask)
        self._isPlaying = True

    def stop_anim(self):
        '''stop animation'''

        if self._isPlaying:
            self._app.task_mgr.remove(self._execAnimTask)
            self._isPlaying = False

    def _execute_anim(self, task):
        animTime = task.time - self._loops * self._currentDuration
        if animTime > self._currentDuration:
            if not self._doLoop:
                self._isPlaying = False
                self._app.task_mgr.remove(self._execAnimTask)
            self._loops += 1
            animTime -= self._currentDuration
        self._fframe = self._currentFrameRate * animTime + self._startFrame
        self._animCtrl.pose(self._fframe)
        #
        return task.cont

    @property
    def fframe(self):
        return self._fframe

    @property
    def is_playing(self):
        return self._isPlaying

    @property
    def play_rate(self):
        return self._playRate

    @play_rate.setter
    def play_rate(self, playRate):
        if not playRate:
            self._playRate = self._playRateInv = 1.0
        else:
            self._playRate = float(playRate)
            self._playRateInv = 1.0 / self._playRate


def movePlayer(action, playerDriver):
    '''player's movement callback'''

    if not playerDriver:
        return

    forwardMove = 1
    # forwardMoveStop = -1
    leftMove = 2
    # leftMoveStop = -2
    backwardMove = 3
    # backwardMoveStop = -3
    rightMove = 4
    # rightMoveStop = -4

    if action > 0:
        # start movement
        enable = True
    else:
        action = -action
        # stop movement
        enable = False
    #
    if action == forwardMove:
        playerDriver.enable_forward(enable)
    elif action == leftMove:
        playerDriver.enable_head_left(enable)
    elif action == backwardMove:
        playerDriver.enable_backward(enable)
    elif action == rightMove:
        playerDriver.enable_head_right(enable)


def blend(actor, anim1, anim2, beta, enabled=False, frameBlend=None,
          blendType=None, partName=None):
    '''Linear interpolation (LERP) blending.'''

    if not enabled:
        actor.set_blend(animBlend=True, frameBlend=frameBlend,
                        blendType=blendType, partName=partName)
    actor.set_control_effect(anim1, beta)
    actor.set_control_effect(anim2, 1.0 - beta)
    actor.loop(anim1)
    actor.loop(anim2)


def leftToRightBreadthFirstTraversal(node, action, extraArgs=[], debug=False):
    '''Iterative left-to-right breadth-first tree traversal'''

    if debug:
        print('\n', leftToRightBreadthFirstTraversal.__doc__, sep='')
    Item = namedtuple('Item', ['node', 'level'])
    level = 0
    queue = [Item(node, level)]
    action(node, level, *extraArgs)
    while queue:
        curr = queue.pop(0)
        childLevel = curr.level + 1
        for child in curr.node.get_children():
            action(child, childLevel, *extraArgs)
            queue.append(Item(child, childLevel))


def leftToRightDepthFirstTraversal(node, action, extraArgs=[], preorder=True,
                                   debug=False):
    '''Iterative left-to-right depth-first tree traversal'''

    if debug:
        msg = leftToRightDepthFirstTraversal.__doc__ + \
            (' - preorder' if preorder else '- postorder')
        print('\n', msg, sep='')
    if preorder:
        Item = namedtuple('Item', ['node', 'level'])
        level = 0
        stack = [Item(node, level)]
        while stack:
            curr = stack.pop()
            action(curr.node, curr.level, *extraArgs)
            # add children in reversed order
            for child in reversed(curr.node.get_children()):
                stack.append(Item(child, curr.level + 1))
    else:
        # postorder
        Item = namedtuple('Item', ['node', 'level', 'visited'])
        level = 0
        stack = [Item(node, level, False)]
        while stack:
            currItem = stack[-1]
            if not currItem.visited:
                # currItem not first visited: add children in reversed order
                stack[-1] = Item(currItem.node, currItem.level, True)
                # add children in reversed order
                for child in reversed(currItem.node.get_children()):
                    stack.append(Item(child, currItem.level + 1, False))
            else:
                # currItem already first visited: run action and remove it
                action(currItem.node, currItem.level, *extraArgs)
                stack.pop()


class TreeNode(object):

    class _Iter(object):

        def __init__(self, seq):
            self._seq = seq

        def __iter__(self):
            i = 0
            while i < len(self._seq):
                yield self._seq[i]
                i += 1

        def __reversed__(self):
            i = len(self._seq) - 1
            while i >= 0:
                yield self._seq[i]
                i -= 1

    def __init__(self, value):
        self._children = []
        self._value = value

    def add_child(self, child):
        self._children.append(child)

    def get_children(self):
        return TreeNode._Iter(self._children)

    def __str__(self):
        return str(self._value)


def buildTree(tree):
    '''
    Builds a tree from arry-based representation.

    Return:  tree root TreeNode.
    '''

    rootRepr = tree[0]
    rootNode = TreeNode(tree[0][0])
    queue = [(rootRepr, rootNode)]
    idx = 0
    while queue:
        currRepr, currNode = queue.pop(0)
        currItemChildren = [tree[idx + 1 + i] for i in range(currRepr[1])]
        for childRepr in currItemChildren:
            childNode = TreeNode(childRepr[0])
            currNode.add_child(childNode)
            queue.append((childRepr, childNode))
        idx += currRepr[1]
    #
    return rootNode


def retrievePartGroups(model, verbose=True):
    '''retrieve PartGroup(s) below model.

    It uses tree depth-first preorder traversal algorithm.

    '''

    partGroups = []
    fmt = '\'{}\' (type: {})'
    sp = '  '
    print('\n*PartBundleNodes under \'', model.get_name(), '\':', sep='')
    partBundleNPs = model.find_all_matches('**/+PartBundleNode')
    for pbnIdx in range(partBundleNPs.get_num_paths()):
        partBundleNode = partBundleNPs.get_path(pbnIdx).node()
        print(sp, pbnIdx + 1, ') PartBundleNode: ',
              fmt.format(partBundleNode.get_name(),
                         partBundleNode.get_type().get_name()), sep='')

        # print('    *Joints and sliders:', sep='')
        # outStr = StringStream()
        # partBundleNode.write_part_values(outStr)
        # print('      ', outStr.get_data().decode('utf-8'), sep='')

        print(sp * 2 + '*PartBundles pointed:', sep='')
        for pbIdx in range(partBundleNode.get_num_bundles()):
            partBundle = partBundleNode.get_bundle(pbIdx)
            partGroups.append(partBundle)
            print(sp * 3, pbIdx + 1, ') PartBundle: ',
                  fmt.format(partBundle,
                             partBundle.get_type().get_name()), sep='')
            if verbose:
                outStr = StringStream()
                partBundle.write_with_value(outStr, 10)
                print(sp * 4 + '*PartBundle description: \'\n',
                      outStr.get_data().decode('utf-8'), '\'', sep='')
                print(sp * 4 + '*PartBundle root transform matrix:\n',
                      partBundle.get_root_xform(), sep='')

            # walk the hierarchy in depth-first preorder traversal
            def _partGroupInfo(p, l):
                nonlocal partGroups, verbose
                partGroups.append(p)
                sep = ' ' + '-' * l
                isJoint = ' (joint)' if p.is_character_joint() else ''
                print(sep, p.get_name(), isJoint, sep='')
                if verbose:
                    trans = transNet = ''
                    if p.is_of_type(CharacterJoint.get_class_type()):  # isJoint
                        mat = LMatrix4f()
                        matNet = LMatrix4f()
                        p.get_transform(mat)
                        trans = TransformState.make_mat(mat)
                        p.get_net_transform(matNet)
                        transNet = TransformState.make_mat(matNet)
                        print(sep + '- Trans: ', str(trans).rstrip('\n'), sep='')
                        print(sep + '- Net trans: ',
                              str(transNet).rstrip('\n'), sep='')
                    print()

            print('Walk the hierarchy of \'', partBundle, '\'', sep='')
            leftToRightDepthFirstTraversal(
                partBundle, action=_partGroupInfo, preorder=True)

            # print(sp * 4 + '*PartBundle Child nodes:', sep='')
            # for cnIdx in range(partBundle.get_num_children()):
            #     childNode = partBundle.get_child(cnIdx)
            #     print(sp * 5, cnIdx + 1, ') Child node:\n',
            #           fmt.format(childNode,
            #                      childNode.get_type().get_name()), sep='')
            #     outStr = StringStream()
            #     childNode.write_with_value(outStr, 12)
            #     print(sp * 6 + '*Child node description: \'\n',
            #           outStr.get_data().decode('utf-8'), '\'', sep='')
    return partGroups


############ PANDA 3D SOFTWARE ############
# logging
chan_cat = logging.getLogger('COM_logger')
chan_cat.setLevel(logging.DEBUG)
# create console handler
consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter(
    '%(pathname)s - %(funcName)s - %(levelname)s - %(message)s')
consoleHandler.setFormatter(formatter)
# add the handlers to the logger
chan_cat.addHandler(consoleHandler)

chan_cat.setLevel(logging.DEBUG)

# Copyright (c) Carnegie Mellon University.  All rights reserved.
#
# All use of this software is subject to the terms of the revised BSD
# license.  You should have received a copy of this license along
# with this source code in a file named "LICENSE."
#
# @file auto_bind.cxx
# @author drose
# @date 1999-02-23

# AnimBundles = {AnimBundle,...}
AnimBundles = set
# Anims = {str: AnimBundles, ...}
Anims = dict

# PartBundles = {PartBundle,...}
PartBundles = set
# Parts = {str: PartBundles, ...}
Parts = dict


def bind_anims(parts: PartBundles,  anims: AnimBundles,
               controls: AnimControlCollection,  hierarchy_match_flags: int):
    '''
    A support function for auto_bind(), below.  Given a set of AnimBundles and
    a set of PartBundles that all share the same name, perform whatever
    bindings make sense.
    '''

    for part in parts:
        for anim in anims:
            chan_cat.info('Attempting to bind ' + str(part) +
                          ' to ' + str(anim) + '\n')
            control = part.bind_anim(anim, hierarchy_match_flags)
            name = anim.get_name()
            if not name:
                name = anim.get_name()
            if control:
                if controls.find_anim(name):
                    # That name's already used synthesize another one.
                    index = 0
                    new_name = str()
                    while True:
                        index += 1
                        new_name = name + '.' + str(index)
                        if not controls.find_anim(new_name):
                            break
                    name = new_name
                controls.store_anim(control, name)
            if not control:
                chan_cat.info('Bind failed.\n')
            else:
                chan_cat.info('Bind succeeded, index ' +
                              str(control.get_channel_index()) +
                              ' accessible as ' + name + '\n')


def r_find_bundles(node: PandaNode,  anims: Anims,  parts: Parts):
    '''
    A support function for auto_bind(), below.  Walks through the hierarchy and
    finds all of the PartBundles and AnimBundles.
    '''

    if node.is_of_type(AnimBundleNode.get_class_type()):
        bundle = node.get_bundle()
        bundleName = bundle.get_name()
        if bundleName in anims:
            anims[bundleName].add(bundle)
        else:
            anims[bundleName] = set([bundle])
    elif node.is_of_type(PartBundleNode.get_class_type()):
        num_bundles = node.get_num_bundles()
        for i in range(num_bundles):
            bundle = node.get_bundle(i)
            bundleName = bundle.get_name()
            if bundleName in parts:
                parts[bundleName].add(bundle)
            else:
                parts[bundleName] = set([bundle])
    cr = node.get_children()
    for child in cr:
        r_find_bundles(child, anims, parts)


def auto_bind(root_node: PandaNode,  controls: AnimControlCollection,
              hierarchy_match_flags: int=0):
    '''
    Walks the scene graph or subgraph beginning at the indicated node, and
    attempts to bind any AnimBundles found to their matching PartBundles, when
    possible.

    The list of all resulting AnimControls created is filled into controls.
    '''

    # First, locate all the bundles in the subgraph.
    anims = Anims()
    extra_anims = AnimBundles()
    parts = Parts()
    extra_parts = PartBundles()
    r_find_bundles(root_node, anims, parts)
    animsKeys = sorted(anims)
    partsKeys = sorted(parts)
    #
    msg = ''
    anim_count = 0
    for ai in animsKeys:
        anim_count += len(anims[ai])
    msg += ('Found ' + str(anim_count) + ' anims:\t')
    for ai in animsKeys:
        msg += (ai)
        if len(anims[ai]) != 1:
            msg += (' * ' + str(len(anims[ai])))
        msg += (', ')
    chan_cat.debug(msg.rstrip(', ') + '\n')
    #
    msg = ''
    part_count = 0
    for pi in partsKeys:
        part_count += len(parts[pi])
    msg += ('Found ' + str(part_count) + ' parts:\t')
    for ai in partsKeys:
        msg += (pi)
        if len(parts[pi]) != 1:
            msg += (' * ' + str(len(parts[pi])))
        msg += (', ')
    chan_cat.debug(msg.rstrip(', ') + '\n')
    #
    # Now, match up the bundles by name.
    while animsKeys and partsKeys:
        ai = animsKeys[0]
        pi = partsKeys[0]
        if ai < pi:
            # Here's an anim with no matching parts.
            if hierarchy_match_flags & PartGroup.HMF_ok_wrong_root_name:
                for anim in anims[ai]:
                    extra_anims.add(anim)
            animsKeys.pop(0)
        elif pi < ai:
            # And here's a part with no matching anims.
            if hierarchy_match_flags & PartGroup.HMF_ok_wrong_root_name:
                for part in parts[pi]:
                    extra_parts.add(part)
            partsKeys.pop(0)
        else:
            # But here we have (at least one) match!
            bind_anims(parts[pi], anims[ai], controls, hierarchy_match_flags)
            partsKeys.pop(0)
            # We don't increment the anim counter yet.  That way, the same anim
            # may bind to multiple parts, if they all share the same name.
    if hierarchy_match_flags & PartGroup.HMF_ok_wrong_root_name:
        # Continue searching through the remaining anims and parts.
        while animsKeys:
            ai = animsKeys[0]
            # Here's an anim with no matching parts.
            if hierarchy_match_flags & PartGroup.HMF_ok_wrong_root_name:
                for anim in anims[ai]:
                    extra_anims.add(anim)
            animsKeys.pop(0)
        while partsKeys:
            pi = partsKeys[0]
            # And here's a part with no matching anims.
            if hierarchy_match_flags & PartGroup.HMF_ok_wrong_root_name:
                for part in parts[pi]:
                    extra_parts.add(part)
            partsKeys.pop(0)
        bind_anims(extra_parts, extra_anims, controls, hierarchy_match_flags)


class BVHAnimControl(object):
    '''
    BVHAnimControl.
    '''

    class FrameFormat(IntEnum):
        TRANS = auto()
        SQT = auto()

    class _BVHModelNode(object):
        def __init__(self, nodePath):
            self._nodePath = nodePath
            self._children = []

        def get_children(self):
            return self._children

        @property
        def nodePath(self):
            return self._nodePath

        def addChild(self, modelNode):
            self._children.append(modelNode)

        def __str__(self):
            return self._nodePath.get_name()

    def __init__(self, fileToOpen, frameDataFormat=FrameFormat.TRANS,
                 raiseOnError=True, app=None, debug=False):
        # get bvhData informations
        self._parser = BVHParser(fileToOpen, raiseOnError=raiseOnError)
        self._hierarchy = self._parser.parse()
        # create skeleton model
        self._skeletons = [skel for _,
                           skel in self._hierarchy.skeletons.items()]
        self._currSkeleton = self._skeletons[0]
        self._roots = {skel.name: skel.root for skel in self._skeletons}
        # some data useful for rendering
        self._jointModel = procedurallyGenerating3DModels(
            cubeVertices, cubeTriangles, 12, name='jointModel', color=(0, 1, 1, 1), scale=1.0)
        self._jointModel.set_scale(0.3)
        self._boneModel = procedurallyGenerating3DModels(
            cubeVertices, cubeTriangles, 12, name='boneModel', color=(1, 0, 1, 1), scale=1.0)
        self._textScale = 0.2
        # create the model nodes
        self._modelNodes = self._createSkeletonModels(debug=debug)
        if app:
            for modelNode in self._modelNodes.values():
                modelNode.nodePath.reparent_to(app.render)
        # storage for frames in SQT format: that is a sequence of tuples each of
        # which is a triple (s, q, t) corresponding to one of the joint, in the
        # general case, while it is a pair (q, t) in the case of BVH (since
        # scaling is not used)
        self._framesSQT = {
            skel.name: [None] * self._hierarchy.frames for skel in self._skeletons}
        # storage for frames in transform state format: that is a items each of
        # which is a TransformState instance corresponding to one of the joint
        self._framesTrans = {
            skel.name: [None] * self._hierarchy.frames for skel in self._skeletons}
        # compute and store the transforms for each frame in both formats
        self._computeStoreFramesData(debug=debug)
        self._frameDataFormat = frameDataFormat
        # In the BVH hierarchy, the world space is defined as a right handed
        # coordinate system with the Y axis as the world up vector.
        self._changeOfCoordTrans = TransformState.make_hpr(
            LVecBase3f(0, 90, 0))
        # data
        self._numFrames = self._hierarchy.frames
        self._frameRate = self._hierarchy.frameRate
        # default pose function
        self._poseFunc = self._renderIntFrame
        # some infos on screen
        textScale = 0.035
        parent = app.aspect2d  # app.render2d
        self._currFrameText = drawText(
            parent, 'frame: {:.3f}'.format(0.0), pos=LPoint3f(0.6, 0, -0.7), textScale=textScale * 1.2)
        self._currPoseText = drawText(parent, 'pose: ' + str(
            self._poseFunc.__name__) + '()', pos=LPoint3f(0.6, 0, -0.77), textScale=textScale)
        self._currFrameDataFormatText = drawText(parent, 'data format: ' + str(
            self._frameDataFormat), pos=LPoint3f(0.6, 0, -0.82), textScale=textScale)

    def get_num_frames(self):
        return self._numFrames

    def get_frame_rate(self):
        return self._frameRate

    @property
    def pose(self):
        return self._poseFunc

    @pose.setter
    def pose(self, poseFunc):
        if isinstance(poseFunc, Callable):
            self._poseFunc = poseFunc
            self._currPoseText.set_text(
                'pose: ' + str(poseFunc.__name__) + '()')
            if self._poseFunc == self._renderFloatFrameSQT:
                self.frameDataFormat = BVHAnimControl.FrameFormat.SQT

    @property
    def frameDataFormat(self):
        return self._frameDataFormat

    @frameDataFormat.setter
    def frameDataFormat(self, frameDataFormat):
        if ((self._poseFunc == self._renderFloatFrameSQT) and
                (frameDataFormat == BVHAnimControl.FrameFormat.TRANS)):
            # _renderFloatFrameSQT is compatible only with SQT format
            return
        self._frameDataFormat = frameDataFormat
        self._currFrameDataFormatText.set_text('data format: ' + str(
            self._frameDataFormat))

    @property
    def hierarchy(self):
        return self._hierarchy

    @property
    def currentSkeleton(self):
        return self._currSkeleton

    @currentSkeleton.setter
    def currentSkeleton(self, nameOrIdx):
        self._currSkeleton = self._hierarchy.getSkeleton(nameOrIdx)

    @property
    def currentModelNode(self):
        return self._modelNodes[self._currSkeleton.name]

    @property
    def modelNodes(self):
        return self._modelNodes

    def getModelNode(self, idx):
        return self._modelNodes[self._skeletons[idx].name]

    def _renderFloatFrameSQT(self, fframe, debug=False):
        '''Renders a hierarchy of panda nodes at a given float frame.

        Uses SQT data format.
        LERP Interpolation between frames.
        '''

        def _preOrderAction(node, l):
            nonlocal self, frameStart, frameStop, sqtTupleIdx, rootName, beta, debug
            name = node.nodePath.node().get_name()
            if debug:
                print('-' * l + '>', name, l)
            quatStart, translStart = frameStart[sqtTupleIdx[0]]
            quatStop, translStop = frameStop[sqtTupleIdx[0]]
            translation = translStart * (1.0 - beta) + translStop * beta
            quaternion = quatStart * (1.0 - beta) + quatStop * beta
            quaternion.normalize()
            sqtTupleIdx[0] += 1
            # apply the transform to the panda node
            node.nodePath.node().set_transform(TransformState.make_pos_quat_scale(
                translation, quaternion, LVecBase3f(1.0)))
            if name != rootName:
                self._drawBone(node.nodePath)

        if fframe < 0:
            fframe = 0
        currFrame = fframe % self._numFrames
        # compute beta (lerp) and start integral frame
        beta, fframeStartIdx = modf(currFrame)
        frameStart = self._framesSQT[self._currSkeleton.name][int(
            fframeStartIdx)]
        # compute end integral frame
        frameStop = self._framesSQT[self._currSkeleton.name][int(
            (fframe + 1) % self._numFrames)]
        sqtTupleIdx = [0]
        rootName = self._currSkeleton.root.name
        modelNode = self._modelNodes[self._currSkeleton.name]
        leftToRightDepthFirstTraversal(
            modelNode, action=_preOrderAction, preorder=True)
        # perform change of coordinates transformation
        trans = modelNode.nodePath.node().get_transform()
        modelNode.nodePath.node().set_transform(self._changeOfCoordTrans.compose(trans))
        # print frame
        self._currFrameText.set_text('frame: {:.3f}'.format(currFrame))

    def _renderIntFrame(self, frameIdx, debug=False):
        '''Renders a hierarchy of panda nodes at a given integer frame.

        Uses the current frame data format (TransformState, SQT).
        No interpolation between frames.
        '''

        def _preOrderAction(node, l):
            nonlocal self, frame, rootName, sqtTupleIdx, debug
            name = node.nodePath.node().get_name()
            if debug:
                print('-' * l + '>', name, l)
            frameDataFormat = frame[sqtTupleIdx[0]]
            if self._frameDataFormat == BVHAnimControl.FrameFormat.TRANS:
                # apply the transform: frameDataFormat = TransformState
                # instance
                node.nodePath.node().set_transform(frameDataFormat)
            elif self._frameDataFormat == BVHAnimControl.FrameFormat.SQT:
                # apply the transform: frameDataFormat = (quaternion,
                # translation)
                node.nodePath.node().set_transform(TransformState.make_pos_quat_scale(
                    frameDataFormat[1], frameDataFormat[0], LVecBase3f(1.0)))
            sqtTupleIdx[0] += 1
            if name != rootName:
                self._drawBone(node.nodePath)

        if frameIdx < 0:
            frameIdx = 0
        currFrame = int(frameIdx % self._numFrames)
        if self._frameDataFormat == BVHAnimControl.FrameFormat.TRANS:
            frame = self._framesTrans[self._currSkeleton.name][currFrame]
        elif self._frameDataFormat == BVHAnimControl.FrameFormat.SQT:
            frame = self._framesSQT[self._currSkeleton.name][currFrame]
        sqtTupleIdx = [0]
        rootName = self._currSkeleton.root.name
        modelNode = self._modelNodes[self._currSkeleton.name]
        leftToRightDepthFirstTraversal(
            modelNode, action=_preOrderAction, preorder=True)
        # perform change of coordinates transformation
        trans = modelNode.nodePath.node().get_transform()
        modelNode.nodePath.node().set_transform(self._changeOfCoordTrans.compose(trans))
        # print frame
        self._currFrameText.set_text('frame: {:.3f}'.format(currFrame))

    def _createSkeletonModels(self, debug=False):
        '''Creates a hierarchy of panda nodes (without transformations).'''

        def _postOrderAction(node, l):
            nonlocal self, rootName, debug
            if debug:
                print('-' * l + '>', node, l)
            # create a new model node (wrapping a panda node)
            modelNode = BVHAnimControl._BVHModelNode(NodePath(node.name))
            self._jointModel.instance_to(modelNode.nodePath)
            drawText(modelNode.nodePath, modelNode.nodePath.get_name(
            ), pos=0.15, color=LVecBase4f(1, 1, 0.5, 1), textScale=self._textScale)
            if node.name != rootName:
                boneNode = modelNode.nodePath.attach_new_node(
                    node.name + '_bone')
                self._boneModel.instance_to(boneNode)
                ratio = 0.95
                lenght = sqrt(sum(map(lambda x: x**2, node.offset)))
                scaleH = lenght * ratio
                boneNode.set_scale(LVecBase3f(0.5, scaleH, 0.5))
            # add an attribute to node referring to nodePath
            setattr(node, 'modelNode', modelNode)
            for child in node.children:
                # append child to this model node (and re-parent the panda
                # node)
                modelNode.addChild(child.modelNode)
                child.modelNode.nodePath.reparent_to(modelNode.nodePath)
                # remove the attribute from child referring to the NodePath
                # (to avoid circular references)
                delattr(child, 'modelNode')

        modelNodes = {}
        # HACK: add an method alias for class Node, so we can use
        # leftToRightDepthFirstTraversal to traverse it
        setattr(Node, 'get_children', lambda self: self._children)
        for rootName, root in self._roots.items():
            leftToRightDepthFirstTraversal(
                root, action=_postOrderAction, preorder=False)
            modelNodes[rootName] = root.modelNode
            delattr(root, 'modelNode')
        delattr(Node, 'get_children')
        return modelNodes

    def _computeStoreFramesData(self, debug=False):
        '''Compute frames' data in SQT format.'''

        def _preOrderAction(node, l):
            nonlocal self, frameSQT, frameTrans, frame, channelIdxStart, skeleton, debug
            name = node.nodePath.node().get_name()
            if debug:
                print('-' * l + '>', name, l)
            joint = skeleton.getJoint(name)
            if not joint:
                # this must be root
                joint = skeleton.root
            offset = joint.offset
            channels = joint.channels
            channelsNum = len(channels)
            channelIdxStop = channelIdxStart[0] + channelsNum - 1
            rotateTrans = TransformState.make_identity()
            translateTrans = TransformState.make_identity()
            # cycle in reverse
            for i in range(channelIdxStop, channelIdxStart[0] - 1, -1):
                data = frame[i]
                channel = channels[i - channelIdxStart[0]]
                if channel == ChannelType.XROT:
                    rotateTrans = TransformState.make_hpr(
                        LVecBase3f(0, data, 0)).compose(rotateTrans)
                elif channel == ChannelType.YROT:
                    rotateTrans = TransformState.make_hpr(
                        LVecBase3f(0, 0, data)).compose(rotateTrans)
                elif channel == ChannelType.ZROT:
                    rotateTrans = TransformState.make_hpr(
                        LVecBase3f(data, 0, 0)).compose(rotateTrans)
                elif channel == ChannelType.XPOS:
                    translateTrans = TransformState.make_pos(
                        LPoint3f(data, 0, 0)).compose(translateTrans)
                elif channel == ChannelType.YPOS:
                    translateTrans = TransformState.make_pos(
                        LPoint3f(0, data, 0)).compose(translateTrans)
                elif channel == ChannelType.ZPOS:
                    translateTrans = TransformState.make_pos(
                        LPoint3f(0, 0, data)).compose(translateTrans)
            translateTrans = TransformState.make_pos(
                LPoint3f(*offset)).compose(translateTrans)
            # update index counter
            channelIdxStart[0] += channelsNum
            # store SQT frame format
            trans = translateTrans.compose(rotateTrans)
            frameSQT.append((trans.get_quat(), trans.get_pos()))
            frameTrans.append(trans)

        for skeleton in self._skeletons:
            for frameIdx in range(self._hierarchy.frames):
                frame = skeleton.animation.frames[frameIdx]
                channelIdxStart = [0]
                frameSQT = []
                frameTrans = []
                leftToRightDepthFirstTraversal(
                    self._modelNodes[skeleton.name], action=_preOrderAction, preorder=True)
                self._framesSQT[skeleton.name][frameIdx] = frameSQT
                self._framesTrans[skeleton.name][frameIdx] = frameTrans

    def _drawBone(self, nodePath):
        parentNP = nodePath.get_parent()
        if parentNP.is_empty():
            return
        boneNP = nodePath.find(nodePath.get_name() + '_bone')
        boneNP.look_at(parentNP)


def drawText(nodePath, name, pos=0.0, hpr=0.0, textScale=0.1,
             color=LVecBase4f(1, 1, 1, 1), drawBin=50):
    nodeText = nodePath.attach_new_node(name + '_text')
    nodeText.set_pos_hpr(pos, hpr)
    text = TextNode(name + '_TextNode')
    text.set_text(name)
    text.set_card_color(color)
    text.set_card_as_margin(0, 0, 0, 0)
    text.set_card_decal(True)
    textNodePath = nodeText.attach_new_node(text)
    textNodePath.set_scale(textScale)
    textNodePath.set_color(0, 0, 0)
    textNodePath.set_bin('fixed', drawBin)
    textNodePath.set_depth_write(False)
    textNodePath.set_depth_test(False)
    textNodePath.set_billboard_point_eye()
    return text


cubeVertices = [-0.5, 0, -0.5,  # 0
                0.5, 0, -0.5,  # 1
                -0.5, 1, -0.5,  # 2
                0.5, 1, -0.5,  # 3
                -0.5, 0, 0.5,  # 4
                0.5, 0, 0.5,  # 5
                -0.5, 1, 0.5,  # 6
                0.5, 1, 0.5, ]  # 7
cubeTriangles = [0, 2, 1,
                 1, 2, 3,
                 4, 5, 6,
                 5, 7, 6,
                 0, 4, 2,
                 2, 4, 6,
                 1, 3, 5,
                 3, 7, 5,
                 2, 6, 7,
                 2, 7, 3,
                 0, 1, 5,
                 0, 5, 4, ]


def procedurallyGenerating3DModels(vertices, triangles, ntriangles,
                                   name='gnode', color=(0, 0, 1, 1),
                                   scale=1.0):
    # # Defining your own GeomVertexFormat
    array = GeomVertexArrayFormat()
    array.add_column(InternalName.make('vertex'), 3, Geom.NT_float32,
                     Geom.C_point)
    array.add_column(InternalName.make('normal'), 3, Geom.NT_float32,
                     Geom.C_normal)
    #
    unregistered_format = GeomVertexFormat()
    unregistered_format.add_array(array)
    #
    format1 = GeomVertexFormat.register_format(unregistered_format)
    # # Creating and filling a GeomVertexData
    vdata = GeomVertexData(name + 'VD', format1, Geom.UH_static)
    #
    numVerts = 0
    i = 0
    for i in range(0, ntriangles * 3, 1):
        numVerts = max(triangles[i], numVerts)
    numVerts += 1
    vdata.set_num_rows(numVerts)
    #
    vertexRW = GeomVertexRewriter(vdata, 'vertex')
    normalRW = GeomVertexRewriter(vdata, 'normal')
    #
    # vertices in panda3d space reset normal
    for i in range(0, numVerts * 3, 3):
        p3dvert = LPoint3f(vertices[i], vertices[i + 1], vertices[i + 2])
        vertexRW.add_data3f(p3dvert)
        normalRW.add_data3f(LVector3f.zero())
    # # Creating the GeomPrimitive objects and compute normals per vertex
    # # note: for each face the normal is computed and added to each normal of
    # # its vertices normals are not normalized at this stage, so a vertex
    # # normal is more influenced by triangles with wider area. All vertex
    # # normals are normalized all together in a second step.
    prim = GeomTriangles(Geom.UH_static)
    for i in range(0, ntriangles * 3, 3):
        i0 = triangles[i]
        i1 = triangles[i + 1]
        i2 = triangles[i + 2]
        prim.add_vertex(i0)
        prim.add_vertex(i1)
        prim.add_vertex(i2)
        # compute face normal (not normalized): (p1-p0)X(p2-p0)
        vertexRW.set_row(i0)
        p0 = vertexRW.get_data3f()
        vertexRW.set_row(i1)
        p1 = vertexRW.get_data3f()
        vertexRW.set_row(i2)
        p2 = vertexRW.get_data3f()
        faceNorm = (p1 - p0).cross(p2 - p0)
        # add faceNorm to each vertex normal (not normalized)
        normalRW.set_row(i0)
        n0 = normalRW.get_data3f()
        normalRW.set_row(i0)
        normalRW.set_data3f(n0 + faceNorm)
        #
        normalRW.set_row(i1)
        n1 = normalRW.get_data3f()
        normalRW.set_row(i1)
        normalRW.set_data3f(n1 + faceNorm)
        #
        normalRW.set_row(i2)
        n2 = normalRW.get_data3f()
        normalRW.set_row(i2)
        normalRW.set_data3f(n2 + faceNorm)
    prim.close_primitive()
    # normalize all vertex normals
    normalRW.set_row(0)
    for i in range(0, numVerts, 1):
        normalRW.set_row(i)
        n = LVector3f(normalRW.get_data3f())
        n.normalize()
        normalRW.set_row(i)
        normalRW.set_data3f(n)
    # Putting your new geometry in the scene graph
    geom = Geom(vdata)
    geom.add_primitive(prim)
    node = GeomNode(name)
    node.add_geom(geom)
    #
    modelNP = NodePath.any_path(node)
    modelNP.set_color(color)
    modelNP.set_scale(scale)
    return modelNP


if __name__ == '__main__':
    # tests of tree traversals
    # array-based tree representation
    treeRepr = [(1, 3), (2, 3), (6, 2), (9, 2), (3, 0), (4, 0),
                (5, 0), (7, 0), (8, 0), (10, 2), (13, 0), (11, 0), (12, 0)]
    rootTreeNode = buildTree(treeRepr)
    leftToRightBreadthFirstTraversal(rootTreeNode, lambda n, l: print(n, l))
    leftToRightDepthFirstTraversal(
        rootTreeNode, action=lambda n, l: print(n, l), preorder=True)
    leftToRightDepthFirstTraversal(
        rootTreeNode, action=lambda n, l: print(n, l), preorder=False)
