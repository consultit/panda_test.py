'''
Created on Dec 15, 2019

@author: consultit
'''

from panda3d.core import LVector3f

ikData = {
    'john':
    {
        # multiple effectors/targets
        #         'targetScale' : 0.5,
        #         'jointBaseParent' : 'Hips',
        #         'jointHierarchyArray' : [
        #             {'name':'Spine', 'children': 1,
        #              'upAxis':LVector3f(0, -1, 0),
        #              'baseForwardAxis': LVector3f(0, 0, 1)},
        #             {'name':'Spine1', 'children': 1,
        #              'upAxis':LVector3f(0, -1, 0),
        #              'angles':(15, 15, 15, 15),
        #              'w':1.0}, #
        #             {'name':'Spine2', 'children': 1,
        #              'upAxis':LVector3f(0, -1, 0),
        #              'angles':(15, 15, 15, 15),
        #              'w':1.0}, #
        #             {'name':'Spine3', 'children': 3,
        #              'upAxis':LVector3f(0, -1, 0),
        #              'angles':(15, 15, 15, 15),
        #              'w':1.0}, #
        #             {'name':'LeftShoulder', 'children': 1,
        #              'upAxis':LVector3f(0, -1, 0),
        #              'angles':(15, 60, 15, 0),#(5, 170, 5, 0),
        #              'w':1.0}, #
        #             {'name':'Neck', 'children': 1,
        #              'upAxis':LVector3f(0, -1, 0),
        #              'angles':(30, 30, 15, 30),#(5, 0, 5, 170),
        #              'w':1.0}, #
        #             {'name':'RightShoulder', 'children': 1,
        #              'upAxis':LVector3f(0, -1, 0),
        #              'angles':(15, 0, 15, 60),#(35, 15, 35, 15),
        #              'w':1.0}, #
        #             {'name':'LeftArm', 'children': 1,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(30, 90, 15, 0),#(35, 15, 35, 15),
        #              'w':1.0}, #
        #             {'name':'Head', 'children': 0,
        #              'upAxis':LVector3f(0, -1, 0),
        #              'angles':(45, 45, 35, 45),
        #              'w':1.0}, #
        #             {'name':'RightArm', 'children': 1,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(30, 0, 15, 90),
        #              'w':1.0}, #
        #             {'name':'LeftArmRoll', 'children': 1,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(60, 80, 90, 5),
        #              'w':1.0}, #
        #             {'name':'RightArmRoll', 'children': 1,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(60, 5, 90, 80),
        #              'w':1.0}, #
        #             {'name':'LeftForeArm', 'children': 1,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(5, 5, 5, 5),
        #              'w':1.0}, #
        #             {'name':'RightForeArm', 'children': 1,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(5, 5, 5, 5),
        #              'w':1.0}, #
        #             {'name':'LeftForeArmRoll', 'children': 1,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(5, 120, 5, 5),
        #              'w':1.0}, #
        #             {'name':'RightForeArmRoll', 'children': 1,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(5, 5, 5, 120),
        #              'w':1.0}, #
        #             {'name':'LeftHand', 'children': 0,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(5, 5, 5, 5),
        #              'w':1.0}, #
        #             {'name':'RightHand', 'children': 0,
        #              'upAxis':LVector3f(0, 0, 1),
        #              'angles':(5, 5, 5, 5),
        #              'w':1.0}, #
        #         ],
        # left leg
        'targetScale': 0.5,
        'jointBaseParent': 'Hips',
        'jointHierarchyArray': [
            {'name': 'LeftUpLeg', 'children': 1,
             'upAxis': LVector3f(0, -1, 0),
             'baseForwardAxis': LVector3f(0, 0, -1)},
            {'name': 'LeftUpLegRoll', 'children': 1,
             'upAxis': LVector3f(0, -1, 0),
             #'endEffector':True,
             'angles': (45, 120, 30, 0),  # (35, 15, 35, 15),
             'w': 1.0},
            {'name': 'LeftLeg', 'children': 1,
             'upAxis': LVector3f(0, -1, 0),
             #'endEffector':True,
             'angles': (89, 15, 40, 15),
             'w': 1.0},
            {'name': 'LeftLegRoll', 'children': 1,
             'upAxis': LVector3f(0, -1, 0),
             #'endEffector':True,
             'angles': (5, 5, 150, 5),
             'w': 1.0},
            {'name': 'LeftFoot', 'children': 1,
             'upAxis': LVector3f(0, -1, 0),
             #'endEffector':True,
             'angles': (5, 5, 5, 5),
             'w': 1.0},
            {'name': 'LeftToeBase', 'children': 0,
             'upAxis': LVector3f(0, -1, 0),
             'angles': (95, 20, 0, 20),
             'w': 1.0},
        ],
    },
    'ralph': {
        # left arm
        'targetScale': 0.2,
        'jointBaseParent': 'Chest',
        'jointHierarchyArray': [
            {'name': 'LeftShoulder', 'children': 1,
             'upAxis': LVector3f(0, 0, 1),
             'baseForwardAxis': LVector3f(1, 0, -1)},
            {'name': 'LeftElbow', 'children': 1,
             'upAxis': LVector3f(0, 0, 1),
             #'endEffector':True,
             'angles': (120, 120, 120, 120),
             'w': 1.0},
            {'name': 'LeftWrist', 'children': 1,
             'upAxis': LVector3f(0, 0, 1),
             #'endEffector':True,
             'angles': (120, 120, 120, 120),
             'w': 1.0},
            {'name': 'LeftHand', 'children': 1,
             'upAxis': LVector3f(0, 0, 1),
             'angles': (120, 120, 120, 120),
             'w': 1.0},
        ],
    },
}
