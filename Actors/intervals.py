'''
Created on Sep 16, 2017

@author: consultit
'''

from panda3d.core import load_prc_file, load_prc_file_data, Filename, \
    LPoint3f, LVector3f, WindowProperties

import sys
import os
import random
from direct.showbase.ShowBase import ShowBase
from direct.interval import IntervalGlobal
from direct.interval.IntervalGlobal import *
from actorsAnim import loadActor
import argparse
import textwrap

# Get the location and Convert that to panda's unix-style notation.
datadir = os.path.abspath(sys.path[0])
datadir = os.path.join(datadir, '..')
datadir = Filename.fromOsSpecific(datadir).getFullpath()

load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')

if __name__ == '__main__':
    intervalChoises = [item for item in dir(IntervalGlobal)
                       if 'interval' in item.lower()]
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test Actor animations
      
    Commands:
        - '': .
        - '': .
    '''))
    # set up arguments
    parser.add_argument('model', type=str, choices=['john', 'ralph', 'pilot'],
                        help='the actor\'s model')
    parser.add_argument('--interval-type', type=str,
                        choices=intervalChoises,
                        help='the animations\' play rate')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    # set model dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    app = ShowBase()
    msg = ''

    # get an actor
    actor = loadActor(args.model)
    actor.reparent_to(app.render)

    def getAnimToFrame(animFromFrame):
        global animData
        animFromLen = float(
            animData['actor'].get_num_frames(animData['animFrom']))
        animToLen = float(animData['actor'].get_num_frames(animData['animTo']))
        animToFrame = int(animFromFrame * animToLen / animFromLen)
        return min(animToFrame, animToLen)

    animData = {'actor': actor, 'animFrom': 'walk', 'animTo': 'run',
                'animTo1': 'jump',
                'animToFrame': getAnimToFrame,
                'animPlayRates': None}

    # # base Interval: useless
    if args.interval_type == 'Interval':
        msg = 'base Interval'
        interval = Interval('Interval', 10, openEnded=1)
        interval.done_event = 'interval.done_event'

        def doneEvent(_interval):
            print(_interval, 'done')

        app.accept('interval.done_event', doneEvent, [interval])
        interval.start()

    # # LerpInterval
    elif args.interval_type == 'LerpPosInterval':
        msg = 'LerpPosInterval'

        def newPos():
            D = 20
            R = random.random
            return LPoint3f(D * (2 * R() - 1), D * (2 * R() - 1), D * (2 * R() - 1))

        lerpPosInt = LerpPosInterval(actor, 5, newPos(), startPos=None,
                                     other=None, blendType='easeInOut',
                                     bakeInStart=0, fluid=0, name='lerpPosInt')

        def playLater(task):
            global lerpPosInt
            lerpPosInt.set_end_pos(newPos())
            lerpPosInt.start()
            return task.again

        app.taskMgr.do_method_later(7, playLater, 'playLater')
        lerpPosInt.start()

    # # PositionInterval
    elif args.interval_type == 'PosInterval':
        msg = 'PosInterval'
        actor.posInterval(5.0, LPoint3f(2, -3, 8),
                          startPos=LPoint3f(2, 4, 1)).start()

    # # ProjectileInterval
    elif args.interval_type == 'ProjectileInterval':
        msg = 'ProjectileInterval'
        projectileInterval = ProjectileInterval(actor,
                                                startPos=LPoint3f(-40, 0, 0),
                                                endPos=LPoint3f(40, 0, 0),
                                                duration=5,
                                                startVel=None,
                                                endZ=None,
                                                wayPoint=None,
                                                timeToWayPoint=None,
                                                gravityMult=1,
                                                name='projectileInterval',
                                                collNode=None)
        projectileInterval.loop()

    # # LerpFunctionInterval
    elif args.interval_type == 'LerpFunctionInterval':
        msg = 'LerpFunctionInterval'
        # # animation's transition
        actor.enable_blend()
        actor.setControlEffect('chargeshoot', 0.2)
        actor.setControlEffect('newdeath', 0.8)
        actor.loop('chargeshoot')
        actor.loop('newdeath')

        def animTransition(controlEffect, animData):
            animData['actor'].set_control_effect(
                animData['animFrom'], 1.0 - controlEffect)
            animData['actor'].set_control_effect(
                animData['animTo'], controlEffect)
            if animData['animPlayRates'] != None:
                PFrom = animData['animPlayRates'][0]
                PTo = animData['animPlayRates'][1]
                playRateFrom = 1.0 + controlEffect * (PTo - 1.0)
                playRateTo = PFrom + controlEffect * (1.0 - PFrom)
                animData['actor'].set_play_rate(
                    playRateFrom, animData['animFrom'])
                animData['actor'].set_play_rate(playRateTo, animData['animTo'])

        animTransitionInterval = LerpFunctionInterval(animTransition,
                                                      duration=10.0,
                                                      fromData=0.0,
                                                      toData=1.0,
                                                      blendType='easeInOut',
                                                      extraArgs=[animData],
                                                      name='animTransitionInterval')
        animTransitionInterval.setDoneEvent('animTransitionDone')

        def animTransitionDone():
            global animData, toggleAnimations
            animData['actor'].stop(animData['animFrom'])
            animData['actor'].disable_blend()
            toggleAnimations()

        app.accept('animTransitionDone', animTransitionDone)

        # start transition event
        def getAnimPlayRates():
            global animData
            animFromLen = float(
                animData['actor'].get_num_frames(animData['animFrom']))
            animToLen = float(
                animData['actor'].get_num_frames(animData['animTo']))
            animFromPlayRate = animToLen / animFromLen
            animToPlayRate = animFromLen / animToLen
            return (animFromPlayRate, animToPlayRate)

        def startTransition():
            global animTransitionInterval, animData
            animData['actor'].enable_blend()
            animData['actor'].set_control_effect(animData['animTo'], 0.0)
            animFromFrame = animData['actor'].get_current_frame(
                animData['animFrom'])
            if callable(animData['animToFrame']):
                animToFrame = animData['animToFrame'](animFromFrame)
            else:
                animToFrame = animData['animToFrame']
            animData['animPlayRates'] = getAnimPlayRates()
            animData['actor'].pose(animData['animTo'], animToFrame)
            animData['actor'].loop(animData['animTo'], restart=False)
#             animTransitionInterval.duration = 1.0
            animTransitionInterval.start()

        app.accept('s', startTransition)

        # switch animation event
        def toggleAnimations():
            global animData
            animFrom = animData['animFrom']
            animData['animFrom'] = animData['animTo']
            animData['animTo'] = animFrom

        app.accept('t', toggleAnimations)

        # initialization
        animData['actor'].set_play_rate(0.5, animData['animFrom'])
        animData['actor'].set_play_rate(0.5, animData['animTo'])
        animData['actor'].loop(animData['animFrom'])

    # # LerpAnimInterval
    elif args.interval_type == 'LerpAnimInterval':
        msg = 'LerpAnimInterval'
        animData['actor'].enable_blend()
        animData['actor'].loop(animData['animFrom'])
        animData['actor'].loop(animData['animTo'])
        i1 = Sequence(LerpAnimInterval(animData['actor'], 10, animData['animFrom'], animData['animTo']),
                      Wait(3),
                      LerpAnimInterval(
                          animData['actor'], 10, animData['animTo'], animData['animFrom']),
                      Wait(3))
        # i1.loop()

        i2 = Sequence(ActorInterval(animData['actor'], animData['animFrom'], loop=1, duration=3),
                      LerpAnimInterval(
                          animData['actor'], 0.5, animData['animFrom'], animData['animTo1']),
                      ActorInterval(animData['actor'], animData['animTo1']),
                      LerpAnimInterval(
                          animData['actor'], 0.5, animData['animTo1'], animData['animFrom']),
                      ActorInterval(animData['actor'], animData['animFrom'], loop=1, duration=3))
        i2.loop()

    elif args.interval_type:
        msg = args.interval_type + ' not yet implemented'

    props = WindowProperties()
    props.setTitle(msg)
    app.win.requestProperties(props)

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 80.0, -2.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
