'''
Created on Apr 24, 2020

@author: consultit
'''

from panda3d.core import load_prc_file_data, LVector3f, DirectionalLight,\
    LPoint3f
from actorsAnim_common import BVHAnimControl, leftToRightDepthFirstTraversal, \
    getDimensions, AnimControlManual, procedurallyGenerating3DModels, \
    cubeVertices, cubeTriangles, drawText
import argparse
import textwrap
import os
from direct.showbase.ShowBase import ShowBase
from panda_test_py.bvh_parser.bvhHierarchy import Node


def main(argv):
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will convert bvh file to Panda3d's egg animation file
    
    Use:
        - 'p' to play animation
        - 's' to play animation frame-by-frame
    '''))
    # set up arguments
    parser.add_argument('bvhfile', type=str,
                        help='the bvh file (with or without extension)')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args(argv)

    # do actions
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    load_prc_file_data('', 'win-size 1024 768')
    app = ShowBase()

    # bvh stuff
    # search the first match of bvhfile
    fileToOpen = None
    for pathDir in args.data_dir:
        testFile = os.path.join(pathDir, args.bvhfile)
        if os.path.exists(testFile):
            fileToOpen = testFile
            break
        else:
            testFile = os.path.join(pathDir, args.bvhfile + '.bvh')
            if os.path.exists(testFile):
                fileToOpen = testFile
                break
    if not fileToOpen:
        raise FileNotFoundError(args.bvhfile)

    # get bvhData informations
    bvhAnimCtrl = BVHAnimControl(
        fileToOpen,   raiseOnError=True, app=app, debug=False)

    # test traversals
    setattr(Node, 'get_children', lambda self: self._children)
    leftToRightDepthFirstTraversal(bvhAnimCtrl.hierarchy.getSkeleton(
        0).root, action=lambda n, l: print('-' * l + '>', n, l), preorder=True)
    leftToRightDepthFirstTraversal(bvhAnimCtrl.hierarchy.getSkeleton(
        0).root, action=lambda n, l: print('-' * l + '>', n, l), preorder=False)
    delattr(Node, 'get_children')

    # a hierarchy could have more than one skeleton
    # panda3d hierarchy
    bvhAnimCtrl.currentModelNode.nodePath.ls()
    # model node hierarchy
    leftToRightDepthFirstTraversal(bvhAnimCtrl.currentModelNode, action=lambda n, l: print(
        '-' * l + '>', n, l), preorder=True)

    # set the pose function
#     bvhAnimCtrl.pose = bvhAnimCtrl._renderFloatFrameSQT
#     bvhAnimCtrl.pose = lambda frame: BVHAnimControl._renderFloatFrameSQT(bvhAnimCtrl, frame)
    bvhAnimCtrl.pose = bvhAnimCtrl._renderIntFrame
    # set the frame data format to use with BVHAnimControl._renderIntFrame
#     bvhAnimCtrl.frameDataFormat = BVHAnimControl.FrameFormat.SQT
    bvhAnimCtrl.frameDataFormat = BVHAnimControl.FrameFormat.TRANS

    # play loop using AnimControlManual
    animCtrlManual = AnimControlManual(app, bvhAnimCtrl)
    animCtrlManual.play_rate = 1.0
    # some infos on screen
    drawText(app.aspect2d, 'press \'p\' to play animation'.format(),
             pos=LPoint3f(-1.3, 0, 0.95), color=(1, 0.7, 0.7, 1), textScale=0.035)
    drawText(app.aspect2d, 'press \'s\' to play animation frame-by-frame'.format(),
             pos=LPoint3f(-1.3, 0, 0.91), color=(1, 0.7, 0.7, 1), textScale=0.035)
    #
    textScale = 0.045
    color = (1, 0.7, 0, 1)
    totalFrames = bvhAnimCtrl.get_num_frames()
    playRate = animCtrlManual.play_rate
    frameRate = playRate * bvhAnimCtrl.get_frame_rate()
    duration = bvhAnimCtrl.hierarchy.duration / playRate
    drawText(app.aspect2d, 'total frames:  {:d}'.format(
        totalFrames), pos=LPoint3f(0.6, 0, 0.9), color=color, textScale=textScale)
    drawText(app.aspect2d, 'play rate:  {:.3f}'.format(
        playRate),             pos=LPoint3f(0.6, 0, 0.85), color=color, textScale=textScale)
    drawText(app.aspect2d, 'frame rate: {:.3f}'.format(
        frameRate), pos=LPoint3f(0.6, 0, 0.8), color=color, textScale=textScale)
    drawText(app.aspect2d, 'duration: {:.3f}'.format(duration), pos=LPoint3f(
        0.6, 0, 0.75), color=color, textScale=textScale)

    # play animation step by step
    def _playAnimStep(animCtrlManual):  # , frameIdx, frameNum):
        animCtrlManual.play_anim(
            startFrame=animCtrlManual.fframe, endFrame=animCtrlManual.fframe)

    app.accept('s', _playAnimStep, extraArgs=[animCtrlManual])

    togglePlay = [True]

    def _playAnim(animCtrlManual, togglePlay):
        if togglePlay[0]:
            animCtrlManual.play_anim(doLoop=True)
        else:
            animCtrlManual.stop_anim()
        togglePlay[0] = not togglePlay[0]

    app.accept('p', _playAnim, extraArgs=[animCtrlManual, togglePlay])

    bvhAnimCtrl.pose(0)

    # add environment
    planeModel = procedurallyGenerating3DModels(
        cubeVertices, cubeTriangles, 12, name='planeModel', color=(0.2, 0.4, 0.5, 1), scale=1.0)
    planeModel.set_scale(200, 200, 1)
    planeModel.set_pos(0, -100, -1)
    planeModel.reparent_to(app.render)

    dirLight = app.render.attach_new_node(DirectionalLight('directionalLight'))
    dirLight.set_hpr(45, -45, 0)
#     dirLight.node().set_shadow_caster(True, 512, 512)
    app.render.set_light(dirLight)
#     app.render.set_shader_auto()
    # default camera positioning
    # get 'tight' dimensions of actor
    _, xC, yC, zC, xL, yL, zL = getDimensions(
        bvhAnimCtrl.currentModelNode.nodePath)
    #
    app.trackball.node().set_origin(LVector3f(xL, yL, zL))
    app.trackball.node().set_pos(-xC + xL, -yC + yL, -zC + zL)
    app.trackball.node().set_hpr(0, 0, 0)

    # run
    app.run()


if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
