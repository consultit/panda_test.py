'''
Created on Feb 24, 2018

Tests about Actor augmented with Ely's features.

@author: consultit
'''

from panda3d.core import load_prc_file_data, BitMask32, OmniBoundingVolume, \
    TextureStage, LVecBase4f, LPoint3f, LVecBase3f
from ely.physics import GamePhysicsManager, BT3CharacterController
from ely.direct.ragdoll import ActorRagDoll
from ely.direct.animTransition import AnimTransition, AnimTransitionParam
from BaseApplication import BaseApplication
from ely.samples.direct.common import addPlane
from testActorFeatures_common import createCharacterController, setRagdollTest
from testActorFeatures_fsm import ActorFSM
import argparse
import textwrap

load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')

# GAME DATA
groupMask = BitMask32(0x10)
collideMask = BitMask32(0x10)
mask = BitMask32(0x10)
cameraPos = LPoint3f(0.0, -60.0, 5.0)
cameraHpr = LVecBase3f(0.0, -5.0, 0.0)
clearColor = LVecBase4f(0.3334, 0.6667, 1.0, 1)
debugMode = True
# callbacks' keys
setupRagdollKey = '1'
resetRagdollKey = '2'
cycleModelAnimationsKey = '3'
toggleModelVisibilityKey = '4'
toggleModelWireframeKey = '5'
toggleCameraDriverKey = 'f1'
toggleEnableCameraDriverKey = 'f2'
debugKey = 'f3'
physicPickingKey = 'f4'
rayTestClosestKey = 'f5'
rayTestAllKey = 'f6'
selectContactObjectKey = 'f7'
contactTestKey = 'f8'
cameraDriverKeys = {'move_forward': 'mouse3', 'move_backward': 'x',
                    'rotate_head_left': 'a', 'rotate_head_right': 'd',
                    'rotate_pitch_up': 'w', 'rotate_pitch_down': 's',
                    'move_strafe_left': 'q', 'move_strafe_right': 'e',
                    'move_up': 'r', 'move_down': 'f', }
actorEventInputMap = {'arrow_up': 'f', 'arrow_up-up': 'f_up', 'shift-arrow_up': 'f_q',
                      'arrow_down': 'b', 'arrow_down-up': 'b_up', 'shift-arrow_down': 'b_q',
                      'arrow_left': 'rl', 'arrow_left-up': 'rl_up', 'shift-arrow_left': 'rl_q',
                      'arrow_right': 'rr', 'arrow_right-up': 'rr_up', 'shift-arrow_right': 'rr_q',
                      '-': 'j', '--up': 'j_up', 'shift--': 'j_q',
                      'shift': 'q', 'shift-up': 'q_up',
                      }

# To create the actor's FSM template from xml specification do:
# from Actors.testActorFeatures_common import createFSM
# actorFSMFile = createFSM('testActorFeatures_fsm.xml', fsmFile='actorFSM_tmpl.py',
#                          className='ActorFSM', defaultTrans=True, inputEvents=True, debug=True)
# test FSM template
# from direct.actor.Actor import Actor
# exec(open(actorFSMFile).read(), globals()) # or: from actorFSM_tmpl import ActorFSMTmpl
# actorFSMTmpl = ActorFSMTmpl(Actor())
# actorFSMTmpl.request('I')
# actorFSMTmpl.setupDebug(app, t)


class ActorApp(BaseApplication):
    '''
    ActorApp
    '''

    def gamePlay(self, actorData, actorZ, actorScale=1.0, **kwArgs):
        '''
        ActorApp GamePlay
        '''

        # call BaseApplication' gamePlay
        super(ActorApp, self).gamePlay(**kwArgs)

        # add a plane to collide with
        plane = addPlane(self, width=500, depth=500,
                         texture='grass_ground2.jpg')
        plane.owner_object.set_tex_scale(TextureStage.default, 100.0, 100.0)

        # read actor data
        exec(open(actorData).read(), globals())
        global data

        # # ACTOR
        # create the actor
        actor = ActorRagDoll(self.physicsMgr, data.jointHierarchyArray,
                             groupMask, collideMask,
                             models=data.models, anims=data.anims)
        actor.reparent_to(self.referenceNode)
        actor.set_z(actorZ)
        actor.node().set_bounds(OmniBoundingVolume())
        actor.node().set_final(True)
        # setup start animation's pose
        actor.pose(*data.animData['startAnimPose'])

        # set ragdoll test
        animTestIdx = [0]
        setRagdollTest(self, actor, setupRagdollKey, resetRagdollKey,
                       cycleModelAnimationsKey, animTestIdx,
                       toggleModelVisibilityKey, toggleModelWireframeKey)

        # To find out speeds of the movement animations do:
#         from panda3d.core import NodePath
#         from Actors.testActorFeatures_common import findOutSpeedsForMovementAnimation
#         findOutSpeedsForMovementAnimation(self, self.physicsMgr, actor, NodePath.any_path(plane),
#                                           'LeftToe', 'walk',
#                                           playRate=0.2, ghostDims=(0.1, 0.1, 1.0),
#                                           planeHeight=-0.9, planeLiftRes=0.01,
#                                           carpetRay=50.0, carpetHeight=0.5,
# carpetTex='chessboard.png', carpetTexScale=5.0)

        # # ACTOR's CHARACTER CONTROLLER
        # get the real actor to use with the character controller
        actorReal, actorDims, actorDeltaCenter, actorRadius = data.getRealActor(
            actor, self.referenceNode)

        # create actor's character controller
        actorCtrl = createCharacterController(actorReal, self.physicsMgr,
                                              GamePhysicsManager.CAPSULE)
        # setup actorCtrl'event emission
        actorCtrl.enable_throw_event(
            BT3CharacterController.INAIR, True, 0.1, 'INAIR')
        actorCtrl.enable_throw_event(
            BT3CharacterController.ONGROUND, True, 0.1, 'ONGROUND')

        self.accept('INAIR', self.actorCtrlEventNotify, ['INAIR'])
        self.accept('ONGROUND', self.actorCtrlEventNotify, ['ONGROUND'])

        # setup animations' transitions (after creation of actorCtrl)
        class LerpValue(object):
            def __init__(self, f, actorCtrl, deltaSpeedInv, ctrlData):
                self.f = f
                self.actorCtrl = actorCtrl
                self.deltaSpeedInv = deltaSpeedInv
                self.ctrlData = ctrlData

            def __call__(self):
                return self.f(self.actorCtrl, self.deltaSpeedInv, self.ctrlData)

        animTrans = {}
        for name in data.animTransData:
            tData = data.animTransData[name]
            if tData['type'] == 'param':
                trans = AnimTransitionParam(app=self, actor=actor,
                                            animFrom=tData['animFrom'],
                                            animTo=tData['animTo'])
            elif tData['type'] == 'time':
                trans = AnimTransition(app=self, actor=actor,
                                       animFrom=tData['animFrom'],
                                       animTo=tData['animTo'])
            trans.animToFrame = tData['animToFrame']
            trans.playRate = tData['playRate']
            trans.playRateRatios = tData['playRateRatios']
            trans.usePose = tData['usePose']
            trans.loop = tData['loop']
            trans.duration = tData['duration']
            trans.doneEvent = tData['doneEvent']
            if tData['lerpValue']:
                trans.lerpValue = LerpValue(tData['lerpValue'], actorCtrl,
                                            data.deltaSpeedInv,
                                            data.ctrlData)
            animTrans[name] = trans

        # # ACTOR's FSM
        # create FSM
        actorFSM = ActorFSM(actor, actorCtrl, animTrans, data.ctrlData)
        # go to initial state
        actorFSM.request(data.ctrlData['initState'])
        # setup driving events for the actor
        actorFSM.setupInputEvents(self, actorEventInputMap)

    def actorCtrlEventNotify(self, name, object0):
        '''character controller event notify'''

        print('got \'' + name + '\' for \'' +
              object0.get_name() + '\' character controller.')


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script performs tests about Actor augmented with several features of Ely.
      
    Commands:
        - \'''' + setupRagdollKey + '''\': to setup the ragdoll.
        - \'''' + resetRagdollKey + '''\': to reset the ragdoll.
        - \'''' + cycleModelAnimationsKey + '''\': to cycle model animations.
        - \'''' + toggleModelVisibilityKey + '''\': to toggle model visibility.
        - \'''' + toggleModelWireframeKey + '''\': to toggle model wireframe mode.
        - \'''' + debugKey + '''\': to toggle physics' debug drawings.
        - \'''' + physicPickingKey + '''\': to pick-and-drag physic bodies with the mouse hold the key.        
        - \'''' + rayTestClosestKey + '''\': to perform physics' ray test closest. 
        - \'''' + rayTestAllKey + '''\': to perform physics' ray test all. 
        - \'''' + selectContactObjectKey + '''\': to select physics' contact object.
        - \'''' + contactTestKey + '''\': to perform physics' contact test.
        - \'''' + toggleCameraDriverKey + '''\': to toggle camera's driver type: 'Panda3d default' | 'Free style view'.

    Commands for 'Free style view' camera movement: 
        - \'''' + toggleEnableCameraDriverKey + '''\': to enable/disable driver.
        - \'''' + cameraDriverKeys['move_forward'] + '''\': to move forward. 
        - \'''' + cameraDriverKeys['move_backward'] + '''\': to move backward.
        - \'''' + cameraDriverKeys['rotate_head_left'] + '''\': to rotate head left. 
        - \'''' + cameraDriverKeys['rotate_head_right'] + '''\': to rotate head right.
        - \'''' + cameraDriverKeys['rotate_pitch_up'] + '''\': to rotate pitch up.
        - \'''' + cameraDriverKeys['rotate_pitch_down'] + '''\': to rotate pitch down.
        - \'''' + cameraDriverKeys['move_strafe_left'] + '''\': to move strafe left. 
        - \'''' + cameraDriverKeys['move_strafe_right'] + '''\': to move strafe right.
        - \'''' + cameraDriverKeys['move_up'] + '''\': to move up.
        - \'''' + cameraDriverKeys['move_down'] + '''\': to move down.
    '''))

    # set up arguments
    parser.add_argument('actorData', type=str,
                        help='the actor model data path')
    parser.add_argument('-s', '--scale', type=float,
                        default=1.0, help='the actor scale')
    parser.add_argument('-z', '--z-coord', type=float, default=0.0,
                        help='the actor''s z coordinate')
    parser.add_argument('-d', '--data-dir', type=str,
                        action='append', help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()
    # do actions
    app = ActorApp(title='testActorFeatures', useRenderPipeline=False,
                   daytime=None, dataDirs=args.data_dir, mask=mask,
                   groupMask=groupMask, collideMask=collideMask,
                   debugMode=debugMode)
    app.setup()
    app.gamePlay(args.actorData, args.z_coord, args.scale, cameraPos=cameraPos,
                 cameraHpr=cameraHpr, clearColor=clearColor,
                 toggleCameraDriverKey=toggleCameraDriverKey,
                 toggleEnableCameraDriverKey=toggleEnableCameraDriverKey,
                 cameraDriverKeys=cameraDriverKeys, debugKey=debugKey,
                 physicPickingKey=physicPickingKey,
                 rayTestClosestKey=rayTestClosestKey,
                 rayTestAllKey=rayTestAllKey,
                 selectContactObjectKey=selectContactObjectKey,
                 contactTestKey=contactTestKey)
    app.run()
