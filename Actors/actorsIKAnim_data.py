'''
Created on Dec 16, 2019

@author: consultit
'''

from panda3d.core import LVector3f


class Data:
    pass


data = Data()

# # model data
data.modelName = 'john'
data.models = 'john.egg'
data.anims = {'run': 'john-run.egg', 'walk': 'john-walk.egg'}
data.animData = {'startAnimPose': ('walk', 0)}

# # character controller data
data.ctrlData = {'initState': 'I', 'linAccel': 150.0, 'linSpeed': 125.0, 'linSpeedQ': 275.0,
                 'angAccel': 0.8, 'angSpeed': 1.6, 'angSpeedQ': 3.2, 'jumpSpeed': 10.0, 'jumpspeedQ': 20.0}

# # CCDik related data
# right CCDik object
data.jointBaseParentR = 'Hips'
data.jointHierarchyArrayR = [
    {'name': 'RightUpLeg', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     'baseForwardAxis': LVector3f(0, 0, -1)
     },
    {'name': 'RightUpLegRoll', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     #'endEffector':True,
     'angles': (45, 120, 30, 0),
     'w': 1.0
     },  #
    {'name': 'RightLeg', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     #'endEffector':True,
     'angles': (89, 15, 40, 15),
     'w': 1.0
     },  #
    {'name': 'RightLegRoll', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     #'endEffector':True,
     'angles': (5, 5, 150, 5),
     'w': 1.0
     },  #
    {'name': 'RightFoot', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     #'endEffector':True,
     'angles': (5, 5, 5, 5),
     'w': 1.0
     },  #
    {'name': 'RightToeBase', 'children': 0,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (95, 20, 0, 20),
     'w': 1.0
     },  #
]
data.minZR = {'RightFoot': 0.67, 'RightToeBase': 0.65}
# left CCDik object
data.jointBaseParentL = 'Hips'
data.jointHierarchyArrayL = [
    {'name': 'LeftUpLeg', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     'baseForwardAxis': LVector3f(0, 0, -1)
     },
    {'name': 'LeftUpLegRoll', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     #'endEffector':True,
     'angles': (45, 120, 30, 0),
     'w': 1.0
     },  #
    {'name': 'LeftLeg', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     #'endEffector':True,
     'angles': (89, 15, 40, 15),
     'w': 1.0
     },  #
    {'name': 'LeftLegRoll', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     #'endEffector':True,
     'angles': (5, 5, 150, 5),
     'w': 1.0
     },  #
    {'name': 'LeftFoot', 'children': 1,
     'upAxis': LVector3f(0, -1, 0),
     #'endEffector':True,
     'angles': (5, 5, 5, 5),
     'w': 1.0
     },  #
    {'name': 'LeftToeBase', 'children': 0,
     'upAxis': LVector3f(0, -1, 0),
     'angles': (95, 20, 0, 20),
     'w': 1.0
     },  #
]
data.minZL = {'LeftFoot': 0.62, 'LeftToeBase': 0.6}
