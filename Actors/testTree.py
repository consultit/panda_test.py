'''
Created on Oct 8, 2017
 
@author: consultit
'''

from panda3d.core import load_prc_file_data, Filename, TextNode, LPoint3f, \
    TextFont, LVecBase3f, LineSegs, GeomNode, LVector3f, NodePath
from direct.showbase.ShowBase import ShowBase
import sys, os
# Get the location and Convert that to panda's unix-style notation.
datadir = os.path.abspath(sys.path[0])
datadir = os.path.join(datadir, "..")
datadir = Filename.fromOsSpecific(datadir).getFullpath()

load_prc_file_data("", "win-size 1024 768")
# load_prc_file_data("", "want-tk #t")

from ely.direct.data_structures_and_algorithms.ch08.linked_tree import LinkedTree
from ely.direct.data_structures_and_algorithms.ch07.linked_queue import LinkedQueue

dynFont = '/usr/share/fonts/truetype/dejavu/DejaVuSans-BoldOblique.ttf'

def setup3dText(app, parent, text, labelColor=(1, 0, 0, 1), font='Default',
                align=TextNode.A_center, pos=LPoint3f.zero(), scale=(1, 1, 1),
                renderMode=TextFont.RM_solid, slant=0.3, wordwrap=5.0, smallCaps=True,
                smallCapsScale=0.5, shadow=(0.05, 0.05), shadowColor=(0, 0, 0, 1),
                frameColor=(0, 1, 0, 1), frameAsMargin=(0.2, 0.2, 0.2, 0.2),
                cardColor=(0, 0, 1, 1), cardAsMargin=(0, 0, 0, 0), cardDecal=True,
                rotate=True, rotateTime=5):
    #
    label = TextNode(text + '_TextNode')
    if font == 'Default':
        labelFont = label.get_font()
    else:
        labelFont = app.loader.load_font(font)
    labelFont.set_render_mode(renderMode)
    label.set_font(labelFont)
    label.set_text(text)
    label.set_text_color(*labelColor)
    label.set_small_caps(smallCaps)
    label.set_small_caps_scale(smallCapsScale)
    label.set_slant(slant)
    label.set_shadow(*shadow)
    label.set_shadow_color(*shadowColor)
    label.set_wordwrap(wordwrap)
    label.set_align(align)
    label.set_frame_color(*frameColor)
    label.set_frame_as_margin(*frameAsMargin)
    label.set_card_color(*cardColor)
    label.set_card_as_margin(*cardAsMargin)
    label.set_card_decal(cardDecal)
    if parent:
        labelCenter = parent.attach_new_node(text + '_TextCenter')
    else:
        labelCenter = NodePath(text + '_TextCenter')
    labelCenter.set_pos(pos)
    labelNodePath = label.generate() 
    labelNode = labelCenter.attach_new_node(labelNodePath)
    labelNode.set_scale(*scale)
    # rotate label
    if rotate:
        labelCenter.hprInterval(rotateTime, LVecBase3f(360, 0, 0), startHpr=0).loop()
    return labelCenter

def drawSegment(name, parent, pos=(0, 0, 0), toward=(0, 0, 1), length=1, color=(1, 1, 1, 1),
                thickness=1, drawBin=None):
    segment = LineSegs(name)
    segment.set_thickness(thickness)
    segment.set_color(*color)
    segmentGN = GeomNode(name + "_GN")
    segmentnp = parent.attach_new_node(segmentGN)
    if drawBin:
        segmentnp.set_bin("fixed", drawBin) 
        segmentnp.set_depth_write(False) 
        segmentnp.set_depth_test(False)
    # update transform
    geomnode = segmentnp.node()
    sPos = LPoint3f(*pos)
    sTow = LVector3f(*toward)
    segment.move_to(sPos)
    segment.draw_to(sPos + sTow * length)
    if geomnode.get_num_geoms() > 0:
        geomnode.remove_geom(0)
    segment.create(geomnode, True)
    return segmentnp

def drawTree(app, tree, levelWidths, levelHeight, getTreePosNP, orig=LVector3f.zero(),
             thickness=4.0, color=(0.8, 0.2, 0.6, 1.0)):         
    # reposition and draw tree nodes (using preorder traversal)
    for p in tree.preorder():
        print(getTreePosNP(p).get_name())
        level = tree.depth(p)
        # get this node and find it in the levelWidths[level]
        idx = 0
        node = tree._validate(p)
        for nw in levelWidths[level]:
            if node == nw[0]:
                break
            idx += 1
        ePos = LPoint3f(levelWidths[level][idx][1], 0, levelHeight[level]) + orig
        getTreePosNP(p).set_pos(ePos)
        if tree.parent(p):
            ePPos = getTreePosNP(tree.parent(p)).get_pos()
            ##ePPos = tree.parent(p).element()[0].get_pos()
            deltaEPos = ePPos - ePos
            eTow = deltaEPos.normalized()
            eLen = deltaEPos.length()
            drawSegment(getTreePosNP(p).get_name(), app.render, pos=ePos, toward=eTow, length=eLen,
                        thickness=thickness, color=color)
    
def computeLevels(tree, screenWidth, screenHeight, getTreePosNP):
    # compute each level's height (up -> down)
    levelNum = tree.height()
    levelWidths = [None] * (levelNum + 1)
    levelHeight = []
    height = screenHeight[1] - screenHeight[0]
    h = screenHeight[1]
    if levelNum == 0:
        deltaH = height / 2
        h -= deltaH
        levelHeight.append(h)
    else:
        deltaH = height / levelNum 
        for l in range(levelNum + 1):
            levelHeight.append(h)
            h -= deltaH
    # compute each level's width (using breadthfirst traversal)
    width = screenWidth[1] - screenWidth[0]
    for p in tree.breadthfirst():
        print(getTreePosNP(p).get_name())
        # this is the level of the node
        level = tree.depth(p)
        # check if it is a new level
        if levelWidths[level] == None:
            # this is the first node of this level: put it at center
            levelWidths[level] = [(tree._validate(p), 0.0)]
        else:
            # adjust the widths of the level considering this node
            levelWidths[level] = levelWidths[level] + [(tree._validate(p), 0.0)]
            levelNodes = len(levelWidths[level])
            deltaW = width / levelNodes
            w = screenWidth[0]
            # recompute widths of this level
            for i in range(levelNodes):
                if i == 0:
                    w += deltaW / 2.0    
                else:
                    w += deltaW
                levelWidths[level][i] = (levelWidths[level][i][0], w)
    return (levelWidths, levelHeight)

def make_copy(tree, copyElement=None):
    treeCopy = LinkedTree()
    pQueue = LinkedQueue()
    for p in tree.breadthfirst():
        childNum = tree.num_children(p)
        element = copyElement(p.element()) if copyElement else None
        if tree.is_root(p):
            # add base joint
            pC = treeCopy._add_root(element)
        else:
            # add remaining nodes
            pC = pQueue.dequeue()
            pC = treeCopy._add_child(pC, element)
        #
        while childNum > 0:
            pQueue.enqueue(pC)
            childNum -= 1
    return treeCopy
    
if __name__ == '__main__':
    app = ShowBase()
    # input data
    screenWidth = (-30.0, 30.0)
    screenHeight = (-25.0, 25.0)

    class Text3d(object):

        def __call__(self, text):
            return setup3dText(app, None, text, font=dynFont, scale=(1,), rotate=False)

    # user friendly format specification as array, so insertion can use breadthfirst traversal like
    treeNodes = [
        (Text3d()('/user/rt/courses/'), 2),
        (Text3d()('cs016/'), 3),
        (Text3d()('cs252/'), 2),
        (Text3d()('grades'), 0),
        (Text3d()('homeworks/'), 3),
        (Text3d()('programs/'), 3),
        (Text3d()('projects/'), 2),
        (Text3d()('grades'), 0),
        (Text3d()('hw1'), 0),
        (Text3d()('hw2'), 0),
        (Text3d()('hw3'), 0),
        (Text3d()('pr1'), 0),
        (Text3d()('pr2'), 0),
        (Text3d()('pr3'), 0),
        (Text3d()('papers/'), 2),
        (Text3d()('demos/'), 1),
        (Text3d()('buylow'), 0),
        (Text3d()('sellhigh'), 0),
        (Text3d()('market'), 0),
        ]
    # create a (empty) tree
    tree = LinkedTree()
    # node insertion use breadthfirst traversal like algorithm
    # FIFO queue of positions
    pQueue = LinkedQueue()
    # add root
    treeNodeI = app.render.attach_new_node(treeNodes[0][0].get_name() + 'I')
    treeNodes[0][0].instance_to(treeNodeI)
    element = (treeNodeI, treeNodes[0][0])
    childNum = treeNodes[0][1]
    p = tree._add_root(element)
    for i in range(childNum):
        pQueue.enqueue(p)    
    # add remaining nodes
    for node in treeNodes[1:]:
        p = pQueue.dequeue()
        nodeI = app.render.attach_new_node(node[0].get_name() + 'I')
        node[0].instance_to(nodeI)
        element = (nodeI, node[0])
        childNum = node[1]
        p = tree._add_child(p, element)
        for i in range(childNum):
            pQueue.enqueue(p)    
    # test insertion
    for p in tree.breadthfirst():
        print(p.element()[0].get_name())

    def getTreePosNP(p):
        '''Must return a NodePath'''
        return p.element()[0]
    
    levelWidths, levelHeight = computeLevels(tree, screenWidth, screenHeight, getTreePosNP)
    drawTree(app, tree, levelWidths, levelHeight, getTreePosNP, orig=LVector3f.zero())
    
    # make a tree copy and draw it
    def copyElement(e):
        global app
        eInst = app.render.attach_new_node(e[1].get_name() + 'I')
        e[1].instance_to(eInst)
        return (eInst, e[1])
    
    treeCopy = make_copy(tree, copyElement=copyElement)
    levelWidthsC, levelHeightC = computeLevels(treeCopy, screenWidth, screenHeight, getTreePosNP)
    drawTree(app, treeCopy, levelWidthsC, levelHeightC, getTreePosNP, orig=LVector3f(0.0, 20.0, 0.0),
             thickness=3.0, color=(0.2, 0.8, 0.6, 1.0))

    treeCopy2 = make_copy(tree, copyElement=copyElement)
    levelWidthsC2, levelHeightC2 = computeLevels(treeCopy2, screenWidth, screenHeight, getTreePosNP)
    drawTree(app, treeCopy2, levelWidthsC2, levelHeightC2, getTreePosNP, orig=LVector3f(0.0, 40.0, 0.0),
             thickness=2.0, color=(0.2, 0.6, 0.8, 1.0))
    
    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 100.0, 0.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
