import sys
import os
from direct.fsm.FSM import FSM
from panda3d.core import LVector3f


class ActorFSM(FSM, object):
    '''Actor's FSM'''

    def __init__(self, actor, actorCtrl, animTrans, ctrlData):
        '''Constructor.'''

        super(ActorFSM, self).__init__('ActorFSM')
        self.actor = actor
        self.actorCtrl = actorCtrl
        # animations' transitions
        self.animTrans = animTrans
        self.animTransCurr = None
        # set accelerations and speeds
        self.actorCtrl.linear_accel = LVector3f(ctrlData['linAccel'])
        self.actorCtrl.max_linear_speed = self.linSpeed = LVector3f(
            ctrlData['linSpeed'])
        self.linSpeedQ = LVector3f(ctrlData['linSpeedQ'])
        self.actorCtrl.angular_accel = ctrlData['angAccel']
        self.actorCtrl.max_angular_speed = self.maxAngSpeed = \
            ctrlData['angSpeed']
        self.maxAngSpeedQ = ctrlData['angSpeedQ']
        self.actorCtrl.jump_speed = self.jumpSpeed = ctrlData['jumpSpeed']
        self.jumpSpeedQ = ctrlData['jumpspeedQ']

        # set default transitions
        self.defaultTransitions = {
            'F_Rr_J': ['Rr_J', 'F_J', 'F_Rr', ],
            'Rr': ['F_Rr', 'F_Rr_Q', 'B_Rr', 'Sr_Rr', 'Sr_Rr_Q', 'Sl_Rr', 'Sr_Rl_Q', 'I', 'Rr_Q', 'Rr_Q', ],
            'Sr_Rr': ['Rr', 'Sr_Rr_Q', 'Sr', 'Sr_Rr_Q', 'Sr_Rr_Q', ],
            'Sr_Rl': ['Rl', 'Sr_Rl_Q', 'Sr', 'Sr_Rl_Q', 'Sr_Rl_Q', ],
            'F_Rr_J_Q': ['Rr_J', 'F_J_Q', 'F_Rr_Q', 'F_Rr_J', ],
            'Rl': ['F_Rl', 'F_Rl_Q', 'B_Rl', 'Sr_Rl', 'Sr_Rl_Q', 'Sl_Rl', 'Sl_Rl_Q', 'I', 'Rl_Q', 'Rl_Q', ],
            'F_J_Q': ['J', 'F_Rr_J_Q', 'F_Rr_J_Q', 'F_Rl_J_Q', 'F_Rl_J_Q', 'F_Q', 'F_J', ],
            'F_Rr_Q': ['Rr_Q', 'F_Q', 'F_Rr', ],
            'Sl_Rr': ['Rr', 'Sl_Rr_Q', 'Sl', 'Sl_Rr_Q', 'Sl_Rr_Q', ],
            'B_Rl': ['Rl', 'B', ],
            'Rl_Q': ['F_Rl_Q', 'F_Rl_Q', 'Sr_Rl_Q', 'Sr_Rl_Q', 'Sl_Rl_Q', 'Sl_Rl_Q', 'I', 'Rl', ],
            'F_Rr': ['Rr', 'F_Rr_Q', 'F', 'F_Rr_Q', 'F_Rr_J', 'F_Rr_Q', ],
            'F_Rl_J_Q': ['Rl_J', 'F_J_Q', 'F_Rl_Q', 'F_Rl_J', ],
            'F_J': ['J', 'F_J_Q', 'F', 'F_J_Q', 'F_J_Q', ],
            'Sr_Q': ['I', 'Sr_Rr_Q', 'Sr_Rr_Q', 'Sr_Rl_Q', 'Sr_Rl_Q', 'Sr', ],
            'F_Q': ['I', 'F_Rr_Q', 'F_Rr_Q', 'F_Rl_Q', 'F_Rl_Q', 'F_J_Q', 'F_J_Q', 'F', ],
            'F_Rl': ['Rl', 'F_Rl_Q', 'F', 'F_Rl_Q', 'F_Rl_J', 'F_Rl_Q', ],
            'Sl_Rl_Q': ['Rl_Q', 'Sl_Q', 'Sr_Rl', ],
            'B_Rr': ['Rr', 'B', ],
            'Sl_Rl': ['Rl', 'Sl_Rl_Q', 'Sl', 'Sl_Rl_Q', 'Sl_Rl_Q', ],
            'B': ['I', 'B_Sr', 'B_Sl', 'B_Rr', 'B_Rl', ],
            'F_Rl_Q': ['Rl_Q', 'F_Q', 'F_Rl', ],
            'F': ['I', 'F_Q', 'F_Sr', 'F_Sl', 'F_Rr', 'F_Rr_Q', 'F_Rl', 'F_Rl_Q', 'F_J', 'F_J_Q', 'F_Q', ],
            'I': ['F', 'F_Q', 'B', 'Sr', 'Sr_Q', 'Sl', 'Sl_Q', 'Rr', 'Rr_Q', 'Rl', 'Rl_Q', 'J', ],
            'Sr': ['I', 'Sr_Q', 'Sr_Rr', 'Sr_Rr_Q', 'Sr_Rl', 'Sr_Rl_Q', 'Sr_Q', ],
            'J': ['F_J', 'F_J_Q', 'I', ],
            'Sr_Rr_Q': ['Rr_Q', 'Sr_Q', 'Sr_Rr', ],
            'Sl': ['I', 'Sl_Q', 'Sl_Rr', 'Sl_Rr_Q', 'Sl_Rl', 'Sl_Rl_Q', 'Sl_Q', ],
            'Sl_Rr_Q': ['Rr_Q', 'Sl_Q', 'Sl_Rr', ],
            'F_Rl_J': ['Rl_J', 'F_J', 'F_Rl', ],
            'B_Sl': ['Sl', 'B', ],
            'F_Sr': ['Sr', 'F', ],
            'Rr_Q': ['F_Rr_Q', 'F_Rr_Q', 'Sr_Rr_Q', 'Sr_Rr_Q', 'Sl_Rr_Q', 'Sl_Rr_Q', 'I', 'Rr', ],
            'Rl_J': ['F_Rl_J', 'J', 'Rl', ],
            'F_Sl': ['Sl', 'F', ],
            'Sl_Q': ['I', 'Sl_Rr_Q', 'Sl_Rr_Q', 'Sl_Rl_Q', 'Sl_Rl_Q', 'Sl', ],
            'Rr_J': ['F_Rr_J', 'J', 'Rr', ],
            'Sr_Rl_Q': ['Rl_Q', 'Sr_Q', 'Sr_Rl', ],
            'B_Sr': ['Sr', 'B', ],
        }

    # F_Rr_J
    def enterF_Rr_J(self):
        '''enterF_Rr_J'''

        print('enterF_Rr_J')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        self.actorCtrl.jump_speed = self.jumpSpeed
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.rotate_head_right = True
        self.actorCtrl.jump = True

    def exitF_Rr_J(self):
        '''exitF_Rr_J'''

        print('exitF_Rr_J')
        self.actorCtrl.move_forward = False
        self.actorCtrl.rotate_head_right = False
        self.actorCtrl.jump = False

    def filterF_Rr_J(self, request, args):
        '''filterF_Rr_J'''

        print('filterF_Rr_J')
        if request == 'f_up':
            return 'Rr_J'
        elif request == 'rr_up':
            return 'F_J'
        elif request == 'j_up':
            return 'F_Rr'
        else:
            return None

    # Rr
    def enterRr(self):
        '''enterRr'''

        print('enterRr')
        # set speeds
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        #
        self.actorCtrl.rotate_head_right = True

    def exitRr(self):
        '''exitRr'''

        print('exitRr')
        self.actorCtrl.rotate_head_right = False

    def filterRr(self, request, args):
        '''filterRr'''

        print('filterRr')
        if request == 'f':
            return 'F_Rr'
        elif request == 'f_q':
            return 'F_Rr_Q'
        elif request == 'b':
            return 'B_Rr'
        elif request == 'sr':
            return 'Sr_Rr'
        elif request == 'sr_q':
            return 'Sr_Rr_Q'
        elif request == 'sl':
            return 'Sl_Rr'
        elif request == 'sl_q':
            return 'Sr_Rl_Q'
        elif request == 'rr_up':
            return 'I'
        elif request == 'rr_q':
            return 'Rr_Q'
        elif request == 'q':
            return 'Rr_Q'
        else:
            return None

    # Sr_Rr
    def enterSr_Rr(self):
        '''enterSr_Rr'''

        print('enterSr_Rr')

    def exitSr_Rr(self):
        '''exitSr_Rr'''

        print('exitSr_Rr')

    def filterSr_Rr(self, request, args):
        '''filterSr_Rr'''

        print('filterSr_Rr')
        if request == 'sr_up':
            return 'Rr'
        elif request == 'sr_q':
            return 'Sr_Rr_Q'
        elif request == 'rr_up':
            return 'Sr'
        elif request == 'rr_q':
            return 'Sr_Rr_Q'
        elif request == 'q':
            return 'Sr_Rr_Q'
        else:
            return None

    # Sr_Rl
    def enterSr_Rl(self):
        '''enterSr_Rl'''

        print('enterSr_Rl')

    def exitSr_Rl(self):
        '''exitSr_Rl'''

        print('exitSr_Rl')

    def filterSr_Rl(self, request, args):
        '''filterSr_Rl'''

        print('filterSr_Rl')
        if request == 'sr_up':
            return 'Rl'
        elif request == 'sr_q':
            return 'Sr_Rl_Q'
        elif request == 'rl_up':
            return 'Sr'
        elif request == 'rl_q':
            return 'Sr_Rl_Q'
        elif request == 'q':
            return 'Sr_Rl_Q'
        else:
            return None

    # F_Rr_J_Q
    def enterF_Rr_J_Q(self):
        '''enterF_Rr_J_Q'''

        print('enterF_Rr_J_Q')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeedQ
        self.actorCtrl.max_angular_speed = self.maxAngSpeedQ
        self.actorCtrl.jump_speed = self.jumpSpeedQ
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.rotate_head_right = True
        self.actorCtrl.jump = True

    def exitF_Rr_J_Q(self):
        '''exitF_Rr_J_Q'''

        print('exitF_Rr_J_Q')
        self.actorCtrl.move_forward = False
        self.actorCtrl.rotate_head_right = False
        self.actorCtrl.jump = False

    def filterF_Rr_J_Q(self, request, args):
        '''filterF_Rr_J_Q'''

        print('filterF_Rr_J_Q')
        if request == 'f_up':
            return 'Rr_J'
        elif request == 'rr_up':
            return 'F_J_Q'
        elif request == 'j_up':
            return 'F_Rr_Q'
        elif request == 'q_up':
            return 'F_Rr_J'
        else:
            return None

    # Rl
    def enterRl(self):
        '''enterRl'''

        print('enterRl')
        # set speeds
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        #
        self.actorCtrl.rotate_head_left = True

    def exitRl(self):
        '''exitRl'''

        print('exitRl')
        self.actorCtrl.rotate_head_left = False

    def filterRl(self, request, args):
        '''filterRl'''

        print('filterRl')
        if request == 'f':
            return 'F_Rl'
        elif request == 'f_q':
            return 'F_Rl_Q'
        elif request == 'b':
            return 'B_Rl'
        elif request == 'sr':
            return 'Sr_Rl'
        elif request == 'sr_q':
            return 'Sr_Rl_Q'
        elif request == 'sl':
            return 'Sl_Rl'
        elif request == 'sl_q':
            return 'Sl_Rl_Q'
        elif request == 'rl_up':
            return 'I'
        elif request == 'rl_q':
            return 'Rl_Q'
        elif request == 'q':
            return 'Rl_Q'
        else:
            return None

    # F_J_Q
    def enterF_J_Q(self):
        '''enterF_J_Q'''

        print('enterF_J_Q')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeedQ
        self.actorCtrl.jump_speed = self.jumpSpeedQ
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.jump = True

    def exitF_J_Q(self):
        '''exitF_J_Q'''

        print('exitF_J_Q')
        self.actorCtrl.move_forward = False
        self.actorCtrl.jump = False

    def filterF_J_Q(self, request, args):
        '''filterF_J_Q'''

        print('filterF_J_Q')
        if request == 'f_up':
            return 'J'
        elif request == 'rr':
            return 'F_Rr_J_Q'
        elif request == 'rr_q':
            return 'F_Rr_J_Q'
        elif request == 'rl':
            return 'F_Rl_J_Q'
        elif request == 'rl_q':
            return 'F_Rl_J_Q'
        elif request == 'j_up':
            return 'F_Q'
        elif request == 'q_up':
            return 'F_J'
        else:
            return None

    # F_Rr_Q
    def enterF_Rr_Q(self):
        '''enterF_Rr_Q'''

        print('enterF_Rr_Q')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeedQ
        self.actorCtrl.max_angular_speed = self.maxAngSpeedQ
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.rotate_head_right = True
        if self.oldState == 'F_Rr':
            if self.animTransCurr and self.animTransCurr._inTransition:
                self.animTransCurr.stopTransition()
            self.animTransCurr = self.animTrans['walk-run']
            self.animTransCurr.startTransition()
        elif not (self.actor.get_current_anim(partName='modelRoot') == 'run'):
            self.actor.loop('run')

    def exitF_Rr_Q(self):
        '''exitF_Rr_Q'''

        print('exitF_Rr_Q')
        self.actorCtrl.move_forward = False
        self.actorCtrl.rotate_head_right = False

    def filterF_Rr_Q(self, request, args):
        '''filterF_Rr_Q'''

        print('filterF_Rr_Q')
        if request == 'f_up':
            return 'Rr_Q'
        elif request == 'rr_up':
            return 'F_Q'
        elif request == 'q_up':
            return 'F_Rr'
        else:
            return None

    # Sl_Rr
    def enterSl_Rr(self):
        '''enterSl_Rr'''

        print('enterSl_Rr')

    def exitSl_Rr(self):
        '''exitSl_Rr'''

        print('exitSl_Rr')

    def filterSl_Rr(self, request, args):
        '''filterSl_Rr'''

        print('filterSl_Rr')
        if request == 'sl_up':
            return 'Rr'
        elif request == 'sl_q':
            return 'Sl_Rr_Q'
        elif request == 'rr_up':
            return 'Sl'
        elif request == 'rr_q':
            return 'Sl_Rr_Q'
        elif request == 'q':
            return 'Sl_Rr_Q'
        else:
            return None

    # B_Rl
    def enterB_Rl(self):
        '''enterB_Rl'''

        print('enterB_Rl')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        #
        self.actorCtrl.move_backward = True
        self.actorCtrl.rotate_head_left = True

    def exitB_Rl(self):
        '''exitB_Rl'''

        print('exitB_Rl')
        self.actorCtrl.move_backward = False
        self.actorCtrl.rotate_head_left = False

    def filterB_Rl(self, request, args):
        '''filterB_Rl'''

        print('filterB_Rl')
        if request == 'b_up':
            return 'Rl'
        elif request == 'rl_up':
            return 'B'
        else:
            return None

    # Rl_Q
    def enterRl_Q(self):
        '''enterRl_Q'''

        print('enterRl_Q')
        # set speeds
        self.actorCtrl.max_angular_speed = self.maxAngSpeedQ
        #
        self.actorCtrl.rotate_head_left = True

    def exitRl_Q(self):
        '''exitRl_Q'''

        print('exitRl_Q')
        self.actorCtrl.rotate_head_left = False

    def filterRl_Q(self, request, args):
        '''filterRl_Q'''

        print('filterRl_Q')
        if request == 'f':
            return 'F_Rl_Q'
        elif request == 'f_q':
            return 'F_Rl_Q'
        elif request == 'sr':
            return 'Sr_Rl_Q'
        elif request == 'sr_q':
            return 'Sr_Rl_Q'
        elif request == 'sl':
            return 'Sl_Rl_Q'
        elif request == 'sl_q':
            return 'Sl_Rl_Q'
        elif request == 'rl_up':
            return 'I'
        elif request == 'q_up':
            return 'Rl'
        else:
            return None

    # F_Rr
    def enterF_Rr(self):
        '''enterF_Rr'''

        print('enterF_Rr')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.rotate_head_right = True
        if self.oldState == 'F_Rr_Q':
            if self.animTransCurr and self.animTransCurr._inTransition:
                self.animTransCurr.stopTransition()
            self.animTransCurr = self.animTrans['run-walk']
            self.animTransCurr.startTransition()
        elif not (self.actor.get_current_anim(partName='modelRoot') == 'walk'):
            self.actor.loop('walk')

    def exitF_Rr(self):
        '''exitF_Rr'''

        print('exitF_Rr')
        self.actorCtrl.move_forward = False
        self.actorCtrl.rotate_head_right = False

    def filterF_Rr(self, request, args):
        '''filterF_Rr'''

        print('filterF_Rr')
        if request == 'f_up':
            return 'Rr'
        elif request == 'f_q':
            return 'F_Rr_Q'
        elif request == 'rr_up':
            return 'F'
        elif request == 'rr_q':
            return 'F_Rr_Q'
        elif request == 'j':
            return 'F_Rr_J'
        elif request == 'q':
            return 'F_Rr_Q'
        else:
            return None

    # F_Rl_J_Q
    def enterF_Rl_J_Q(self):
        '''enterF_Rl_J_Q'''

        print('enterF_Rl_J_Q')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeedQ
        self.actorCtrl.max_angular_speed = self.maxAngSpeedQ
        self.actorCtrl.jump_speed = self.jumpSpeedQ
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.rotate_head_left = True
        self.actorCtrl.jump = True

    def exitF_Rl_J_Q(self):
        '''exitF_Rl_J_Q'''

        print('exitF_Rl_J_Q')
        self.actorCtrl.move_forward = False
        self.actorCtrl.rotate_head_left = False
        self.actorCtrl.jump = False

    def filterF_Rl_J_Q(self, request, args):
        '''filterF_Rl_J_Q'''

        print('filterF_Rl_J_Q')
        if request == 'f_up':
            return 'Rl_J'
        elif request == 'rl_up':
            return 'F_J_Q'
        elif request == 'j_up':
            return 'F_Rl_Q'
        elif request == 'q_up':
            return 'F_Rl_J'
        else:
            return None

    # F_J
    def enterF_J(self):
        '''enterF_J'''

        print('enterF_J')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        #self.actorCtrl.jump_speed = self.jumpSpeed
        #
        self.actorCtrl.move_forward = True
        #self.actorCtrl.jump = True
        if self.oldState == 'F':
            if self.animTransCurr and self.animTransCurr._inTransition:
                self.animTransCurr.stopTransition()
            self.animTransCurr = self.animTrans['walk-jump']
            self.animTransCurr.startTransition()
        elif not (self.actor.get_current_anim(partName='modelRoot') == 'jump'):
            self.actor.loop('jump')

    def exitF_J(self):
        '''exitF_J'''

        print('exitF_J')
        self.actorCtrl.move_forward = False
        self.actorCtrl.jump = False

    def filterF_J(self, request, args):
        '''filterF_J'''

        print('filterF_J')
        if request == 'f_up':
            return 'J'
        elif request == 'f_q':
            return 'F_J_Q'
        elif request == 'j_up':
            return 'F'
        elif request == 'j_q':
            return 'F_J_Q'
        elif request == 'q':
            return 'F_J_Q'
        else:
            return None

    # Sr_Q
    def enterSr_Q(self):
        '''enterSr_Q'''

        print('enterSr_Q')

    def exitSr_Q(self):
        '''exitSr_Q'''

        print('exitSr_Q')

    def filterSr_Q(self, request, args):
        '''filterSr_Q'''

        print('filterSr_Q')
        if request == 'sr_up':
            return 'I'
        elif request == 'rr':
            return 'Sr_Rr_Q'
        elif request == 'rr_q':
            return 'Sr_Rr_Q'
        elif request == 'rl':
            return 'Sr_Rl_Q'
        elif request == 'rl_q':
            return 'Sr_Rl_Q'
        elif request == 'q_up':
            return 'Sr'
        else:
            return None

    # F_Q
    def enterF_Q(self):
        '''enterF_Q'''

        print('enterF_Q')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeedQ
        #
        self.actorCtrl.move_forward = True
        if self.oldState == 'F':
            if self.animTransCurr and self.animTransCurr._inTransition:
                self.animTransCurr.stopTransition()
            self.animTransCurr = self.animTrans['walk-run']
            self.animTransCurr.startTransition()
        elif not (self.actor.get_current_anim(partName='modelRoot') == 'run'):
            self.actor.loop('run')

    def exitF_Q(self):
        '''exitF_Q'''

        print('exitF_Q')
        self.actorCtrl.move_forward = False

    def filterF_Q(self, request, args):
        '''filterF_Q'''

        print('filterF_Q')
        if request == 'f_up':
            return 'I'
        elif request == 'rr':
            return 'F_Rr_Q'
        elif request == 'rr_q':
            return 'F_Rr_Q'
        elif request == 'rl':
            return 'F_Rl_Q'
        elif request == 'rl_q':
            return 'F_Rl_Q'
        elif request == 'j':
            return 'F_J_Q'
        elif request == 'j_q':
            return 'F_J_Q'
        elif request == 'q_up':
            return 'F'
        else:
            return None

    # F_Rl
    def enterF_Rl(self):
        '''enterF_Rl'''

        print('enterF_Rl')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.rotate_head_left = True
        if self.oldState == 'F_Rl_Q':
            if self.animTransCurr and self.animTransCurr._inTransition:
                self.animTransCurr.stopTransition()
            self.animTransCurr = self.animTrans['run-walk']
            self.animTransCurr.startTransition()
        elif not (self.actor.get_current_anim(partName='modelRoot') == 'walk'):
            self.actor.loop('walk')

    def exitF_Rl(self):
        '''exitF_Rl'''

        print('exitF_Rl')
        self.actorCtrl.move_forward = False
        self.actorCtrl.rotate_head_left = False

    def filterF_Rl(self, request, args):
        '''filterF_Rl'''

        print('filterF_Rl')
        if request == 'f_up':
            return 'Rl'
        elif request == 'f_q':
            return 'F_Rl_Q'
        elif request == 'rl_up':
            return 'F'
        elif request == 'rl_q':
            return 'F_Rl_Q'
        elif request == 'j':
            return 'F_Rl_J'
        elif request == 'q':
            return 'F_Rl_Q'
        else:
            return None

    # Sl_Rl_Q
    def enterSl_Rl_Q(self):
        '''enterSl_Rl_Q'''

        print('enterSl_Rl_Q')

    def exitSl_Rl_Q(self):
        '''exitSl_Rl_Q'''

        print('exitSl_Rl_Q')

    def filterSl_Rl_Q(self, request, args):
        '''filterSl_Rl_Q'''

        print('filterSl_Rl_Q')
        if request == 'sl_up':
            return 'Rl_Q'
        elif request == 'rl_up':
            return 'Sl_Q'
        elif request == 'q_up':
            return 'Sr_Rl'
        else:
            return None

    # B_Rr
    def enterB_Rr(self):
        '''enterB_Rr'''

        print('enterB_Rr')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        #
        self.actorCtrl.move_backward = True
        self.actorCtrl.rotate_head_right = True

    def exitB_Rr(self):
        '''exitB_Rr'''

        print('exitB_Rr')
        self.actorCtrl.move_backward = False
        self.actorCtrl.rotate_head_right = False

    def filterB_Rr(self, request, args):
        '''filterB_Rr'''

        print('filterB_Rr')
        if request == 'b_up':
            return 'Rr'
        elif request == 'rr_up':
            return 'B'
        else:
            return None

    # Sl_Rl
    def enterSl_Rl(self):
        '''enterSl_Rl'''

        print('enterSl_Rl')

    def exitSl_Rl(self):
        '''exitSl_Rl'''

        print('exitSl_Rl')

    def filterSl_Rl(self, request, args):
        '''filterSl_Rl'''

        print('filterSl_Rl')
        if request == 'sl_up':
            return 'Rl'
        elif request == 'sl_q':
            return 'Sl_Rl_Q'
        elif request == 'rl_up':
            return 'Sl'
        elif request == 'rl_q':
            return 'Sl_Rl_Q'
        elif request == 'q':
            return 'Sl_Rl_Q'
        else:
            return None

    # B
    def enterB(self):
        '''enterB'''

        print('enterB')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        #
        self.actorCtrl.move_backward = True

    def exitB(self):
        '''exitB'''

        print('exitB')
        self.actorCtrl.move_backward = False

    def filterB(self, request, args):
        '''filterB'''

        print('filterB')
        if request == 'b_up':
            return 'I'
        elif request == 'sr':
            return 'B_Sr'
        elif request == 'sl':
            return 'B_Sl'
        elif request == 'rr':
            return 'B_Rr'
        elif request == 'rl':
            return 'B_Rl'
        else:
            return None

    # F_Rl_Q
    def enterF_Rl_Q(self):
        '''enterF_Rl_Q'''

        print('enterF_Rl_Q')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeedQ
        self.actorCtrl.max_angular_speed = self.maxAngSpeedQ
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.rotate_head_left = True
        if self.oldState == 'F_Rl':
            if self.animTransCurr and self.animTransCurr._inTransition:
                self.animTransCurr.stopTransition()
            self.animTransCurr = self.animTrans['walk-run']
            self.animTransCurr.startTransition()
        elif not (self.actor.get_current_anim(partName='modelRoot') == 'run'):
            self.actor.loop('run')

    def exitF_Rl_Q(self):
        '''exitF_Rl_Q'''

        print('exitF_Rl_Q')
        self.actorCtrl.move_forward = False
        self.actorCtrl.rotate_head_left = False

    def filterF_Rl_Q(self, request, args):
        '''filterF_Rl_Q'''

        print('filterF_Rl_Q')
        if request == 'f_up':
            return 'Rl_Q'
        elif request == 'rl_up':
            return 'F_Q'
        elif request == 'q_up':
            return 'F_Rl'
        else:
            return None

    # F
    def enterF(self):
        '''enterF'''

        print('enterF')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        #
        self.actorCtrl.move_forward = True
        if self.oldState == 'F_Q':
            if self.animTransCurr and self.animTransCurr._inTransition:
                self.animTransCurr.stopTransition()
            self.animTransCurr = self.animTrans['run-walk']
            self.animTransCurr.startTransition()
        if self.oldState == 'F_J':
            if self.animTransCurr and self.animTransCurr._inTransition:
                self.animTransCurr.stopTransition()
            self.animTransCurr = self.animTrans['jump-walk']
            self.animTransCurr.startTransition()
        elif not (self.actor.get_current_anim(partName='modelRoot') == 'walk'):
            self.actor.loop('walk')

    def exitF(self):
        '''exitF'''

        print('exitF')
        self.actorCtrl.move_forward = False

    def filterF(self, request, args):
        '''filterF'''

        print('filterF')
        if request == 'f_up':
            return 'I'
        elif request == 'f_q':
            return 'F_Q'
        elif request == 'sr':
            return 'F_Sr'
        elif request == 'sl':
            return 'F_Sl'
        elif request == 'rr':
            return 'F_Rr'
        elif request == 'rr_q':
            return 'F_Rr_Q'
        elif request == 'rl':
            return 'F_Rl'
        elif request == 'rl_q':
            return 'F_Rl_Q'
        elif request == 'j':
            return 'F_J'
        elif request == 'j_q':
            return 'F_J_Q'
        elif request == 'q':
            return 'F_Q'
        else:
            return None

    # I
    def enterI(self):
        '''enterI'''

        print('enterI')
        self.actor.stop()

    def exitI(self):
        '''exitI'''

        print('exitI')

    def filterI(self, request, args):
        '''filterI'''

        print('filterI')
        if request == 'f':
            return 'F'
        elif request == 'f_q':
            return 'F_Q'
        elif request == 'b':
            return 'B'
        elif request == 'sr':
            return 'Sr'
        elif request == 'sr_q':
            return 'Sr_Q'
        elif request == 'sl':
            return 'Sl'
        elif request == 'sl_q':
            return 'Sl_Q'
        elif request == 'rr':
            return 'Rr'
        elif request == 'rr_q':
            return 'Rr_Q'
        elif request == 'rl':
            return 'Rl'
        elif request == 'rl_q':
            return 'Rl_Q'
        elif request == 'j':
            return 'J'
        else:
            return None

    # Sr
    def enterSr(self):
        '''enterSr'''

        print('enterSr')

    def exitSr(self):
        '''exitSr'''

        print('exitSr')

    def filterSr(self, request, args):
        '''filterSr'''

        print('filterSr')
        if request == 'sr_up':
            return 'I'
        elif request == 'sr_q':
            return 'Sr_Q'
        elif request == 'rr':
            return 'Sr_Rr'
        elif request == 'rr_q':
            return 'Sr_Rr_Q'
        elif request == 'rl':
            return 'Sr_Rl'
        elif request == 'rl_q':
            return 'Sr_Rl_Q'
        elif request == 'q':
            return 'Sr_Q'
        else:
            return None

    # J
    def enterJ(self):
        '''enterJ'''

        print('enterJ')
        # set speeds
        self.actorCtrl.jump_speed = self.jumpSpeed
        #
        self.actorCtrl.jump = True

    def exitJ(self):
        '''exitJ'''

        print('exitJ')
        self.actorCtrl.jump = False

    def filterJ(self, request, args):
        '''filterJ'''

        print('filterJ')
        if request == 'f':
            return 'F_J'
        elif request == 'f_q':
            return 'F_J_Q'
        elif request == 'j_up':
            return 'I'
        else:
            return None

    # Sr_Rr_Q
    def enterSr_Rr_Q(self):
        '''enterSr_Rr_Q'''

        print('enterSr_Rr_Q')

    def exitSr_Rr_Q(self):
        '''exitSr_Rr_Q'''

        print('exitSr_Rr_Q')

    def filterSr_Rr_Q(self, request, args):
        '''filterSr_Rr_Q'''

        print('filterSr_Rr_Q')
        if request == 'sr_up':
            return 'Rr_Q'
        elif request == 'rr_up':
            return 'Sr_Q'
        elif request == 'q_up':
            return 'Sr_Rr'
        else:
            return None

    # Sl
    def enterSl(self):
        '''enterSl'''

        print('enterSl')

    def exitSl(self):
        '''exitSl'''

        print('exitSl')

    def filterSl(self, request, args):
        '''filterSl'''

        print('filterSl')
        if request == 'sl_up':
            return 'I'
        elif request == 'sl_q':
            return 'Sl_Q'
        elif request == 'rr':
            return 'Sl_Rr'
        elif request == 'rr_q':
            return 'Sl_Rr_Q'
        elif request == 'rl':
            return 'Sl_Rl'
        elif request == 'rl_q':
            return 'Sl_Rl_Q'
        elif request == 'q':
            return 'Sl_Q'
        else:
            return None

    # Sl_Rr_Q
    def enterSl_Rr_Q(self):
        '''enterSl_Rr_Q'''

        print('enterSl_Rr_Q')

    def exitSl_Rr_Q(self):
        '''exitSl_Rr_Q'''

        print('exitSl_Rr_Q')

    def filterSl_Rr_Q(self, request, args):
        '''filterSl_Rr_Q'''

        print('filterSl_Rr_Q')
        if request == 'sl_up':
            return 'Rr_Q'
        elif request == 'rr_up':
            return 'Sl_Q'
        elif request == 'q_up':
            return 'Sl_Rr'
        else:
            return None

    # F_Rl_J
    def enterF_Rl_J(self):
        '''enterF_Rl_J'''

        print('enterF_Rl_J')
        # set speeds
        self.actorCtrl.max_linear_speed = self.linSpeed
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        self.actorCtrl.jump_speed = self.jumpSpeed
        #
        self.actorCtrl.move_forward = True
        self.actorCtrl.rotate_head_left = True
        self.actorCtrl.jump = True

    def exitF_Rl_J(self):
        '''exitF_Rl_J'''

        print('exitF_Rl_J')
        self.actorCtrl.move_forward = False
        self.actorCtrl.rotate_head_left = False
        self.actorCtrl.jump = False

    def filterF_Rl_J(self, request, args):
        '''filterF_Rl_J'''

        print('filterF_Rl_J')
        if request == 'f_up':
            return 'Rl_J'
        elif request == 'rl_up':
            return 'F_J'
        elif request == 'j_up':
            return 'F_Rl'
        else:
            return None

    # B_Sl
    def enterB_Sl(self):
        '''enterB_Sl'''

        print('enterB_Sl')

    def exitB_Sl(self):
        '''exitB_Sl'''

        print('exitB_Sl')

    def filterB_Sl(self, request, args):
        '''filterB_Sl'''

        print('filterB_Sl')
        if request == 'b_up':
            return 'Sl'
        elif request == 'sl_up':
            return 'B'
        else:
            return None

    # F_Sr
    def enterF_Sr(self):
        '''enterF_Sr'''

        print('enterF_Sr')

    def exitF_Sr(self):
        '''exitF_Sr'''

        print('exitF_Sr')

    def filterF_Sr(self, request, args):
        '''filterF_Sr'''

        print('filterF_Sr')
        if request == 'f_up':
            return 'Sr'
        elif request == 'sr_up':
            return 'F'
        else:
            return None

    # Rr_Q
    def enterRr_Q(self):
        '''enterRr_Q'''

        print('enterRr_Q')
        # set speeds
        self.actorCtrl.max_angular_speed = self.maxAngSpeedQ
        #
        self.actorCtrl.rotate_head_right = True

    def exitRr_Q(self):
        '''exitRr_Q'''

        print('exitRr_Q')
        self.actorCtrl.rotate_head_right = False

    def filterRr_Q(self, request, args):
        '''filterRr_Q'''

        print('filterRr_Q')
        if request == 'f':
            return 'F_Rr_Q'
        elif request == 'f_q':
            return 'F_Rr_Q'
        elif request == 'sr':
            return 'Sr_Rr_Q'
        elif request == 'sr_q':
            return 'Sr_Rr_Q'
        elif request == 'sl':
            return 'Sl_Rr_Q'
        elif request == 'sl_q':
            return 'Sl_Rr_Q'
        elif request == 'rr_up':
            return 'I'
        elif request == 'q_up':
            return 'Rr'
        else:
            return None

    # Rl_J
    def enterRl_J(self):
        '''enterRl_J'''

        print('enterRl_J')
        # set speeds
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        self.actorCtrl.jump_speed = self.jumpSpeed
        #
        self.actorCtrl.rotate_head_left = True
        self.actorCtrl.jump = True

    def exitRl_J(self):
        '''exitRl_J'''

        print('exitRl_J')
        self.actorCtrl.rotate_head_left = False
        self.actorCtrl.jump = False

    def filterRl_J(self, request, args):
        '''filterRl_J'''

        print('filterRl_J')
        if request == 'f':
            return 'F_Rl_J'
        elif request == 'rl_up':
            return 'J'
        elif request == 'j_up':
            return 'Rl'
        else:
            return None

    # F_Sl
    def enterF_Sl(self):
        '''enterF_Sl'''

        print('enterF_Sl')

    def exitF_Sl(self):
        '''exitF_Sl'''

        print('exitF_Sl')

    def filterF_Sl(self, request, args):
        '''filterF_Sl'''

        print('filterF_Sl')
        if request == 'f_up':
            return 'Sl'
        elif request == 'sl_up':
            return 'F'
        else:
            return None

    # Sl_Q
    def enterSl_Q(self):
        '''enterSl_Q'''

        print('enterSl_Q')

    def exitSl_Q(self):
        '''exitSl_Q'''

        print('exitSl_Q')

    def filterSl_Q(self, request, args):
        '''filterSl_Q'''

        print('filterSl_Q')
        if request == 'sl_up':
            return 'I'
        elif request == 'rr':
            return 'Sl_Rr_Q'
        elif request == 'rr_q':
            return 'Sl_Rr_Q'
        elif request == 'rl':
            return 'Sl_Rl_Q'
        elif request == 'rl_q':
            return 'Sl_Rl_Q'
        elif request == 'q_up':
            return 'Sl'
        else:
            return None

    # Rr_J
    def enterRr_J(self):
        '''enterRr_J'''

        print('enterRr_J')
        # set speeds
        self.actorCtrl.max_angular_speed = self.maxAngSpeed
        self.actorCtrl.jump_speed = self.jumpSpeed
        #
        self.actorCtrl.rotate_head_right = True
        self.actorCtrl.jump = True

    def exitRr_J(self):
        '''exitRr_J'''

        print('exitRr_J')
        self.actorCtrl.rotate_head_right = False
        self.actorCtrl.jump = False

    def filterRr_J(self, request, args):
        '''filterRr_J'''

        print('filterRr_J')
        if request == 'f':
            return 'F_Rr_J'
        elif request == 'rr_up':
            return 'J'
        elif request == 'j_up':
            return 'Rr'
        else:
            return None

    # Sr_Rl_Q
    def enterSr_Rl_Q(self):
        '''enterSr_Rl_Q'''

        print('enterSr_Rl_Q')

    def exitSr_Rl_Q(self):
        '''exitSr_Rl_Q'''

        print('exitSr_Rl_Q')

    def filterSr_Rl_Q(self, request, args):
        '''filterSr_Rl_Q'''

        print('filterSr_Rl_Q')
        if request == 'sr_up':
            return 'Rl_Q'
        elif request == 'rl_up':
            return 'Sr_Q'
        elif request == 'q_up':
            return 'Sr_Rl'
        else:
            return None

    # B_Sr
    def enterB_Sr(self):
        '''enterB_Sr'''

        print('enterB_Sr')

    def exitB_Sr(self):
        '''exitB_Sr'''

        print('exitB_Sr')

    def filterB_Sr(self, request, args):
        '''filterB_Sr'''

        print('filterB_Sr')
        if request == 'b_up':
            return 'Sr'
        elif request == 'sr_up':
            return 'B'
        else:
            return None

    # input events
    def setupInputEvents(self, app, eventInputMap):
        '''setupInputEvents'''

        self.eventInputMap = eventInputMap
        for ev in eventInputMap:
            app.accept(ev, self.inputEventsClbk, [ev])

    def inputEventsClbk(self, ev):
        '''inputEventsClbk'''

        self.request(self.eventInputMap[ev])

    # debug
    def setupDebug(self, app, keyEv):
        '''setupDebug'''

        self.inputKey = keyEv
        self.stateKey = 'shift-' + keyEv
        self.inputList = ['f', 'f_up', 'f_q', 'b', 'b_up', 'b_q', 'sr', 'sr_up', 'sr_q', 'sl', 'sl_up',
                          'sl_q', 'rr', 'rr_up', 'rr_q', 'rl', 'rl_up', 'rl_q', 'j', 'j_up', 'j_q', 'q', 'q_up', ]
        self.stateList = ['I', 'F', 'B', 'Sr', 'Sl', 'Rr', 'Rl', 'J', 'F_Rr', 'F_Rl', 'F_Sr', 'F_Sl', 'F_J', 'B_Rr', 'B_Rl', 'B_Sr', 'B_Sl', 'Sr_Rr', 'Sr_Rl', 'Sl_Rr', 'Sl_Rl',
                          'F_Q', 'Sr_Q', 'Sl_Q', 'Rr_J', 'Rr_Q', 'Rl_J', 'Rl_Q', 'F_Rr_J', 'F_Rr_Q', 'F_Rl_J', 'F_Rl_Q', 'F_J_Q', 'F_Rr_J_Q', 'F_Rl_J_Q', 'Sr_Rr_Q', 'Sr_Rl_Q', 'Sl_Rr_Q', 'Sl_Rl_Q', ]
        self.currInput = 0
        self.currState = self.stateList.index(self.state)
        app.accept(self.inputKey, self.cycleDebug, [self.inputKey])
        app.accept(self.stateKey, self.cycleDebug, [self.stateKey])

    def cycleDebug(self, what):
        '''cycleDebug'''

        if what == self.inputKey:
            fsmInput = self.inputList[self.currInput]
            print('input: \'' + fsmInput + '\'')
            self.request(self.inputList[self.currInput])
            sys.stdout = open(os.devnull, 'w')
            self.forceTransition(self.stateList[self.currState])
            sys.stdout = sys.__stdout__
            print('\n')
            self.currInput += 1
            if self.currInput >= len(self.inputList):
                self.currInput = 0
        elif what == self.stateKey:
            self.currState += 1
            if self.currState >= len(self.stateList):
                self.currState = 0
            print('\n')
            self.forceTransition(self.stateList[self.currState])
            self.currInput = 0
