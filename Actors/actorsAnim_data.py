'''
Created on Mar 11, 2020

@author: consultit
'''

actorData = {
    'john': {
        'fileName': 'john.egg',
        'anims': {
            'run': ('john-run.egg', 0.0),  # (fileName, speed)
            'walk': ('john-walk.egg', 0.0),
        },
        'trackedJoint': 'RightToeBase',
    },
    'pilot': {
        'fileName': 'pilot-model.egg',
        'anims': {
            # 'charge':('pilot-charge.egg', 0.0),
            # 'chargeloop':('pilot-chargeloop.egg', 0.0),
            'chargeshoot': ('pilot-chargeshoot.egg', 0.0),
            'chargewindup': ('pilot-chargewindup.egg', 0.0),
            'crash': ('pilot-crash.egg, 0.0', 0.0),
            'discloop': ('pilot-discloop.egg', 0.0),
            'discwinddown': ('pilot-discwinddown.egg', 0.0),
            'discwindup': ('pilot-discwindup.egg', 0.0),
            # 'fire':('pilot-fire.egg', 0.0),
            'firewinddown': ('pilot-firewinddown.egg', 0.0),
            'firewindup': ('pilot-firewindup.egg', 0.0),
            'idle': ('pilot-idle.egg', 0.0),
            'newdeath': ('pilot-newdeath.egg', 0.0),
            'newidle': ('pilot-newidle.egg', 0.0),
            # 'pain':('pilot-pain.egg', 0.0),
        },
        'trackedJoint': 'Bone_l_hand',
    },
    'ralph': {
        'fileName': 'ralph.egg',
        'anims': {
            'run': ('ralph-run.egg', 0.0),
            'walk': ('ralph-walk.egg', 0.0),
            'jump': ('ralph-jump.egg', 0.0),
        },
        'trackedJoint': 'LeftHand',
    },
    'eve': {
        'fileName': 'eve.egg',
        'anims': {
            'run': ('eve-run.egg', 10.06),
            'walk': ('eve-walk.egg', 4.37),
            'offbalance': ('eve-offbalance.egg', 0.0),
            'tireroll': ('eve-tireroll.egg', 0.0),
        },
        'trackedJoint': 'LeftBall',
    },
    'sonic': {
        'fileName': 'sonic.egg',
        'anims': {
            'run': ('sonic-run.egg', 0.0),
            'board': ('sonic-board.egg', 0.0),
            'fallingwboard': ('sonic-fallingwboard.egg', 0.0),
            'fallingwoboard': ('sonic-fallingwoboard.egg', 0.0),
            'win': ('sonic-win.egg', 0.0),
        },
        'trackedJoint': 'LeftWrist',
    },
}
