'''
Created on Sep 15, 2017

@author: Executor (https://www.panda3d.org/forums/viewtopic.php?t=7202), consultit
'''

from panda3d.core import load_prc_file, load_prc_file_data, Filename
from direct.showbase.ShowBase import ShowBase
from direct.interval.IntervalGlobal import LerpFunc
from direct.interval.MetaInterval import Sequence
from direct.interval.FunctionInterval import Func
from direct.actor.Actor import Actor
import sys
import os
import random
import argparse
import textwrap

# Get the location and Convert that to panda's unix-style notation.
datadir = os.path.abspath(sys.path[0])
datadir = os.path.join(datadir, '..')
datadir = Filename.fromOsSpecific(datadir).getFullpath()

load_prc_file(datadir + '/config.prc')
load_prc_file_data('', 'win-size 1024 768')
# load_prc_file_data('', 'want-tk #t')


class Animation:
    '''
    Event driven Animation class.

    Animations with corresponding names should already have been loaded by the actor.
    Events should be listed chronologically.
    animations=[{'Animation':   'Animation name',
              'Events':
              [{'Func':      'function name',
                'Frame':      'frame on which to call the function',
                'Args':      'extra arguments'}]
             }]

    :param parent : The parent object which has an Actor as member named 'Model'.
    :param name : Name of Animation object.
    :param loop : To loop the Animation or not.
    :param endseqfunc : The end sequence function which is called at the 
                      end of the sequence. This function should handle looping.    
    '''

    def __init__(self, parent, name, loop=0, endseqfunc=None, animations=None):

        self.parent = parent
        self.name = name
        self.loop = loop
        self.animations = animations
        self.num = 0
        self.sequence = {}

        if self.animations:
            for item in animations:
                self.parent.actor.setControlEffect(item['Animation'], 0)
                count = 0
                ctrl = self.parent.actor.getAnimControl(item['Animation'])
                if ctrl:
                    count = ctrl.getNumFrames()
                self.sequence[item['Animation']] = Sequence()
                lastanim = 0
                if item['Events']:
                    for event in item['Events']:
                        self.sequence[item['Animation']].append(
                            self.parent.actor.actorInterval(
                                item['Animation'], startFrame=lastanim + 1, endFrame=event['Frame']))
                        self.sequence[item['Animation']].append(
                            Func(eval(event['Func']), item['Animation'], args=event['Args']))
                        lastanim = event['Frame']
                if lastanim < count:
                    self.sequence[item['Animation']].append(
                        self.parent.actor.actorInterval(
                            item['Animation'], startFrame=(lastanim + 1), endFrame=count))
                self.sequence[item['Animation']].append(Func(endseqfunc, self))

    def printFrameNum(self, anim, args):
        ctrl = self.parent.actor.getAnimControl(anim)
        if ctrl:
            print('Run event on frame', ctrl.getFrame())

    def startSequence(self):
        self.setControlEffect(0)
        self.randomize()
        self.parent.actor.loop(self.getAnim())
        self.sequence[self.getAnim()].start()
        self.setControlEffect(1)

    def finishSequence(self):
        self.sequence[self.getAnim()].finish()
        self.setControlEffect(0)

    def getAnim(self):
        return self.animations[self.num]['Animation']

    def getAnimControl(self):
        return self.parent.actor.getAnimControl(self.getAnim())

    def isPlaying(self):
        return self.parent.actor.getAnimControl(self.getAnim()).isPlaying()

    def randomize(self):
        if len(self.animations) == 0:
            print('Animation list empty')
            return False
        self.num = int(random.random() * len(self.animations))

    def setControlEffect(self, effect):
        self.parent.actor.setControlEffect(self.getAnim(), effect)


class Model:

    def __init__(self, app):
        self.app = app
        self.actor = Actor('pilot-model.egg')
        self.actor.load_anims({
            'chargeshoot': 'pilot-chargeshoot.egg',
            'chargewindup': 'pilot-chargewindup.egg',
            'crash': 'pilot-crash.egg',
            'discloop': 'pilot-discloop.egg',
            'discwinddown': 'pilot-discwinddown.egg',
            'discwindup': 'pilot-discwindup.egg',
            'firewinddown': 'pilot-firewinddown.egg',
            'firewindup': 'pilot-firewindup.egg',
            'idle': 'pilot-idle.egg',
            'newdeath': 'pilot-newdeath.egg',
            'newidle': 'pilot-newidle.egg',
        })
        self.currentAnim = None
        self.previousAnim = None
        self.blending = False
        self.animation = {}
        anims = [{'Animation': 'chargeshoot',
                  'Events': [
                  ]
                  }]
        self.animation['chargeshoot'] = Animation(
            self, 'walk', 1, self.endSequence, anims)
        anims = [{'Animation': 'crash',
                  'Events': [
                  ]
                  }]
        self.animation['crash'] = Animation(
            self, 'idle', 1, self.endSequence, anims)
        anims = [{'Animation': 'firewindup',
                  'Events': [
                      {'Func': 'self.printFrameNum',
                       'Frame': 20,
                       'Args': None}
                  ]
                  }]
        self.animation['firewindup'] = Animation(self, 'primarymeleeattack', 0,
                                                 self.endSequence, anims)
        self.actor.reparent_to(self.app.render)
        self.actor.set_blend(animBlend=True)
        self.app.accept('1', self.playAnim, [self.animation['chargeshoot']])
        self.app.accept('2', self.playAnim, [self.animation['crash']])
        self.app.accept('mouse1', self.playAnim, [
                        self.animation['firewindup']])

    def playAnim(self, animobj):
        if self.currentAnim == animobj or self.blending:
            return False

        if self.currentAnim:
            self.previousAnim = self.currentAnim
            self.blending = True
            Sequence(LerpFunc(self.setControlEffect, fromData=1, toData=0, duration=0.1,
                              extraArgs=[self.currentAnim]), Func(self.blendDone)).start()

        self.startSequence(animobj)

    def startSequence(self, animobj):
        self.currentAnim = animobj
        animobj.startSequence()

    def endSequence(self, animobj):
        animobj.finishSequence()

        if self.currentAnim == animobj:
            if animobj.loop:
                animobj.startSequence()
            elif self.previousAnim != None:
                self.playAnim(self.previousAnim)
                self.previousAnim = None

    def blendDone(self):
        self.blending = False

    def setControlEffect(self, t, arg):
        # print('set effect',t,arg)
        arg.setControlEffect(t)


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''
    This script will test Anim Events.
      
    Commands:
        - '': .
        - '': .
    '''))
    # set up arguments
    parser.add_argument('-d', '--data-dir', type=str,
                        action='append', help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    # set model dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    app = ShowBase()
    m = Model(app)

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 200.0, -20.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
