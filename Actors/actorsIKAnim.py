'''
Created on Dec 16, 2019

@author: consultit
'''

from panda3d.core import load_prc_file_data, LVector3f, BitMask32, LVecBase4f, \
    LPoint3f, LMatrix4f, TransformState, CardMaker, LVecBase3f, TextureStage
from direct.showbase.ShowBase import ShowBase
from ely.direct.ccdik import CCDik
from ely.samples.direct.common import loadActor
from ely.samples.direct.picker import Picker
from ely.samples.direct.driver import Driver
from ely.samples.direct.utilities import Utilities as utils
import argparse
import textwrap
import random


def updateJointsLocalTransFromAnim(ccdik, jointTrans, frame):
    '''update joints' local transforms'''

    # traverse the hierarchy and jointTrans in preorder
    for p, pT in zip(ccdik.jHierarchyTree.preorder(), jointTrans.preorder()):
        animChannel = p.element()['joints'].get_bound(0)
        mat = LMatrix4f()
        animChannel.get_value(frame, mat)
        pT.element()['local'] = TransformState.make_mat(mat)


def handleActorUpdate(actorDriver, animCtrl, playing, actorHeight, U, app,
                      animRateFactor, task):
    '''handles actor on every update'''

    # get current forward velocity size
    currentVelSize = abs(actorDriver.get_current_speeds()[0].get_y())
    # handle vehicle's animation
    if actorDriver.is_forward_enabled() or actorDriver.is_backward_enabled():
        if actorDriver.is_backward_enabled():
            currentVelSize = -currentVelSize
        animCtrl.set_play_rate(currentVelSize * animRateFactor)
        if not playing[0]:
            animCtrl.loop(False)
            playing[0] = True
    else:
        # stop any animation
        if playing[0]:
            currFrame = animCtrl.get_full_fframe()
            endFrame = animCtrl.get_num_frames()
            animCtrl.play(currFrame, endFrame)
            playing[0] = False
        animCtrl.set_play_rate(currentVelSize * animRateFactor)
    # correct position on uneven terrain
    pos = actor.get_pos()
    pOrig = pos + LPoint3f(0, 0, actorHeight)
    gotCollisionZ = U.get_collision_height(pOrig, app.render)
    if gotCollisionZ[0]:
        deltaHeight = pOrig.get_z() - gotCollisionZ[1]
        actor.set_z(pos.get_z() + actorHeight - deltaHeight)
    # print('handleActorUpdate')
    #
    return task.cont


def makeCCDikStepAnim(render, ccdik, animCtrl, jointTrans, jointTransEE,
                      jBaseParentTransPrev, Pd, Od, IKNEEDED, minZ,
                      utilities, useOrient, task):
    '''performs a CCDik step with/without animation'''

    global debugDraw
    # get the joints' local transforms for the current animation frame
    updateJointsLocalTransFromAnim(ccdik, jointTrans, animCtrl.get_frame())
    # compute the delta joints' world transforms (tree's array based
    # representation)
    jointsDeltaWorldTransArray = []
    # the parent's initial world transform
    worldTransI = jBaseParentTransPrev[0]
    # the parent's final world transform
    # worldTransF = jBaseParent.get_transform(render)
    jBaseParent = ccdik.jHierarchyTree.root().element()['jBaseParent']
    worldTransF = ccdik.actor.get_transform().compose(
        jBaseParent.get_transform())
    jBaseParentTransPrev[0] = worldTransF

    # check if jBaseParent's transform has changed (use heuristic)
    deltaWorldTrans = worldTransF.compose(
        worldTransI.get_inverse())
    # check if there was a change of jBaseParent's transform (use heuristic)
    baseDeltaPosLen = deltaWorldTrans.get_pos().length_squared()
    baseDeltaRotLen = deltaWorldTrans.get_hpr().length_squared()

    # traverse the hierarchy in breadthfirst order
    for p, pT in zip(ccdik.jHierarchyTree.breadthfirst(),
                     jointTrans.breadthfirst()):
        jParamsT = pT.element()
        jControlNodesTrans = p.element()['jControlNodes'].get_transform()
        # the joint's initial world transform
        if jointTrans.is_root(pT):
            jParamsT['worldI'] = worldTransI.compose(jControlNodesTrans)
            jParamsT['worldFAnim'] = worldTransF.compose(
                jParamsT['local'])
            jParamsT['worldF'] = worldTransF
        else:
            jParamsTP = jointTrans.parent(pT).element()
            jParamsT['worldI'] = jParamsTP['worldI'].compose(
                jControlNodesTrans)
            jParamsT['worldFAnim'] = jParamsTP['worldFAnim'].compose(
                jParamsT['local'])
            jParamsT['worldF'] = jParamsTP['worldF']
        # the joint's final world transform
        if IKNEEDED:
            # IK is being applied: use the current transform
            jParamsT['worldF'] = jParamsT['worldF'].compose(
                jControlNodesTrans)
        else:
            # IK is not being applied: use transform from FK (animation)
            jParamsT['worldF'] = jParamsT['worldFAnim']
            p.element()['jControlNodes'].set_transform(jParamsT['local'])
        # compute the joint's delta transform: from the initial to the final
        # ones
        deltaTrans = jParamsT['worldF'].compose(
            jParamsT['worldI'].get_inverse())
        # store the joint's delta transform
        jointsDeltaWorldTransArray.append(deltaTrans)
    # apply the delta joints' world transforms to joint hierarchy
    # Note: remember that the nodes in jointsDeltaWorldTransArray are ordered
    # as the breadthfirst traversal.
    ccdik.applyJointsTransforms(jointsDeltaWorldTransArray)
    # check every end effector's joint transform
    for pEE in jointTransEE:
        jParamsEE = pEE.element()
        name = jParamsEE['name']
        # find the candidate target for this end effector (stated by the
        # animation)
        eePos = LPoint3f(jParamsEE['worldFAnim'].get_pos())
        # collision height from terrain
        eeCollisionZ = utilities.get_collision_height(eePos, render)
        # EE's position should be fixed on the uneven terrain
        if ((eePos.get_z() <= minZ[name] + eeCollisionZ[1]) and
                ((baseDeltaPosLen > 0.0001) or (baseDeltaRotLen > 0.0001))):
            # EE is on the terrain
            if not (name in IKNEEDED):
                # fixed position is set the first time on the terrain
                IKNEEDED.add(name)
                Pd[name] = eePos
            # print('Pd[' + name +']: ' + str(Pd[name]))
            # correct the height of the fixed position
            Pd[name].set_z(eeCollisionZ[1] + minZ[name])
        else:
            # EE is not on the terrain
            if name in IKNEEDED:
                IKNEEDED.remove(name)
            # follow the animation
            Pd[name] = eePos
    if IKNEEDED:
        if useOrient:
            # 1) prepare orientation target: it should follow that of the actor
            actorDirs = (-render.get_relative_vector(ccdik.actor,
                                                     LVector3f.right()),
                         -render.get_relative_vector(ccdik.actor,
                                                     LVector3f.forward()),
                         render.get_relative_vector(ccdik.actor,
                                                    LVector3f.up()))
            for pEE in jointTransEE:
                Od[pEE.element()['name']] = actorDirs
            # compute IK
            ccdik.iterateIK(Pd, Od, tolerance=0.01, relError=0.01,
                            maxIterations=100)
        else:
            # 2) ignore orientations
            # compute IK
            ccdik.iterateIK(Pd, None, tolerance=0.01, relError=0.01,
                            maxIterations=100)
        # update joint node transforms
        CCDik.updateJointNodesLocalTransforms(render, ccdik.jHierarchyTree)
        print('iterationCycleCount:' + str(ccdik.iterationCycleCount))
    #
    if debugDraw:
        CCDik._debugDraw(ccdik, app, color=LVecBase4f(0.0, 0.5, 1.0, 1.0))
        # print('makeCCDikStepAnim: ' + ccdik.joints[ccdik.jNum - 1].get_name())
    #
    return task.cont


def moveActor(data, actorDriver, forwardMove, leftMove, backwardMove,
              rightMove):
    '''actor's movement callback'''

    if not actorDriver:
        return

    action = data
    if action > 0:
        # start movement
        enable = True
    else:
        action = -action
        # stop movement
        enable = False
    #
    if action == forwardMove:
        actorDriver.enable_forward(enable)
    elif action == leftMove:
        actorDriver.enable_head_left(enable)
    elif action == backwardMove:
        actorDriver.enable_backward(enable)
    elif action == rightMove:
        actorDriver.enable_head_right(enable)


def buildCCDikData(ccdik):
    '''
    Builds data structures needed by makeCCDikStepAnim() task.
    '''

    # we need a tree (jointTrans) structurally equivalent to that of joint
    # hierarchy, and in which we store the various joints' transforms used
    # to obtain the final pose of the actor; every node of jointTrans has
    # the same name as the corresponding node of the hierarchy
    jointTrans = ccdik.jHierarchyTree._make_copy(
        copyElement=lambda e: {'name': e['name']})

    # we need a list of jointTrans' nodes each one corresponding to an end
    # effectors of the hierarchy; we prepare the Pd and Od dictionaries
    # needed by CCDik.iterateIK()
    eelist = []
    Pd = {}
    Od = {}

    # we need a per end effector flag indicating whether IK correction is
    # needed or not
    IKNEEDED = set()
    for p, pT in zip(ccdik.jHierarchyTree.breadthfirst(),
                     jointTrans.breadthfirst()):
        if p in ccdik.jEndEffectors:
            eelist.append(pT)
            name = pT.element()['name']
            Pd[name] = None
            Od[name] = None

    # return data
    return (jointTrans, eelist, Pd, Od, IKNEEDED)


random.seed()
randNum = random.random

load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test the walking cycle using CCDIK with animations.
      
    Commands:
        - 'arrow keys': use the keys to move the actor.
    '''))
    # set up arguments
    parser.add_argument('actorData', type=str,
                        help='the actor model data path')
    parser.add_argument('-a', '--anim', type=str, default='walk',
                        choices=['walk', 'run'], help='the actor\'s animation')
    parser.add_argument('-u', '--use-orientation', action='store_true',
                        help='when specified IK will use desired orientations, default is not')
    parser.add_argument('-g', '--debug-draw', action='store_true',
                        help='enable debug drawing')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    actorData = args.actorData
    actorAnim = args.anim
    debugDraw = args.debug_draw
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #
    app = ShowBase()

    # read actor data
    exec(open(actorData).read(), globals())
    global data
    useOrient = args.use_orientation

    # get an actor
    actor = loadActor(data.modelName)
    actor.reparent_to(app.render)

    # handle skeleton's animation, and bind it to the skeleton
    animCtrl = actor.get_anim_control(actorAnim, 'modelRoot',
                                      allowAsyncBind=False)
    # set start frame
    animCtrl.pose(0)

    # setup the right CCDik object
    ccdikR = CCDik(actor, data.jointHierarchyArrayR, alpha=10.0)
    CCDik.initializeJointNodes(actor, ccdikR.jHierarchyTree,
                               jointBaseParent=data.jointBaseParentR)
    CCDik.updateJointNodesLocalTransforms(app.render, ccdikR.jHierarchyTree)

    # setup the left CCDik object
    ccdikL = CCDik(actor, data.jointHierarchyArrayL, alpha=10.0)
    CCDik.initializeJointNodes(actor, ccdikL.jHierarchyTree,
                               jointBaseParent=data.jointBaseParentL)
    CCDik.updateJointNodesLocalTransforms(app.render, ccdikL.jHierarchyTree)

    # create the picker
    PICKABLETAG = 'pickable'
    PICKKEYON = 'a'
    PICKKEYOFF = 'a-up'
    picker = Picker(app, app.render, app.cam, app.mouseWatcher, PICKKEYON,
                    PICKKEYOFF, BitMask32.all_on(), PICKABLETAG)

    # set actor pickable
    actor.set_tag(PICKABLETAG, '')

    # track base's parent world transform
    jBaseParentTransPrevR = [
        ccdikR.jHierarchyTree.root().element()['jBaseParent'].get_transform(
            app.render)]

    jBaseParentTransPrevL = [
        ccdikL.jHierarchyTree.root().element()['jBaseParent'].get_transform(
            app.render)]

    # synchronize animation and movements
    actorDriver = None
    forwardMove = 1
    forwardMoveStop = -1
    leftMove = 2
    leftMoveStop = -2
    backwardMove = 3
    backwardMoveStop = -3
    rightMove = 4
    rightMoveStop = -4

    # driver configuration
    actorDriver = Driver(app, actor, 10)
    actorDriver.set_max_angular_speed(200.0)
    actorDriver.set_angular_accel(100.0)
    actorDriver.set_linear_accel(LVecBase3f(0))
    actorDriver.set_linear_friction(100)
    actorDriver.enable()
    #
    linSpeed = data.ctrlData['linSpeed'] / 10.0
    actorDriver.set_max_linear_speed(LVector3f(linSpeed))
    animRateFactor = 1.0 / linSpeed  # max_linear_speed * animRateFactor = 1.0
    playing = [False]

    # scene
    cm = CardMaker('ground')
    cm.setFrame(1000, -1000, 1000, -1000)
    terrain = app.render.attach_new_node(cm.generate())
    terrain.set_pos(0, 0, 0)
    terrain.set_p(-90)
    terrain.set_texture(app.loader.load_texture('grass_ground2.jpg'))
    terrain.set_tex_scale(TextureStage.default, 10.0, 10.0)

    # utilities
    mask = BitMask32(0x10)
    U = utils(app.render, mask)
    terrain.set_collide_mask(mask)
    modelDims = LVecBase3f()
    modelDeltaCenter = LVecBase3f()
    U.get_bounding_dimensions(actor, modelDims, modelDeltaCenter)
    actorHeight = modelDims.get_z()

    # actor will be driven by arrows keys
    app.accept('arrow_up', moveActor, [forwardMove, actorDriver, forwardMove,
                                       leftMove, backwardMove, rightMove])
    app.accept('arrow_up-up', moveActor, [forwardMoveStop, actorDriver,
                                          forwardMove, leftMove, backwardMove,
                                          rightMove])
    app.accept('arrow_left', moveActor, [leftMove, actorDriver, forwardMove,
                                         leftMove, backwardMove, rightMove])
    app.accept('arrow_left-up', moveActor, [leftMoveStop, actorDriver,
                                            forwardMove, leftMove, backwardMove,
                                            rightMove])
    app.accept('arrow_down', moveActor, [backwardMove, actorDriver, forwardMove,
                                         leftMove, backwardMove, rightMove])
    app.accept('arrow_down-up', moveActor, [backwardMoveStop, actorDriver,
                                            forwardMove, leftMove, backwardMove,
                                            rightMove])
    app.accept('arrow_right', moveActor, [rightMove, actorDriver, forwardMove,
                                          leftMove, backwardMove, rightMove])
    app.accept('arrow_right-up', moveActor, [rightMoveStop, actorDriver,
                                             forwardMove, leftMove,
                                             backwardMove, rightMove])

    # set actor
    actor.set_pos(0, 0, -actorHeight / 2.0)

    # update actor's movement parameters
    app.task_mgr.add(handleActorUpdate, 'handleActorUpdate', appendTask=True,
                     sort=5, extraArgs=[actorDriver, animCtrl, playing,
                                        actorHeight, U, app, animRateFactor])

    # minZ
    # 0.396054387093 RightToeBase
    # 1.11598670483 RightFoot
    # 0.390445828438 LeftToeBase
    # 1.03538942337 LeftFoot

    # right update task
    jointTransR, jointTransEER, PdR, OdR, IKNEEDEDR = buildCCDikData(ccdikR)
    app.task_mgr.add(
        makeCCDikStepAnim, 'makeCCDikStepAnim', appendTask=True, sort=-10,
        extraArgs=[app.render, ccdikR, animCtrl, jointTransR, jointTransEER,
                   jBaseParentTransPrevR, PdR, OdR, IKNEEDEDR, data.minZR, U, useOrient])

    # left update task
    jointTransL, jointTransEEL, PdL, OdL, IKNEEDEDL = buildCCDikData(ccdikL)
    app.task_mgr.add(
        makeCCDikStepAnim, 'makeCCDikStepAnim', appendTask=True, sort=-10,
        extraArgs=[app.render, ccdikL, animCtrl, jointTransL, jointTransEEL,
                   jBaseParentTransPrevL, PdL, OdL, IKNEEDEDL, data.minZL, U, useOrient])

    if debugDraw:
        # first right/left debug draw
        ccdikR._debugDraw(LVecBase4f(1.0, 0.5, 0.0, 1.0))
        ccdikL._debugDraw(LVecBase4f(1.0, 0.5, 0.0, 1.0))

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 55.0, -7.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
