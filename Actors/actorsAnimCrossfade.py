'''
Created on Apr 3, 2020

@author: consultit
'''

from ely.direct.animTransition import AnimTransition
from panda3d.core import load_prc_file_data, LVector3f
from direct.showbase.ShowBase import ShowBase
from actorsAnim_common import getDimensions
import argparse
import textwrap
import os
from direct.actor.Actor import Actor

scriptPath = os.path.dirname(os.path.abspath(__file__))
prefD = os.path.join(scriptPath, '..', 'assets')
dataDirs = [
    os.path.join(prefD, 'misc', 'test'),
    os.path.join(prefD, 'models', 'test'),
    os.path.join(prefD, 'textures', 'test'),
    os.path.join(prefD, 'sounds', 'test'),
    os.path.join(prefD, 'scripts', 'test'),
    os.path.join(prefD, 'shaders', 'test'), ]

for dataDir in dataDirs:
    load_prc_file_data('', 'model-path ' + dataDir)
load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')


def transCmd(animTrans, cmd):
    '''start/stop/pause/resume transition
    '''
    if cmd == 's':
        print('transition started')
        animTrans.startTransition()
    elif cmd == 'e':
        print('transition stopped')
        animTrans.stopTransition()
    elif cmd == 'p':
        print('transition paused')
        animTrans.pauseTransition()
    elif cmd == 'r':
        print('transition resumed')
        animTrans.resumeTransition()


def getAnimToFrame(animFromFrame):
    global args
    frameFrom = animFromFrame % args.from_cycle_length
    frameTo = (frameFrom * args.to_cycle_length / args.from_cycle_length
               + args.delta)
    if frameTo >= args.to_cycle_length:
        frameTo -= args.to_cycle_length
    return frameTo


def getPlayRateRatios(fromCycleLen, toCycleLen):
    return (fromCycleLen / toCycleLen, toCycleLen / fromCycleLen)


def transDone():
    global animTrans
    animTrans.transitionDone()
    print('Transition Done!')


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test smooth animation transitions
      
    Commands:
        - 's': start transition.
        - 'e': stop transition.
        - 'p': pause transition.
        - 'r': resume transition.
    '''))
    # set up arguments
    parser.add_argument('model', type=str, choices=['eve', 'ralph'],
                        help='the actor\'s model')
    parser.add_argument('--from-anim', type=str, default='walk',
                        help='the actor\'s from animation')
    parser.add_argument('--to-anim', type=str, default='run',
                        help='the actor\'s to animation')
    parser.add_argument('--use-pose', action='store_true',
                        help='use pose')
    parser.add_argument('--loop', action='store_true',
                        help='loop animation')
    parser.add_argument('--play-rate', type=float, default=1.0,
                        help='animations\' play rate')
    parser.add_argument('--play-rate-ratios', type=float,
                        nargs=2, help='animations\' play rate ratios')
    parser.add_argument('--duration', type=float,
                        help='animation duration')
    parser.add_argument('--from-cycle-length', type=float,
                        help='the from animation cycle length')
    parser.add_argument('--to-cycle-length', type=float,
                        help='the to animation cycle length')
    parser.add_argument('--delta', type=float,
                        help='delta frame used to correct animToFrame')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #
    app = ShowBase()

    # get an actor
    actor = Actor(args.model, {
        args.from_anim: args.from_anim,
        args.to_anim: args.to_anim,
    })
    actor.reparent_to(app.render)

    # # ANIMATION TRANSITION

    # create AnimTransition
    animTrans = AnimTransition(app, actor.get_anim_control(
        args.from_anim), actor.get_anim_control(args.to_anim))
    animTrans.playRate = args.play_rate
    animTrans.doneEvent = 'TransDone'
    animTrans.loop = args.loop
    animTrans.usePose = args.use_pose
    animTrans.animToFrame = getAnimToFrame
    animTrans.duration = args.duration
    if args.play_rate_ratios and (list(args.play_rate_ratios) == [0, 0]):
        animTrans.playRateRatios = getPlayRateRatios(
            args.from_cycle_length, args.to_cycle_length)
    else:
        animTrans.playRateRatios = args.play_rate_ratios

    # start/stop/pause/resume transition
    app.accept('s', transCmd, extraArgs=[animTrans, 's'])
    app.accept('e', transCmd, extraArgs=[animTrans, 'e'])
    app.accept('p', transCmd, extraArgs=[animTrans, 'p'])
    app.accept('r', transCmd, extraArgs=[animTrans, 'r'])

    app.accept('TransDone', transDone)

    # play initial animation
    actor.set_play_rate(args.play_rate, args.from_anim)
    actor.loop(args.from_anim)
    actor.set_h(-45.0)

    # setup camera
    # get 'tight' dimensions of actor
    _, xC, yC, zC, xL, yL, zL = getDimensions(actor)
    #
    app.trackball.node().set_origin(LVector3f(xL, yL, zL))
    app.trackball.node().set_pos(-xC + xL, -yC + yL, -zC + zL)
    app.trackball.node().set_hpr(0, 0, 0)
    # run
    app.run()
