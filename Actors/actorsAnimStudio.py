'''
Created on Apr 8, 2020

@author: consultit
'''

from panda3d.core import LPoint3f, WindowProperties
from direct.gui.DirectGui import DGG
from direct.gui.DirectLabel import DirectLabel
from direct.gui.DirectEntry import DirectEntry
from direct.gui.DirectButton import DirectButton
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.ShowBase import ShowBase
import actorsAnim
import ely.direct.utils.walkJointHierarchy as walkJointHierarchy
import os
import subprocess
import sys


def startStudio(argv):
    '''setup track base joint transform's GUI'''
    
    #
    actorLabel = DirectLabel(pos=LPoint3f(-0.7, 0, 0.89),
        text='Actor', scale=0.11, text_fg=(0, 0.3, 0.5, 1))
    actorEntry = DirectEntry(pos=LPoint3f(-0.5, 0, 0.9),
        scale=0.05, initialText='actor', width=7, numLines=1,
        text_fg=(0, 0.5, 1.0, 1))
    #
    modeLabel = DirectLabel(pos=LPoint3f(-0.8, 0, 0.465),
        text='mode', scale=0.07, text_fg=(0.6, 0, 0, 1))
    modeEntry = DirectEntry(pos=LPoint3f(-0.6, 0, 0.47),
        scale=0.05, initialText='info', width=7, numLines=1,
        text_fg=(1, 0, 0, 1))
    #
    animLabel = DirectLabel(pos=LPoint3f(-0.8, 0, 0.365),
        text='animation', scale=0.07, text_fg=(0.6, 0, 0, 1))
    animEntry = DirectEntry(pos=LPoint3f(-0.6, 0, 0.37),
        scale=0.05, initialText='anim', width=7, numLines=1,
        text_fg=(1, 0, 0, 1))
    #
    playRateLabel = DirectLabel(pos=LPoint3f(-0.8, 0, 0.265),
        text='play rate', scale=0.07, text_fg=(0.6, 0, 0, 1))
    playRateEntry = DirectEntry(pos=LPoint3f(-0.6, 0, 0.27),
        scale=0.05, initialText='1.0', width=7, numLines=1,
        text_fg=(1, 0, 0, 1))

    # actorsAnim app
    def _startActorsAnim(actorEntry, modeEntry, animEntry, playRateEntry,
                         *args):
        totArgs = [actorEntry.get(), '-m', modeEntry.get(),
                   '-a', animEntry.get(), '-p', playRateEntry.get(), *args]
        # actorsAnim.main(totArgs)
        modulePath = os.path.abspath(actorsAnim.__file__)
        cmdLine = [sys.executable] + [modulePath] + totArgs
        subprocess.Popen(cmdLine)

    actorsAnimLabel = DirectLabel(pos=LPoint3f(-0.9, 0, 0.6),
        text='actorsAnim app', scale=0.1, text_fg=(0, 0.7, 0.7, 1))
    actorsAnimButton = DirectButton(pos=LPoint3f(0.1, 0, 0.6),
        text=('start'), text_fg=(0, 0, 0, 1), scale=0.1,
        command=_startActorsAnim,
        extraArgs=[actorEntry, modeEntry, animEntry, playRateEntry, *argv])
    
    # walkJointHierarchy app
    def _startWalkJointHierarchy(actorEntry, *args):
        totArgs = ['-a', actorEntry.get(), *args]
        modulePath = os.path.abspath(walkJointHierarchy.__file__)
        cmdLine = [sys.executable] + [modulePath] + totArgs
        subprocess.Popen(cmdLine)

    walkJointHierarchyLabel = DirectLabel(pos=LPoint3f(-0.75, 0, -0.1),
        text='walkJointHierarchy app', scale=0.1, text_fg=(0, 0.7, 0.7, 1))
    walkJointHierarchyButton = DirectButton(pos=LPoint3f(0.1, 0, -0.1),
        text=('start'), text_fg=(0, 0, 0, 1), scale=0.1,
        command=_startWalkJointHierarchy,
        extraArgs=[actorEntry, *argv])
   

if __name__ == '__main__':
    app = ShowBase()
    startStudio(sys.argv[1:])
    winTitle = ('Actor Anim Studio')
    props = WindowProperties()
    props.setTitle(winTitle)
    app.win.requestProperties(props)
    app.run()
