'''
Created on May 29, 2020

@author: consultit
'''

from recipe_577197_1 import SortedCollection
import argparse
import textwrap
from panda3d.core import load_prc_file_data, LVector3f, ClockObject
from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor
from actorsAnim_common import getDimensions
from collections import namedtuple


class Anim1DLERPBlender(object):
    '''One-dimensional LERP blending for animations.

    :param animCtrlPoints: sequence of pairs of the form: (animCtrl, bx), where animCtrl=animation control, bx=point coordinate
    :type animCtrlPoints: pair sequence

    .. seealso:: '12 Animations Systems.' Game Engine Architecture, by Jason Gregory,
            3rd ed., CRC Press, Taylor &amp; Francis Group, 2019, pp. 721–816.    
    '''

    class _AnimCtrlPoint(object):
        '''Helper class containing data related to an animation to be LERP blended.

        :param animCtrl: the animation control
        :type animCtrl: object implementing interface of class:`panda3d.core.AnimControl`
        :param point: the point of the linear interval on which this animation is located
        :type point: float
        :param rightIntervalInv: the inverse of right interval length of this animation (helper data)
        :type rightIntervalInv: float
        :param index: the index of this animation in a (sorted) list (helper data)
        :type index: int
        '''

        def __init__(self, animCtrl, point, rightIntervalInv=None, index=None):
            self.animCtrl = animCtrl
            self.point = float(point)
            self.rightIntervalInv = rightIntervalInv
            self.index = index

    def __init__(self, animCtrlPoints):
        '''Constructor
        '''

        self._animCtrlPoints = SortedCollection(key=lambda p: p.point)
        for animCtrl, point in animCtrlPoints:
            animCtrlPoint = Anim1DLERPBlender._AnimCtrlPoint(animCtrl, point)
            self._animCtrlPoints.insert(animCtrlPoint)
        # set helper data
        for idx, animCtrlPoint in enumerate(self._animCtrlPoints):
            animCtrlPoint.index = idx
            if idx > 0:
                self._animCtrlPoints[idx - 1].rightIntervalInv = (
                    1.0 / (self._animCtrlPoints[idx].point - self._animCtrlPoints[idx - 1].point))

    def blend(self, blendPoint):
        '''LERP blends the two animations adjacent to blend point.

        :param blendPoint: the blend point
        :type blendPoint: float
        :return: the two adjacent animations
        :rtype: tuple
        '''

        if blendPoint < self._animCtrlPoints[0].point:
            blendPoint = self._animCtrlPoints[0].point
        elif blendPoint > self._animCtrlPoints[-1].point:
            blendPoint = self._animCtrlPoints[-1].point
        # find first anim
        animCtrlPoint1 = self._animCtrlPoints.find_le(blendPoint)
        firstIdx = animCtrlPoint1.index
        if animCtrlPoint1.point == blendPoint:
            # adjust blend factor
            animCtrlPoint1.animCtrl.get_part().set_control_effect(animCtrlPoint1.animCtrl, 1.0)
            animCtrlPoint2 = animCtrlPoint1
            lastIdx = firstIdx
        else:
            lerpValue = ((blendPoint - animCtrlPoint1.point)
                         * animCtrlPoint1.rightIntervalInv)
            animCtrlPoint2 = self._animCtrlPoints[firstIdx + 1]
            # adjust blend factors
            animCtrlPoint1.animCtrl.get_part().set_control_effect(
                animCtrlPoint1.animCtrl, 1.0 - lerpValue)
            animCtrlPoint2.animCtrl.get_part().set_control_effect(
                animCtrlPoint2.animCtrl, lerpValue)
            lastIdx = firstIdx + 1
        # clear all other blend factors
        i = 0
        while i < len(self._animCtrlPoints):
            if (i < firstIdx) or (i > lastIdx):
                animCtrlPoint = self._animCtrlPoints[i]
                animCtrlPoint.animCtrl.get_part().set_control_effect(animCtrlPoint.animCtrl, 0.0)
            i += 1
        # return animation controls with effects != 0
        return (animCtrlPoint1.animCtrl, animCtrlPoint2.animCtrl)

    @property
    def animCtrlPoints(self):
        '''A sorted sequence of (anim control, point) pair.

        :return: a sorted sequence of (anim control, point) pair
        :rtype: sequence of pairs
        '''

        return [(animCtrlPoint.animCtrl, animCtrlPoint.point) for animCtrlPoint in self._animCtrlPoints]

    def enable_blend(self):
        '''Enables blending for all animations.
        '''

        for animCtrlPoint in self._animCtrlPoints:
            animCtrlPoint.animCtrl.get_part().set_anim_blend_flag(True)

    def disable_blend(self):
        '''Disables blending for all animations.
        '''

        for animCtrlPoint in self._animCtrlPoints:
            animCtrlPoint.animCtrl.get_part().set_anim_blend_flag(False)


class Anim2DLERPBlender(object):
    '''Two-dimensional LERP blending for animations.

    The given animation controls are positioned at the four corners of a unit
    square region specified as (0,0),(1,0),(0,1),(1,1).

    :param animCtrls: sequence of four animation controls each specified for the corresponding square corner ordered as (0,0),(1,0),(0,1),(1,1)
    :type animCtrls: sequence

    .. seealso:: '12 Animations Systems.' Game Engine Architecture, by Jason Gregory,
            3rd ed., CRC Press, Taylor &amp; Francis Group, 2019, pp. 721–816.    
    '''

    def __init__(self, animCtrls):
        '''Constructor
        '''

        assert (len(animCtrls) >= 4)
        self._animCtrls = animCtrls

    def blend(self, blendPair):
        '''LERP blends the four animations forming a square using the blend pair.

        :param blendPair: the pair (bx,by) representing the blend factors, each in the interval [0,1] 
        :type blendPair: pair
        '''

        bx, by = blendPair
        if bx < 0.0:
            bx = 0.0
        elif bx > 1.0:
            bx = 1.0
        if by < 0.0:
            by = 0.0
        elif by > 1.0:
            by = 1.0
        # compute coefficients
        C00 = (1.0 - bx) * (1.0 - by)
        C10 = bx * (1.0 - by)
        C01 = (1.0 - bx) * by
        C11 = bx * by
        # adjust blend factors
        self._animCtrls[0].get_part().set_control_effect(
            self._animCtrls[0], C00)
        self._animCtrls[1].get_part().set_control_effect(
            self._animCtrls[1], C10)
        self._animCtrls[2].get_part().set_control_effect(
            self._animCtrls[2], C01)
        self._animCtrls[3].get_part().set_control_effect(
            self._animCtrls[3], C11)

    @property
    def animCtrls(self):
        '''A sequence of four animation controls.

        :return: sequence of four animation controls each specified for the corresponding square corner ordered as (0,0),(1,0),(0,1),(1,1)
        :rtype: sequence
        '''

        return self._animCtrls

    def enable_blend(self):
        '''Enables blending for all animations.
        '''

        for animCtrl in self._animCtrls:
            animCtrl.get_part().set_anim_blend_flag(True)

    def disable_blend(self):
        '''Disables blending for all animations.
        '''

        for animCtrl in self._animCtrls:
            animCtrl.get_part().set_anim_blend_flag(False)


class AnimTriangular2DLERPBlending(object):
    '''Triangular two-dimensional LERP blending for animations.

    :param animCtrlPoints: a sequence of three pairs of the form: (animCtrl, (bx,by)), where animCtrl=animation control, (bx,by)=point coordinates
    :type animCtrlPoints: sequence of three pairs

    .. seealso:: '12 Animations Systems.' Game Engine Architecture, by Jason Gregory,
            3rd ed., CRC Press, Taylor &amp; Francis Group, 2019, pp. 721–816.    
    '''

    _Point2D = namedtuple('_Point2D', ['x', 'y'])

    class _AnimCtrlPoint2D(object):
        '''Helper class containing data related to an animation to be LERP blended.

        :param animCtrl: the animation control
        :type animCtrl: object implementing interface of class:`panda3d.core.AnimControl`
        :param point: a pair representing the point of the 2D space where this animation is located
        :type point: pair
        '''

        def __init__(self, animCtrl, point):
            self.animCtrl = animCtrl
            self.point = AnimTriangular2DLERPBlending._Point2D(*point)

    def __init__(self, animCtrlPoints):
        '''Constructor
        '''

        assert (len(animCtrlPoints) >= 3)
        self._animCtrlPoints = []
        for animCtrl, point in animCtrlPoints:
            self._animCtrlPoints.append(
                AnimTriangular2DLERPBlending._AnimCtrlPoint2D(animCtrl, point))

    def blend(self, blendPoint):
        '''LERP blends the three animations forming the triangle at the blend point.

        :param blendPoint: a pair representing the point of the 2D space
        :type blendPoint: pair
        '''

        Px = blendPoint[0] - self._animCtrlPoints[0].x
        Py = blendPoint[1] - self._animCtrlPoints[0].y
        P1x = self._animCtrlPoints[1].x - self._animCtrlPoints[0].x
        P1y = self._animCtrlPoints[1].y - self._animCtrlPoints[0].y
        P2x = self._animCtrlPoints[2].x - self._animCtrlPoints[0].x
        P2y = self._animCtrlPoints[2].y - self._animCtrlPoints[0].y
        # compute barycentric coordinates a0, a1, a2 (with a0=1-a1-a2)
        a1 = (Px * P2y - Py * P2x) / (P2y * P1x - P2x * P1y)
        a2 = (Py * P1x - Px * P1y) / (P2y * P1x - P2x * P1y)
        # check if blendPoint is inside the triangle
        if a1 < 0.0:
            a1 = -a1
        if a2 < 0.0:
            a2 = -a2
        if (a1 + a2) > 1:
            r = 1.0 / (a1 + a2)
            a1 = r * a1
            a2 = r * a2
        # adjust blend factors
        self._animCtrlPoints[0].animCtrl.get_part().set_control_effect(
            self._animCtrlPoints[0].animCtrl, (1 - a1 - a2))
        self._animCtrlPoints[1].animCtrl.get_part().set_control_effect(
            self._animCtrlPoints[1].animCtrl, a1)
        self._animCtrlPoints[2].animCtrl.get_part().set_control_effect(
            self._animCtrlPoints[2].animCtrl, a2)

    @property
    def animCtrlPoints(self):
        '''A sorted sequence of pairs: (anim control, point)s.

        :return: a sorted sequence of pairs: (anim control, point)s
        :rtype: sequence of pairs
        '''

        return [(animCtrlPoint.animCtrl, animCtrlPoint.point) for animCtrlPoint in self._animCtrlPoints]

    def enable_blend(self):
        '''Enables blending for all animations.
        '''

        for animCtrlPoint in self._animCtrlPoints:
            animCtrlPoint.animCtrl.get_part().set_anim_blend_flag(True)

    def disable_blend(self):
        '''Disables blending for all animations.
        '''

        for animCtrlPoint in self._animCtrlPoints:
            animCtrlPoint.animCtrl.get_part().set_anim_blend_flag(False)


def blendCycle(animBlend1D, blendStart, blendRange, blendSpeed, blendCurr, task):
    dt = ClockObject.get_global_clock().get_dt()
    blendCurr[0] = blendCurr[0] + blendSpeed * dt
    if blendCurr[0] > (blendStart + blendRange):
        blendCurr[0] = blendStart
    animBlend1D.blend(blendCurr[0])
    print(blendCurr[0])
    return task.cont


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test animation N-dimensional LERP blending
    '''))
    # set up arguments
    parser.add_argument('model', type=str, choices=[
                        'eve', 'ralph'], help='the actor\'s model')
    parser.add_argument(
        '--type', type=str, choices=['1d', '2d', '2dtri'], default='1d', help='the type of lerp blending')
    parser.add_argument('--anim-point-1d', type=str,
                        action='append', nargs=2, help='the anim,point pair for 1d type (point is a float)')
    parser.add_argument('--anim-2d', type=str, nargs=4,
                        help='the anim_mm,anim_Mm,anim_mM,anim_MM tuple for 2d type')
    parser.add_argument('-d', '--data-dir', type=str,
                        action='append', help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #
    app = ShowBase()

    if args.type == '1d':
        # get an actor
        actorAnims = {anim: anim for anim, _ in args.anim_point_1d}
        actor = Actor(args.model, actorAnims)
        actor.reparent_to(app.render)

        # set anim-point pairs
        animCtrlPoints = []
        if args.anim_point_1d:
            animCtrlPoints = [(actor.get_anim_control(anim), point)
                              for anim, point in args.anim_point_1d]

        # create Anim1DLERPBlender object
        animBlend1D = Anim1DLERPBlender(animCtrlPoints)

        # enable blend
        animBlend1D.enable_blend()

        # play/loop all animations
        for animCtrl, point in animBlend1D.animCtrlPoints:
            animCtrl.loop(False)

        # lerp blending cycle task
        blendStart = animBlend1D.animCtrlPoints[0][1]
        blendRange = animBlend1D.animCtrlPoints[-1][1] - blendStart
        blendCurr = [blendStart]
        blendSpeed = 0.1

        app.task_mgr.add(blendCycle, 'blendCycle', appendTask=True, sort=0, extraArgs=[
                         animBlend1D, blendStart, blendRange, blendSpeed, blendCurr])
    elif args.type == '2d':
        # get an actor
        actorAnims = {anim: anim for anim in args.anim_2d}
        actor = Actor(args.model, actorAnims)
        actor.reparent_to(app.render)

        # set anims and points
        for anim in args.anim_2d:
            animCtrls = [actor.get_anim_control(anim) for anim in args.anim_2d]

        # create Anim1DLERPBlender object
        animBlend2D = Anim2DLERPBlender(animCtrls)

        # enable blend
        animBlend2D.enable_blend()

        # blend
        animBlend2D.blend((0.33, 0.89))

        # play/loop all animations
        for animCtrl in animBlend2D.animCtrls:
            animCtrl.loop(False)

    # setup camera
    # get 'tight' dimensions of actor
    _, xC, yC, zC, xL, yL, zL = getDimensions(actor)
    #
    app.trackball.node().set_origin(LVector3f(xL, yL, zL))
    app.trackball.node().set_pos(-xC + xL, -yC + yL, -zC + zL)
    app.trackball.node().set_hpr(0, 0, 0)
    # run
    app.run()
