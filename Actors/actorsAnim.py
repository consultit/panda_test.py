'''
Created on Sep 7, 2017

@author: consultit
'''

from panda3d.core import load_prc_file_data, LVecBase3f, LVector3f, CardMaker, \
    BitMask32, TextureStage, WindowProperties, DirectionalLight
from actorsAnim_common import getInfos, animPanelDestroy, getAnimCtrlAttr, \
    getAnimCtrl, movePlayer, handlePlayerUpdate, AnimControlManual, trackAnim, \
    getDimensions, procedurallyGenerating3DModels, cubeVertices, cubeTriangles
from actorsAnim_data import actorData
from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor
from ely.samples.direct.driver import Driver
from ely.samples.direct.utilities import Utilities
import argparse
import textwrap
from sys import exit

load_prc_file_data('', 'win-size 1024 768')


def main(argv):
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test Actor animations
      
    Commands:
        - 'arrow' keys: to move character in 'driver'  mode.
    '''))
    # set up arguments
    parser.add_argument('model', type=str, default='eve',
                        choices=[k for k in actorData.keys()],
                        help='the actor\'s model')
    parser.add_argument('-m', '--mode', type=str, default='driver',
                        choices=['info', 'panel', 'control', 'actor', 'play',
                                 'track', 'driver'],
                        help='the analizyng/testing mode')
    parser.add_argument('-a', '--anim', type=str, default='walk',
                        help='the actor\'s animation')
    parser.add_argument('-p', '--play-rate', type=float, default=1.0,
                        help='the animations\' play rate')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args(argv)

    # do actions
    # set model dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    if args.mode == 'panel':
        # load_prc_file_data('', 'want-directtools #t')
        load_prc_file_data('', 'want-tk #t')

    # some preliminary checks
    if (args.anim and (not args.anim in actorData[args.model]['anims']) and
            (args.mode != 'info') and (args.mode != 'panel')):
        print(args.model, 'doesn\'t have', args.anim, 'animation')
        exit(-1)

    # get an actor
    app = ShowBase()
    actor = Actor(actorData[args.model]['fileName'],
                  {k: v[0] for k, v in actorData[args.model]['anims'].items()})

    actor.reparent_to(app.render)

    # default title
    winTitle = ('Actor Anim \'' + args.model + '\' - mode: ' + args.mode.upper()
                + ' | anim: ' + args.anim + ' | play rate: ' +
                str(args.play_rate))

#     planeModel = procedurallyGenerating3DModels(cubeVertices, cubeTriangles, 12, name='planeModel', color=(
#         0.2, 0.4, 0.5, 1), scale=LVecBase3f(200, 200, 1))
#     planeModel.reparent_to(app.render)
#     planeModel.set_pos(0, -100, -1)

    cm = CardMaker('')
    cm.setFrame(-2, 2, -2, 2)
    floor = app.render.attachNewNode('floor')
    for y in range(12):
        for x in range(12):
            nn = floor.attachNewNode(cm.generate())
            nn.setP(-90)
            nn.setPos((x - 6) * 4, (y - 6) * 4, 0)
#     floor.setTexture(floorTex)
    floor.flattenStrong()

    # add light
    dirLight = app.render.attach_new_node(DirectionalLight
                                          ('directionalLight'))
    dirLight.set_hpr(45, -45, 0)
    dirLight.node().set_scene(app.render)
    dirLight.node().set_shadow_caster(True, 512, 512)
    app.render.set_light(dirLight)
    app.render.set_shader_auto()
    # default camera positioning
    # get 'tight' dimensions of actor
    _, xC, yC, zC, xL, yL, zL = getDimensions(actor)
    #
    app.trackball.node().set_origin(LVector3f(xL, yL, zL))
    app.trackball.node().set_pos(-xC + xL, -yC + yL, -zC + zL)
    app.trackball.node().set_hpr(0, 0, 0)

    if args.mode == 'info':
        # infos
        getInfos(actor)
        winTitle = ('Actor Anim \'' + args.model + '\' - mode: ' +
                    args.mode.upper())

    elif args.mode == 'panel':
        # # ANIMATION MANAGEMENT
        # 1: through AnimPanel
        animPanel = actor.anim_panel()
        animPanel.setDestroyCallBack(animPanelDestroy)
        winTitle = ('Actor Anim \'' + args.model + '\' - mode: ' +
                    args.mode.upper())

    elif args.mode == 'control':
        # # ANIMATION MANAGEMENT
        # 2: through AnimControl
        ctrl = actor.get_anim_control(args.anim, partName='modelRoot',
                                      lodName='lodRoot', allowAsyncBind=False)
        numFrames = getAnimCtrlAttr(ctrl, 'num_frames')
        pingpong = getAnimCtrlAttr(ctrl, 'pingpong')
        pingpong(True, 0, numFrames - 1)

    elif args.mode == 'actor':
        # # ANIMATION MANAGEMENT
        # 3: through Actor
        numFrames = actor.get_num_frames(args.anim, partName='modelRoot')
        actor.pingpong(args.anim, True, partName='modelRoot', fromFrame=0,
                       toFrame=numFrames - 1)

    elif args.mode == 'play':
        # play animation manually (through a task)
        animCtrl = getAnimCtrl(actor, args.anim)
        animCtrlManual = AnimControlManual(app, animCtrl)
        animCtrlManual.play_rate = 2.0
        # animCtrlManual.play_anim(startFrame=10, endFrame=20, doLoop=False)
        animCtrlManual.play_anim(doLoop=True)

    elif args.mode == 'track':
        # track a joint during animation
        partBundle = actor.get_part_bundle('modelRoot')
        animCtrl = getAnimCtrl(actor, args.anim, partName='modelRoot')

        result = []

        trackAnim(app, app.aspect2d, animCtrl, actor, partBundle, result)

    elif args.mode == 'driver':
        # handle animation and bind it to the skeleton
        animCtrl = getAnimCtrl(actor, args.anim)
        # driver configuration
        playerDriver = Driver(app, actor, 10)
        playerDriver.set_max_angular_speed(200.0)
        playerDriver.set_angular_accel(100.0)
        playerDriver.set_linear_accel(LVecBase3f(0))
        playerDriver.set_linear_friction(1)
        playerDriver.enable()
        # set max speed
        maxSpeed = actorData[args.model]['anims'][args.anim][1]
        playerDriver.set_max_linear_speed(LVecBase3f(maxSpeed))
        animRateFactor = 1.0 / maxSpeed  # max_linear_speed*animRateFactor=1.0
        playing = [False]

        # synchronize animation and movements
        forwardMove = 1
        forwardMoveStop = -1
        leftMove = 2
        leftMoveStop = -2
        backwardMove = 3
        backwardMoveStop = -3
        rightMove = 4
        rightMoveStop = -4

        # player will be driven by arrows keys
        app.accept('arrow_up', movePlayer, [forwardMove, playerDriver])
        app.accept('arrow_up-up', movePlayer, [forwardMoveStop, playerDriver])
        app.accept('arrow_left', movePlayer, [leftMove, playerDriver])
        app.accept('arrow_left-up', movePlayer, [leftMoveStop, playerDriver])
        app.accept('arrow_down', movePlayer, [backwardMove, playerDriver])
        app.accept('arrow_down-up', movePlayer,
                   [backwardMoveStop, playerDriver])
        app.accept('arrow_right', movePlayer, [rightMove, playerDriver])
        app.accept('arrow_right-up', movePlayer, [rightMoveStop, playerDriver])

        # scene
        TERRAIN_HEIGHT = 0.0
        cm = CardMaker('ground')
        cm.setFrame(1000, -1000, 1000, -1000)
        terrain = app.render.attach_new_node(cm.generate())
        terrain.set_pos(0, 0, 0)
        terrain.set_p(-90)
        terrain.set_texture(app.loader.loadTexture('grass_ground2.jpg'))
        terrain.set_tex_scale(TextureStage.default, 10.0, 10.0)

        # utilities
        mask = BitMask32(0x10)
        utils = Utilities(app.render, mask)
        terrain.set_collide_mask(mask)
        modelDims = LVecBase3f()
        modelDeltaCenter = LVecBase3f()
        utils.get_bounding_dimensions(actor, modelDims, modelDeltaCenter)
        playerHeight = modelDims.get_z()
        #
        actor.set_pos(0, 0, -playerHeight / 2.0)
        animCtrl.pose(0)

        app.task_mgr.add(handlePlayerUpdate, 'handlePlayerUpdate',
                         appendTask=True, sort=5,
                         extraArgs=[playerDriver, animCtrl, actor,
                                    animRateFactor, playing, playerHeight,
                                    utils, app])
        # setup camera
        trackball = app.trackball.node()
        trackball.set_pos(0.0, 70.0, TERRAIN_HEIGHT)
        trackball.set_hpr(0.0, 5.0, 0.0)

    # set window title
    props = WindowProperties()
    props.setTitle(winTitle)
    app.win.requestProperties(props)

    # run
    app.run()


if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
