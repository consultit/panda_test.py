'''
Created on Dec 14, 2019

@author: consultit
'''

from panda3d.core import load_prc_file_data, LVector3f, BitMask32, LVecBase4f, \
    LPoint3f
from direct.showbase.ShowBase import ShowBase
from ely.direct.ccdik import CCDik
from ely.samples.direct.common import loadActor
from ely.samples.direct.picker import Picker
import argparse
import textwrap
import random
import actorsIK_data
from direct.interval.IntervalGlobal import LerpPosInterval, Sequence

random.seed()
randNum = random.random

load_prc_file_data('', 'win-size 1024 768')
load_prc_file_data('', 'show-frame-rate-meter #t')
load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')


def rotateActor(delta, actor, euler='h'):
    '''Rotates Actor
    '''
    setCmd = getattr(actor, 'set_' + euler)
    getCmd = getattr(actor, 'get_' + euler)
    setCmd(getCmd() + delta)


def makeCCDikStep(render, ccdik, targets, targetOldPos, targetOldHpr,
                  baseJointProxy, baseJointProxyTrans, task):
    '''
    Performs one step of IK's CCD if necessary. 
    '''

    # track the base joint transform
    baseJointProxyTransNew = baseJointProxy.get_transform(app.render)
    worldTrans = baseJointProxyTransNew.compose(
        baseJointProxyTrans[0].get_inverse())
    # check if there was a change of actor's transform (use heuristic)
    baseDeltaPosLen = worldTrans.get_pos().length_squared()
    baseDeltaRotLen = worldTrans.get_hpr().length_squared()
    # check heuristic
    if (baseDeltaPosLen > 0.0001) or (baseDeltaRotLen > 0.0001):
        # actor's transform changed: apply the (world) transform to the
        # whole hierarchy
        ccdik.applyBaseJointTransform(worldTrans)
        baseJointProxyTrans[0] = baseJointProxyTransNew
        baseDelta = True
    else:
        # actor's transform not changed: do nothing
        baseDelta = False

    # check if there was a change on their transforms (use heuristic)
    targetDeltaPosLen = 0.0
    targetDeltaRotLen = 0.0
    for name in targets:
        targetDeltaPosLen += (targetOldPos[name][0] -
                              targets[name].get_pos()).length_squared()
        targetDeltaRotLen += (targetOldHpr[name][0] -
                              targets[name].get_hpr()).length_squared()
    # check heuristic
    if ((targetDeltaPosLen > 0.0001) or (targetDeltaRotLen > 0.0001)
            or baseDelta):
        # (some) targets' transforms have changed: perform CCD iterations
        Pd = {}
        Od = {}
        # compute desired positions/orientations of the end effectors: in
        # this case they follow the positions/orientations of the respective
        # targets
        for name in targets:
            Pd[name] = targets[name].get_pos()
            r = render.get_relative_vector(
                targets[name], LVector3f.right())
            f = render.get_relative_vector(
                targets[name], LVector3f.forward())
            u = render.get_relative_vector(targets[name], LVector3f.up())
            Od[name] = (r, f, u)
        # iterate!
        ccdik.iterateIK(Pd, Od, tolerance=0.01, relError=0.01,
                        maxIterations=100)
        CCDik.updateJointNodesLocalTransforms(render, ccdik.jHierarchyTree)
        for name in Pd:
            targetOldPos[name][0] = targets[name].get_pos()
        for name in Od:
            targetOldHpr[name][0] = targets[name].get_hpr()
        print('iterationCycleCount:' + str(ccdik.iterationCycleCount))
        # update debug drawing
        CCDik._debugDraw(ccdik, app, color=LVecBase4f(0.0, 0.5, 1.0, 1.0))
    #
    return task.cont


def makeAlwaysCCDikStep(render, ccdik, targets, baseJointProxy,
                        baseJointProxyTrans, task):
    '''
    Performs one step of IK's CCD always. 
    '''

    # track the base joint transform
    baseJointProxyTransNew = baseJointProxy.get_transform(app.render)
    worldTrans = baseJointProxyTransNew.compose(
        baseJointProxyTrans[0].get_inverse())
    # apply the (world) transform to the whole hierarchy
    ccdik.applyBaseTransform(worldTrans)
    baseJointProxyTrans[0] = baseJointProxyTransNew
    # perform CCD iterations
    Pd = {}
    Od = {}
    # compute desired positions/orientations of the end effectors: in
    # this case they follow the positions/orientations of the respective
    # targets
    for name in targets:
        Pd[name] = targets[name].get_pos()
        r = render.get_relative_vector(targets[name], LVector3f.right())
        f = render.get_relative_vector(targets[name], LVector3f.forward())
        u = render.get_relative_vector(targets[name], LVector3f.up())
        Od[name] = (r, f, u)
    # iterate!
    ccdik.iterateIK(Pd, Od, tolerance=0.01, relError=0.01,
                    maxIterations=100)
    CCDik.updateJointNodesLocalTransforms(render, ccdik.jHierarchyTree)
    print('iterationCount:' + str(ccdik.iterationCount))
    # update debug drawing
    CCDik._debugDraw(ccdik, app, color=LVecBase4f(0.0, 0.5, 1.0, 1.0))
    #
    return task.cont


def createTarget(model, pos, scale=1.0):
    '''Create targets
    '''
    global app
    if model:
        target = app.loader.load_model(model)
        target.set_tag(PICKABLETAG, '')
        target.set_scale(scale)
        target.reparent_to(app.render)
    else:
        target = app.render.attach_new_node('target')
    target.set_pos(pos)
    return target


def getBaseJointProxy(jHierarchyTree):
    baseJointParams = jHierarchyTree.root().element()
    baseJointTrans = baseJointParams['jControlNodes'].get_transform()
    baseJointProxy = baseJointParams['jBaseParent'].attach_new_node(
        'baseJointProxy')
    baseJointProxy.set_transform(baseJointTrans)
    return baseJointProxy


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test the Actor with IK features.
      
    Commands:
        - 'a': hold the key to pick-and-drag the targets with the mouse.
    '''))
    # set up arguments
    parser.add_argument('model', type=str, default='john',
                        choices=['john', 'ralph'], help='the actor\'s model')
    parser.add_argument('-a', '--anim', type=str, default='walk',
                        choices=['walk', 'run'], help='the actor\'s animation')
    parser.add_argument('-n', '--no-model', action='store_true',
                        help='don\'t use a model as target')
    parser.add_argument('--always-iterate', action='store_true',
                        help='always performs ccdik iterations')
    parser.add_argument('--perform-intervals', action='store_true',
                        help='performs targets\' transforms through itervals')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()

    # do actions
    actorModel = args.model
    actorAnim = args.anim
    isTargetModel = not args.no_model
    alwaysIterate = args.always_iterate
    performIntervals = args.perform_intervals
    # set data dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #
    app = ShowBase()

    # get an actor
    actor = loadActor(actorModel)
    actor.reparent_to(app.render)

    # rotate Actor
    delta = 5.0
    app.accept('r', rotateActor, extraArgs=[delta, actor])
    app.accept('r-repeat', rotateActor, extraArgs=[delta, actor])
    app.accept('shift-r', rotateActor, extraArgs=[-delta, actor])
    app.accept('shift-r-repeat', rotateActor, extraArgs=[-delta, actor])

    # CCDik object
    ikData = actorsIK_data.ikData[actorModel]

    targetScale = ikData['targetScale']
    jointBaseParent = ikData['jointBaseParent']
    jointHierarchyArray = ikData['jointHierarchyArray']

    ccdik = CCDik(actor, jointHierarchyArray, alpha=10.0)
    CCDik.initializeJointNodes(actor, ccdik.jHierarchyTree,
                               jointBaseParent=jointBaseParent)
    CCDik.updateJointNodesLocalTransforms(app.render, ccdik.jHierarchyTree)

    actor.set_h(-90)
    ccdik.applyBaseJointTransform(actor.get_transform())

    # create the picker
    PICKABLETAG = 'pickable'
    PICKKEYON = 'a'
    PICKKEYOFF = 'a-up'
    picker = Picker(app, app.render, app.cam, app.mouseWatcher, PICKKEYON,
                    PICKKEYOFF, BitMask32.all_on(), PICKABLETAG)

    # create the targets with the same position as end effectors
    targets = {}
    targetOldPos = {}
    targetOldHpr = {}
    for pJEE in ccdik.jEndEffectors:
        jeeParam = pJEE.element()
        name = jeeParam['name']
        pos = jeeParam['jGlobalPositions']
        if isTargetModel:
            targets[name] = createTarget('smiley', pos, targetScale)
        else:
            targets[name] = createTarget(None, pos)
        targetOldPos[name] = [targets[name].get_pos()]
        targetOldHpr[name] = [targets[name].get_hpr()]

    # create the only target's interval
    targetNP = next(iter(targets.values()))

    # rotate targets
    app.accept('t', rotateActor, extraArgs=[delta, targetNP, 'r'])
    app.accept('t-repeat', rotateActor, extraArgs=[delta, targetNP, 'r'])
    app.accept('shift-t', rotateActor, extraArgs=[-delta, targetNP, 'r'])
    app.accept('shift-t-repeat', rotateActor,
               extraArgs=[-delta, targetNP, 'r'])

    # track right/left base joint transform
    actor.set_tag(PICKABLETAG, '')

    #
    baseJointProxy = getBaseJointProxy(ccdik.jHierarchyTree)
    baseJointProxyTrans = [baseJointProxy.get_transform(app.render)]

    # right/left IK update task
    if not alwaysIterate:
        app.task_mgr.add(makeCCDikStep, 'makeCCDikStep', appendTask=True,
                         extraArgs=[app.render, ccdik, targets, targetOldPos,
                                    targetOldHpr, baseJointProxy,
                                    baseJointProxyTrans])
    else:
        app.task_mgr.add(makeAlwaysCCDikStep, 'makeAlwaysCCDikStep',
                         appendTask=True, extraArgs=[app.render, ccdik, targets,
                                                     baseJointProxy,
                                                     baseJointProxyTrans])

    # first right/left debug draw
    CCDik._debugDraw(ccdik, app, color=LVecBase4f(1.0, 0.5, 0.0, 1.0))

    if performIntervals:
        p1 = app.render.get_relative_point(targetNP, LPoint3f(12.0, 0.0, 1.0))
        i1 = LerpPosInterval(targetNP, 1.0, p1)

        p2 = app.render.get_relative_point(targetNP, LPoint3f(12.0, 0.0, 9.0))
        i2 = LerpPosInterval(targetNP, 1.0, p2)

        s = Sequence(i1, i2, name='s')
        s.start()

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 55.0, -7.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
