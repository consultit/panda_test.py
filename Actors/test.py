'''
Created on Oct 8, 2017
 
@author: consultit
'''
from panda3d.core import NodePath, Filename, LPoint3f, LMatrix4f, TransformState,\
    LVector3f, LVecBase3f
import argparse
import textwrap

try:
    from panda3d.core import load_prc_file_data
    from direct.showbase.ShowBase import ShowBase

#    load_prc_file_data('', 'window-type none')

    if __name__ == '__main__':
        #         # create the arguments' parser
        #         parser = argparse.ArgumentParser(
        #             formatter_class=argparse.RawDescriptionHelpFormatter,
        #             description=textwrap.dedent('''
        #         Tests.
        #         '''))
        #         # set up arguments
        #         parser.add_argument('-d', '--data-dir', type=str, action='append',
        #                             help='the data dir(s)')
        #         # parse arguments
        #         args = parser.parse_args()
        #
        #         # do actions
        #         # set data dir(s)
        #         if args.data_dir:
        #             for dataDir in args.data_dir:
        #                 load_prc_file_data('', 'model-path ' + dataDir)
        #         #
        #         app = ShowBase()
        #
        #         if app.win:
        #             app.run()

    #### TEST ####
    #         # create the arguments' parser
    #         parser = argparse.ArgumentParser(
    #             formatter_class=argparse.RawDescriptionHelpFormatter,
    #             description=textwrap.dedent('''
    #         Tests.
    #         '''))
    #         # set up arguments
    #         parser.add_argument('-d', '--data-dir', type=str, action='append',
    #                             help='the data dir(s)')
    #         # parse arguments
    #         args = parser.parse_args()
    # 
    #         # do actions
    #         # set data dir(s)
    #         if args.data_dir:
    #             for dataDir in args.data_dir:
    #                 load_prc_file_data('', 'model-path ' + dataDir)
    #         #
    #         app = ShowBase()
    # 
    #         import bisect
    #         from test_py.common.random import RandInt32
    # 
    #         def index(a, x):
    #             'Locate the leftmost value exactly equal to x'
    #             i = bisect.bisect_left(a, x)
    #             if i != len(a) and a[i] == x:
    #                 return i
    #             raise ValueError
    # 
    #         def find_lt(a, x):
    #             'Find rightmost value less than x'
    #             i = bisect.bisect_left(a, x)
    #             if i:
    #                 return a[i - 1]
    #             raise ValueError
    # 
    #         def find_le(a, x):
    #             'Find rightmost value less than or equal to x'
    #             i = bisect.bisect_right(a, x)
    #             if i:
    #                 return a[i - 1]
    #             raise ValueError
    # 
    #         def find_gt(a, x):
    #             'Find leftmost value greater than x'
    #             i = bisect.bisect_right(a, x)
    #             if i != len(a):
    #                 return a[i]
    #             raise ValueError
    # 
    #         def find_ge(a, x):
    #             'Find leftmost item greater than or equal to x'
    #             i = bisect.bisect_left(a, x)
    #             if i != len(a):
    #                 return a[i]
    #             raise ValueError
    # 
    #         l = [RandInt32(100) for _ in range(100)]
    #         a = []
    #         for x in l:
    #             bisect.insort_left(a, x)
    # 
    #         x = 25
    #         try:
    #             print(find_ge(a, x))
    #             print(find_gt(a, x))
    #             print(find_le(a, x))
    #             print(find_lt(a, x))
    #             print(index(a, x))
    #         except ValueError as e:
    #             print(e)
    # 
    #         if app.win:
    #             app.run()

    #### TEST ####
    #         modelRef = app.loader.loadModel(Filename('ralph'))
    #         modelRef.reparent_to(app.render)
    #         modelRef.set_color(1, 0, 0, 1)
    #         modelTest = app.loader.loadModel(Filename('eve'))
    #         modelTest.reparent_to(app.render)
    #         modelTest.set_color(0, 1, 0, 1)
    #
    #         # verify panda3d transform through matrix conventions: row or column based
    #         # 1) translation
    # #         modelRef.set_pos(LPoint3f(10, 0, 0))
    # #         posMat = LMatrix4f.translate_mat(LPoint3f(10, 0, 0))
    # #         testMatTrans = TransformState.make_mat(posMat)
    # #         modelTest.set_transform(testMatTrans)
    #         # 2) rotation -> translation
    # #         modelRef.set_h(45)
    # #         modelRef.set_pos(LPoint3f(10, 0, 0))
    # #         posMat = LMatrix4f.translate_mat(LPoint3f(10, 0, 0))
    # #         posMatTrans = TransformState.make_mat(posMat)
    # #         rotMat = LMatrix4f.rotate_mat(45, LVector3f.up())
    # #         # 2a)
    # #         #rotPosMat = rotMat * posMat
    # #         #testMatTrans = TransformState.make_mat(rotPosMat)
    # #         # 2b)
    # #         rotMat *= posMat
    # #         testMatTrans = TransformState.make_mat(rotMat)
    # #         modelTest.set_transform(testMatTrans)
    #         # 3) translation -> rotation
    # #         modelRef.set_h(45)
    # #         modelRef.set_pos(LPoint3f(10, 0, 0))
    # #         posMat = LMatrix4f.translate_mat(LPoint3f(10, 0, 0))
    # #         posMatTrans = TransformState.make_mat(posMat)
    # #         rotMat = LMatrix4f.rotate_mat(45, LVector3f.up())
    # #         # 3a)
    # #         posRotMat = posMat * rotMat
    # #         testMatTrans = TransformState.make_mat(posRotMat)
    # #         # 3b)
    # #         #posMat *= rotMat
    # #         #testMatTrans = TransformState.make_mat(posMat)
    # #         modelTest.set_transform(testMatTrans)
    #
    #         # verify panda3d transform composition through TransformState
    #         # 1) translation
    # #         modelRef.set_pos(LPoint3f(10, 0, 0))
    # #         posTrans = TransformState.make_pos(LPoint3f(10, 0, 0))
    # #         # 1a
    # #         #modelTest.set_transform(posTrans)
    # #         # 1b
    # #         modelTest.node().set_transform(posTrans)
    #         # 2) rotation -> translation
    # #         modelRef.set_h(45)
    # #         modelRef.set_pos(LPoint3f(10, 0, 0))
    # #         posTrans = TransformState.make_pos(LPoint3f(10, 0, 0))
    # #         rotTrans = TransformState.make_hpr(LVecBase3f(45, 0, 0))
    # #         rotPosTrans = posTrans.compose(rotTrans)
    # #         modelTest.node().set_transform(rotPosTrans)
    #         # 3) translation -> rotation
    #         modelRef.set_h(45)
    #         modelRef.set_pos(LPoint3f(10, 0, 0))
    #         posTrans = TransformState.make_pos(LPoint3f(10, 0, 0))
    #         rotTrans = TransformState.make_hpr(LVecBase3f(45, 0, 0))
    #         posRotTrans = rotTrans.compose(posTrans)
    #         modelTest.node().set_transform(posRotTrans)
    #
    #         # setup camera
    #         app.disable_mouse()
    #         app.camera.set_z(50)
    #         app.camera.set_p(-90)

    #### TEST ####
    #     np = app.loader.load_model('smiley')
    #     print(np.get_transform(), np.node().get_transform(),
    #           np.get_net_transform(), sep=' 1 ')
    #     np1 = app.render.attach_new_node('np1')
    #     np1.set_x(15.0)
    #     np.reparent_to(np1)
    #     print(np.get_transform(), np.node().get_transform(),
    #           np.get_net_transform(), sep=' 2 ')
    #     np.set_x(15.0)
    #     print(np.get_transform(), np.node().get_transform(),
    #           np.get_net_transform(), sep=' 3 ')
    #     npAny = NodePath.any_path(np.node())
    #     print(npAny.get_transform(), npAny.node().get_transform(),
    #           npAny.get_net_transform(), sep=' 4 ')
    #     inst1 = app.render.attach_new_node('inst1')
    #     inst1.set_x(-20.0)
    #     np.instance_to(inst1)
    #     inst2 = app.render.attach_new_node('inst2')
    #     inst2.set_x(20.0)
    #     np.instance_to(inst2)
    #     print(np.get_transform(), np.node().get_transform(),
    #           np.get_net_transform(), sep=' 5 ')
    #     print(inst1.get_transform(), inst1.node().get_transform(),
    #           inst1.get_net_transform(), sep=' 6 ')
    #     print(inst2.get_transform(), inst2.node().get_transform(),
    #           inst2.get_net_transform(), sep=' 7 ')
    #     npAny = NodePath.any_path(np.node())
    #     print(npAny.get_transform(), npAny.node().get_transform(),
    #           npAny.get_net_transform(), sep=' 8 ')

    #### TEST ####
    #     win1 = app.open_window()
    #     cam1 = app.camList[1]

    #### TEST ####
    #     import numpy
    #     from matplotlib import pyplot
    #     x = numpy.linspace(-5, 5, 25)
    #     y = numpy.linspace(-5, 5, 25)
    #     z = x ** 2 + y ** 2
    #     z1 = 1 - (x ** 2 + y ** 2)
    #
    #     def plot_graph(x, y, z, axes, xlabel, ylabel, zlabel, title, elevation, rotation):
    #         axes.plot3D(x, y, z, label='concave')
    #         axes.plot(x, y, z1, label='convex')
    #         axes.view_init(elev=elevation, azim=rotation)
    #         axes.set_xlabel(xlabel)
    #         axes.set_ylabel(ylabel)
    #         axes.set_zlabel(zlabel)
    #         axes.set_title(title)
    #         axes.legend(loc='best')
    #
    #     fig = pyplot.figure(figsize=(8, 6))
    #     ax1 = fig.add_subplot(111, projection='3d')
    #     plot_graph(x, y, z, ax1, 'X', 'Y', 'Z', 'default view', None, None)
    #     pyplot.tight_layout(w_pad=250)
    #     pyplot.show()

    #### TEST ####
    #     map = app.win.get_keyboard_map()
    #     print(map)
    #     print(map.get_mapped_button('lshift'))
    #     print(str(map.get_mapped_button_label('lshift')))

    #### TEST ####
    #     def myFunc(keyname):
    #         print(keyname)
    #
    #     app.buttonThrowers[0].node().setKeystrokeEvent('keystroke')
    #     app.accept('keystroke', myFunc)

    #### TEST ####
    #     counter = [0]
    #     def background(counter, task):
    #         print(counter[0])
    #         counter[0] +=1
    #         return task.cont
    #
    #     app.task_mgr.add(background, 'background', extraArgs=[counter], sort=10, appendTask=True)

    #### TEST ####
    # def factors(n):
    #     k = 1
    #     while k * k < n:
    #         if n % k == 0:
    #             yield k
    #             yield n // k
    #         k += 1
    #         if k * k == n:
    #             yield k
    #
    # if __name__ == '__main__':
    #     for f in factors(100):
    #         print(f)

    #### TEST ####
    # from random import random, seed
    # from time import time
    # from math import asin, degrees, pow, sqrt
    # seed()
    #
    #
    # def nearestPointToEllipse(point, xAxis, yAxis, maxIterations=4, relError=0.01):
    #     '''
    #     Given a point finds (numerically) the nearest point to it on an ellipse.
    #
    #     See 'Least-squares orthogonal distances 'tting of circle, sphere, ellipse, hyperbola, and
    #         parabola' - Sung Joon Ahn, Wolfgang Rauh, Hans-Jurgen Warnecke - Pattern Recognition (2000)
    #
    #     Attributes:
    #         point(LPoint2f): the point whose nearest point on the ellipse should be found.
    #         xAxis,yAxis(float >=0): lengths of the ellipse's axes relative to x,y frame axis
    #             respectively.
    #         maxIterations(int): max number of iterations for the numerical algorithm (Newton's method).
    #         relError(float >=0): algorithm will stop if two consecutive iterations would update both the
    #             x,y coordinates less than this fraction.
    #     '''
    #
    #     xi = point.get_x()
    #     yi = point.get_y()
    #     # degenerate case: point in the center of a circle
    #     if (xi == 0.0) and (yi == 0.0) and (xAxis == yAxis):
    #         return LPoint2f.zero()
    #     a = xAxis
    #     b = yAxis
    #     #
    #     a2 = pow(a, 2)
    #     b2 = pow(b, 2)
    #     a2b2 = a2 * b2
    #     a2b2D = a2 - b2
    #     # initialize values
    #     abpiRInv = 1 / sqrt(b2 * pow(xi, 2) + a2 * pow(yi, 2))
    #     xk1 = xi * a * b * abpiRInv
    #     yk1 = yi * a * b * abpiRInv
    #     if abs(xi) < a:
    #         xk2 = xi
    #         if yi >= 0:
    #             yk2 = b / a * sqrt(a2 - pow(xi, 2))
    #         else:
    #             yk2 = -b / a * sqrt(a2 - pow(xi, 2))
    #     else:
    #         if xi >= 0:
    #             xk2 = a
    #         else:
    #             xk2 = -a
    #         yk2 = 0
    #     k = 0
    #     xk = 0.5 * (xk1 + xk2)  # x0
    #     yk = 0.5 * (yk1 + yk2)  # y0
    #     dxk = xk
    #     dyk = yk
    #     # start numerical iterations
    #     while (k < maxIterations) and ((abs(dxk) >= relError * xk) and (abs(dyk) >= relError * yk)):
    #         # k iteration
    #         # Q = (p q)
    #         #     (r s)
    #         # -f = (f1N)
    #         #      (f2N)
    #         p = b2 * xk
    #         q = a2 * yk
    #         r = a2b2D * yk + b2 * yi
    #         s = a2b2D * xk - a2 * xi
    #         f1N = -0.5 * (a2 * pow(yk, 2) + b2 * pow(xk, 2) - a2b2)
    #         f2N = -(b2 * xk * (yi - yk) - a2 * yk * (xi - xk))
    #         detInv = 1.0 / (p * s - q * r)
    #         # dxk, dyk
    #         dxk = detInv * (s * f1N - q * f2N)
    #         dyk = detInv * (-r * f1N + p * f2N)
    #         # update xk,yk,k
    #         xk += dxk
    #         yk += dyk
    #         k += 1
    #     #
    #     return LPoint2f(xk, yk)
    #
    # if __name__ == '__main__':
    #     app = ShowBase()
    #
    #     p0 = app.render.attach_new_node('p0')
    #     p0.set_pos(10.0)
    #     # joints
    #     j0 = p0.attach_new_node('j0')
    #     j0.set_pos_hpr(LPoint3f(1, 1, 1), LVecBase3f(30, 30, 30))
    #     j1 = j0.attach_new_node('j1')
    #     j1.set_pos_hpr(LPoint3f(-1, 1, -1), LVecBase3f(-30, 30, -30))
    #     j2 = j1.attach_new_node('j2')
    #     j2.set_pos_hpr(LPoint3f(1, -1, 1), LVecBase3f(30, -30, 30))
    #     #
    #     trW = p0.get_transform()
    #     #
    #     pos00 = j0.get_pos(app.render)
    #     trW = trW.compose(j0.get_transform())
    #     pos01 = trW.get_pos()
    #     print(pos00, pos01)
    #     #
    #     pos10 = j1.get_pos(app.render)
    #     trW = trW.compose(j1.get_transform())
    #     pos11 = trW.get_pos()
    #     print(pos10, pos11)
    #     #
    #     pos20 = j2.get_pos(app.render)
    #     trW = trW.compose(j2.get_transform())
    #     pos21 = trW.get_pos()
    #     print(pos20, pos21)
    #
    #     # setup camera
    #     trackball = app.trackball.node()
    #     trackball.set_pos(0.0, 55.0, -7.0)
    #     trackball.set_hpr(0.0, 0.0, 0.0)
    #     # run
    #     app.run()

    #### TEST ####
    #     n = NodePath('n')
    #     n.reparent_to(app.render)
    #     n.set_tag('a','0')
    #     n.set_tag('b','1')
    #     print(n.tags)
    #     print(dir(n.tags))
    #     print(type(n.tags))
    #     t = n.tags['a']
    # #     f = n.tags.pop('a')
    # #     e = n.tags.get('a')
    #     d = {'a':'0','b':'1'}
    #     print(dir(d))
    # #    for k,v in d.items():
    #     for k in d:
    #         print(d[k])
    #     print(type(n.nodes))
    #     print(len(n.get_nodes()), len(n.nodes))
    #     print(n.get_nodes()[0], n.nodes[0])
    #     print(n in n.get_nodes(), n in n.nodes)
    #     print([n] + [n.get_nodes()], [n] + [n.nodes])
    #     print(n.get_nodes().count(n), n.nodes.count(n))
    #     for j in n.nodes:
    #         print(j)

    #### TEST ####
    #     Jprev = NodePath('Jprev')
    #     J = NodePath('J')
    #     J1 = NodePath('J1')
    #     J2 = NodePath('J2')
    #     Jprev.reparent_to(app.render)
    #     J.reparent_to(Jprev)
    #     J1.reparent_to(J)
    #     J2.reparent_to(J1)
    #     # Jprev
    #     pos = LPoint3f(1.0, 2.0, -3.0)
    #     hpr = LVecBase3f(30, 60, 90)
    #     Jprev.set_transform(TransformState.make_pos_hpr(pos, hpr))
    #     # J
    #     pos = LPoint3f(-3.0, 1.0, 2.0)
    #     hpr = LVecBase3f(90, 30, 60)
    #     J.set_transform(TransformState.make_pos_hpr(pos, hpr))
    #     jWorldQuat = J.get_transform(app.render).get_quat()
    #     # J1
    #     pos = LPoint3f(2.0, -3.0, 1.0)
    #     hpr = LVecBase3f(60, 90, 30)
    #     J1.set_transform(TransformState.make_pos_hpr(pos, hpr))
    #     j1WorldQuat = J1.get_transform(app.render).get_quat()
    #     # J2
    #     pos = LPoint3f(-3.0, 2.0, 1.0)
    #     hpr = LVecBase3f(90, 60, 30)
    #     J2.set_transform(TransformState.make_pos_hpr(pos, hpr))
    #     j2WorldQuat = J2.get_transform(app.render).get_quat()
    #     # J
    #     dirWF = LVector3f(1.0, 1.0, 1.0).normalized()
    #     vecFW = invert(jWorldQuat).xform(dirWF)
    #     vecF = LVector3f(J.get_quat().xform(vecFW))
    #     #
    #     vecF1 = Jprev.get_relative_vector(app.render, (dirWF))
    #     #
    #     print(vecF, vecF1)
    #     # J1
    #     dirWF1 = LVector3f(1.0, -1.0, 1.0).normalized()
    #     vecFW1 = invert(j1WorldQuat).xform(dirWF1)
    #     vecF1 = LVector3f(J1.get_quat().xform(vecFW1))
    #     #
    #     vecF11 = J.get_relative_vector(app.render, (dirWF1))
    #     #
    #     print(vecF1, vecF11)
    #     # J2
    #     dirWF2 = LVector3f(-1.0, 1.0, -1.0).normalized()
    #     vecFW2 = invert(j2WorldQuat).xform(dirWF2)
    #     vecF2 = LVector3f(J2.get_quat().xform(vecFW2))
    #     #
    #     vecF12 = J1.get_relative_vector(app.render, (dirWF2))
    #     #
    #     print(vecF2, vecF12)

    #### TEST ####
    #     # order of transforms and quaternion product
    #     r = LVector3f.right()
    #     f = LVector3f.forward()
    #     R0 = LRotationf(r, 45)
    #     R1 = LRotationf(f, 90)
    #     T0 = TransformState.make_quat(R0)
    #     T1 = TransformState.make_quat(R1)
    #     Va = LVector3f(2,0,0)
    #     ### transform order Vb = T1(T0(Va)) = LVector3f(0,1.414213562,-1.414213562)
    #     #Vb1t = T1.compose(T0).get_mat().xform_vec(Va)
    #     Vb2t = T0.compose(T1).get_mat().xform_vec(Va)#OK
    #     ### quaternion product order Vb = (R1*R0)(Va) = LVector3f(0,1.414213562,-1.414213562)
    #     Vb1r = (R1 * R0).xform(Va)#OK
    #     #Vb2r = (R0 * R1).xform(Va)
    #     ### transform from quaternion product Vb = T(R1*R0)(Va) = LVector3f(0,1.414213562,-1.414213562)
    #     RP0 = R1 * R0
    #     T0R = TransformState.make_quat(RP0)
    #     RP1 = R0 * R1
    #     T1R = TransformState.make_quat(RP1)
    #     Vb1tr = T0R.get_mat().xform_vec(Va)#OK
    #     #Vb2tr = T1R.get_mat().xform_vec(Va)

    #### TEST ####
    #     p0 = app.render.attach_new_node('p0')
    #     p0.set_pos_hpr(LPoint3f(10, -13, 0.2), LVecBase3f(35, -232.8, 11.1))
    #     p1 = p0.attach_new_node('p1')
    #     p1.set_pos_hpr(LPoint3f(1, 1, 1), LVecBase3f(30, 60, 90))
    #     #
    #     tr0 = p0.get_transform(app.render)
    #     tr0Mat = tr0.get_mat()
    #     pos0 = p0.get_pos(app.render)
    #     pos0m = tr0Mat.xform_point(LPoint3f.zero())
    #     #
    #     tr1 = p1.get_transform(app.render)
    #     tr1Mat = tr1.get_mat()
    #     pos1 = p1.get_pos(app.render)
    #     pos1m = tr1Mat.xform_point(LPoint3f.zero())
    #     #
    #     vec0 = pos1 - pos0
    #     # trA components
    #     trT = TransformState.make_pos(-pos0)
    #     trTInv = TransformState.make_pos(pos0)
    #     axis = LVector3f(1, -1, 1).normalized()
    #     rot = LRotationf(axis, -35.0)
    #     trR = TransformState.make_quat(rot)
    #     trRMat = trR.get_mat()
    #     # trA
    #     trA = trTInv.compose(trR.compose(trT))
    #     trAMat = trA.get_mat()
    #     #
    #     tr0Comp = trA.compose(p0.get_transform(app.render))
    #     tr0CompMat = tr0Comp.get_mat()
    #     tr1Comp = trA.compose(p1.get_transform(app.render))
    #     tr1CompMat = tr1Comp.get_mat()
    #     #
    #     p0.set_transform(tr0Comp)
    #     pos01 = p0.get_pos(app.render)
    #     pos01i = tr0CompMat.xform_point(LPoint3f.zero())
    #     #
    #     pos11 = p1.get_pos(app.render)
    #     pos11i = tr1CompMat.xform_point(LPoint3f.zero())
    #     #
    #     vec1 = pos11 - pos01
    #     vec1i = trRMat.xform_vec(vec0)

    #### TEST ####
    #     point = LPoint2f(5.0, -2.0)
    #     xAxis = 0.0
    #     yAxis = 5.0
    #     #
    #     print (nearestPointToEllipse(point, xAxis, yAxis, maxIterations=4, relError=0.01))

    #### TEST ####
    # COUNT = 100000000
    # if __name__ == '__main__':
    #     vecIW = LVector3f((3.0) / (1.0), 5 / (2.0), 9 / (3.0))
    #     vecFW = LVector3f((1.0)/ (3.0), 5 / (2.0), 9 / (1.0))
    #
    #     startTime = time()
    #     for i in range(1, COUNT):
    # #         vecIW = LVector3f((i + 3.0) / (i + 1.0), i / (i + 2.0), i / (i + 3.0))
    # #         vecFW = LVector3f((i + 1.0)/ (i + 3.0), i / (i + 2.0), i / (i + 1.0))
    #         axisW = (vecIW).cross(vecFW)
    #         angleW = degrees(asin(axisW.length() / (vecIW.length() *
    #                                                 vecFW.length())))
    #         if vecIW.dot(vecFW) < 0:
    #             angleW = 180.0 - angleW
    # #         print(angleW)
    #     print(time() - startTime)
    #     startTime = time()
    #     for i in range(COUNT):
    # #         vecIW = LVector3f((i + 3.0) / (i + 1.0),
    # #                           i / (i + 2.0), i / (i + 3.0)).normalized()
    # #         vecFW = LVector3f((i + 1.0)/ (i + 3.0),
    # #                           i / (i + 2.0), i / (i + 1.0)).normalized()
    #         axisW = (vecIW).cross(vecFW).normalized()
    #         angleW = vecIW.angle_deg(vecFW)
    # #         print(angleW)
    #     print(time() - startTime)

    #### TEST ####
    ########## BinaryTree ##########
    # import Queue
    # class BinaryTree(object):
    #     '''Binary Tree Class and its methods'''
    #
    #     def __init__(self, data):
    #         '''Constructor'''
    #         self.data = data  # root node
    #         self.left = None  # left child
    #         self.right = None  # right child
    #
    #     def setData(self, data):
    #         '''Set data'''
    #         self.data = data
    #
    #     def getData(self):
    #         '''Get data'''
    #         return self.data
    #
    #     def getLeft(self):
    #         '''Get left child of a node'''
    #         return self.left
    #
    #     def getRight(self):
    #         '''Get right child of a node'''
    #         return self.right
    #
    #     def insertLeft(self, newNode):
    #         '''Inserts a new node as left child of this node.
    #
    #         An existing left child (and its sub-tree) would become the left child of the new node.'''
    #
    #         if self.left == None:
    #             self.left = newNode
    #         else:
    #             temp = newNode
    #             temp.left = self.left
    #             self.left = temp
    #
    #     def insertRight(self, newNode):
    #         '''Inserts a new node as right child of this node.
    #
    #         An existing right child (and its sub-tree) would become the right child of the new node.'''
    #
    #         if self.right == None:
    #             self.right = newNode
    #         else:
    #             temp = newNode
    #             temp.right = self.right
    #             self.right = temp
    #
    #     @staticmethod
    #     def insertNode(root, data):
    #         '''Inserts a new node into a binary tree using level order traversal.
    #
    #         Returns the new inserted node.'''
    #
    #         newNode = BinaryTree(data)
    #         if root is None:
    #             root = newNode
    #             return newNode
    #         q = Queue.Queue()
    #         q.put(root)
    #         node = None
    #         while not q.empty():
    #             node = q.get()  # dequeue FIFO
    #             if node.getLeft() is not None:
    #                 q.put(node.getLeft())
    #             else:
    #                 node.insertLeft(newNode)
    #                 return newNode
    #             if node.right is not None:
    #                 q.put(node.getRight())
    #             else:
    #                 node.insertRight(newNode)
    #                 return newNode
    #
    #     @staticmethod
    #     def preorderRecursive(root, callback=None):
    #         '''Pre-order recursive traversal.'''
    #
    #         if not root:
    #             return
    #         if callback: callback(root)
    #         BinaryTree.preorderRecursive(root.getLeft(), callback)
    #         BinaryTree.preorderRecursive(root.getRight(), callback)
    #
    #     @staticmethod
    #     def preorderIterative(root, callback=None):
    #         '''Pre-order iterative traversal.'''
    #
    #         if not root:
    #             return
    #         stack = []
    #         stack.append(root)
    #         while stack:
    #             node = stack.pop()
    #             if callback: callback(node)
    #             if node.getRight(): stack.append(node.getRight())
    #             if node.getLeft(): stack.append(node.getLeft())
    #
    #     @staticmethod
    #     def inorderRecursive(root, callback=None):
    #         '''In-order recursive traversal.'''
    #
    #         if not root:
    #             return
    #         BinaryTree.inorderRecursive(root.getLeft(), callback)
    #         if callback: callback(root)
    #         BinaryTree.inorderRecursive(root.getRight(), callback)
    #
    #     @staticmethod
    #     def inorderIterative(root, callback=None):
    #         '''In-order iterative traversal.'''
    #
    #         if not root:
    #             return
    #         stack = []
    #         node = root
    #         while stack or node:
    #             if node:
    #                 stack.append(node)
    #                 node = node.getLeft()
    #             else:
    #                 node = stack.pop()
    #                 if callback: callback(node)
    #                 node = node.getRight()
    #
    #     @staticmethod
    #     def postorderRecursive(root, callback=None):
    #         '''Post-order recursive traversal.'''
    #
    #         if not root:
    #             return
    #         BinaryTree.postorderRecursive(root.getLeft(), callback)
    #         BinaryTree.postorderRecursive(root.getRight(), callback)
    #         if callback: callback(root)
    #
    #     @staticmethod
    #     def postorderIterative(root, callback=None):
    #         '''Post-order iterative traversal.'''
    #
    #         if not root:
    #             return
    #         visited = set()
    #         stack = []
    #         node = root
    #         while stack or node:
    #             if node:
    #                 stack.append(node)
    #                 node = node.getLeft()
    #             else:
    #                 node = stack.pop()
    #                 if node.getRight() and not (node.getRight() in visited):
    #                     stack.append(node)
    #                     node = node.getRight()
    #                 else:
    #                     visited.add(node)
    #                     if callback: callback(node)
    #                     node = None
    #
    #     @staticmethod
    #     def levelOrder(root, callback=None):
    #         '''Level-order traversal.
    #
    #         Inspired from Breadth First Traversal (BPS of Graph algorithms).'''
    #
    #         if root is None:
    #             return
    #         q = Queue.Queue()
    #         q.put(root)
    #         node = None
    #         while not q.empty():
    #             node = q.get()  # dequeue FIFO
    #             if callback: callback(node)
    #             if node.getLeft() is not None:
    #                 q.put(node.getLeft())
    #             if node.getRight() is not None:
    #                 q.put(node.getRight())
    #
    #     @staticmethod
    #     def findMaxRecursive(root, maxData=float('-infinity')):
    #         '''Maximum node's data find recursive algorithm.'''
    #
    #         return BinaryTree._findMinMaxRecursive(root, maxData, 'max')
    #
    #     @staticmethod
    #     def findMinRecursive(root, minData=float('infinity')):
    #         '''Minimum node's data find recursive algorithm.'''
    #
    #         return BinaryTree._findMinMaxRecursive(root, minData, 'min')
    #
    #     @staticmethod
    #     def _findMinMaxRecursive(root, data, op):
    #         '''Helper.'''
    #
    #         if not root:
    #             return data
    #         if op == 'max':
    #             if root.getData() > data:
    #                 data = root.getData()
    #         elif op == 'min':
    #             if root.getData() < data:
    #                 data = root.getData()
    #         data = BinaryTree._findMinMaxRecursive(root.getLeft(), data, op)
    #         data = BinaryTree._findMinMaxRecursive(root.getRight(), data, op)
    #         return data
    #
    #     @staticmethod
    #     def findMaxIterative(root, maxData=float('-infinity')):
    #         '''Maximum node's data find iterative (level order traversal) algorithm.'''
    #
    #         return BinaryTree._findMinMaxIterative(root, maxData, 'max')
    #
    #     @staticmethod
    #     def findMinIterative(root, minData=float('infinity')):
    #         '''Minimum node's data find iterative (level order traversal) algorithm.'''
    #
    #         return BinaryTree._findMinMaxIterative(root, minData, 'min')
    #
    #     @staticmethod
    #     def _findMinMaxIterative(root, data, op):
    #         '''Helper.'''
    #
    #         if not root:
    #             return data
    #         q = Queue.Queue()
    #         q.put(root)
    #         node = None
    #         element = data
    #         while not q.empty():
    #             node = q.get()  # dequeue FIFO
    #             if op == 'max':
    #                 if node.getData() > element:
    #                     element = node.getData()
    #             elif op == 'min':
    #                 if node.getData() < element:
    #                     element = node.getData()
    #             if node.left is not None:
    #                 q.put(node.getLeft())
    #             if node.right is not None:
    #                 q.put(node.getRight())
    #         return element
    #
    #     @staticmethod
    #     def findRecursive(root, data):
    #         '''Node's data search recursive algorithm.
    #
    #          Returns True if a node with data is found in the tree, otherwise False.'''
    #
    #         if not root:
    #             return False
    #         if root.getData() == data:
    #             return True
    #         else:
    #             temp= BinaryTree.findRecursive(root.getLeft(), data)
    #             if temp == True:
    #                 return temp
    #             else:
    #                 return BinaryTree.findRecursive(root.getRight(), data)
    #
    #     @staticmethod
    #     def findIterative(root, data):
    #         '''Node's data search iterative (level order traversal) algorithm.
    #
    #          Returns True if a node with data is found in the tree, otherwise False.'''
    #
    #         if not root:
    #             return False
    #         q = Queue.Queue()
    #         q.put(root)
    #         node = None
    #         while not q.empty():
    #             node = q.get()  # dequeue FIFO
    #             if data == node.getData():
    #                 return True
    #             if node.getLeft() is not None:
    #                 q.put(node.getLeft())
    #             if node.getRight() is not None:
    #                 q.put(node.getRight())
    #         return False
    #
    #
    # from random import random
    # if __name__ == '__main__':
    #     #BinaryTree
    #     def callback(node):
    #         print(node.getData())
    #     # traversals
    #     root = BinaryTree(1)
    #     for i in range(2, pow(2,3)):
    #         BinaryTree.insertNode(root, i)
    #     BinaryTree.preorderRecursive(root, callback)
    #     print('--')
    #     BinaryTree.preorderIterative(root, callback)
    #     print('--')
    #     BinaryTree.inorderRecursive(root, callback)
    #     print('--')
    #     BinaryTree.inorderIterative(root, callback)
    #     print('--')
    #     BinaryTree.postorderRecursive(root, callback)
    #     print('--')
    #     BinaryTree.postorderIterative(root, callback)
    #     print('--')
    #     BinaryTree.levelOrder(root, callback)
    #     print('--')
    #     # searches
    #     root = BinaryTree(int(random() * 100.0))
    #     for i in range(2, pow(2,4)):
    #         data = int(random() * 100.0)
    #         BinaryTree.insertNode(root, data)
    #     BinaryTree.levelOrder(root, callback)
    #     print('--')
    #     print(BinaryTree.findMaxRecursive(root))
    #     print(BinaryTree.findMinRecursive(root))
    #     print('--')
    #     print(BinaryTree.findMaxIterative(root))
    #     print(BinaryTree.findMinIterative(root))
    #     print('--')
    #     print(BinaryTree.findRecursive(root, 91))
    #     print('--')
    #     print(BinaryTree.findIterative(root, 91))
    #     print('--')

except Exception as e:
    print('This script is for generic tests only!', e)
    exit(-1)
