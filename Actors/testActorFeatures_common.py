'''
Created on Feb 26, 2018

Tests about Actor augmented with Ely's features.

@author: consultit
'''

from panda3d.core import NodePath, ClockObject, TextureStage, \
    GeomVertexArrayFormat, InternalName, Geom, GeomVertexFormat, \
    GeomVertexData, GeomVertexWriter, LPoint3f, GeomTriangles, GeomNode,\
    DirectionalLight, LVecBase4f, PointLight
from ely.physics import GamePhysicsManager, BT3Ghost, BT3RigidBody
from ely.control import GameControlManager
from ely.samples.physics.common import toggleDebugDraw
from ely.samples.direct.common import cycleAnims, toggleVisibility, \
    toggleWireframe, setupRagdoll, resetRagdoll
import xml.etree.ElementTree as ET


def setRagdollTest(app, actor, setupKey, resetKey, cycleAnimKey, animIdx,
                   visibleKey, wireKey):
    '''setRagdollTest'''

    app.accept(setupKey, setupRagdoll, [actor, app])
    app.accept(resetKey, resetRagdoll, [actor, app])
    # cycle among actor's animations
    animList = actor.get_anim_names()
    app.accept(cycleAnimKey, cycleAnims, [actor, animList, animIdx])
    # toggle actor's visibility
    app.accept(visibleKey, toggleVisibility, [actor])
    # toggle actor's wire frame mode on/off
    app.accept(wireKey, toggleWireframe, [actor])


# FSM factory
outPrologueDebug = '''import sys, os
'''
outPrologue0 = '''from direct.fsm.FSM import FSM
    
class '''
outPrologue1 = '''(FSM, object):
    \'\'\'Actor's FSM\'\'\'
    
    def __init__(self, actor):
        \'\'\'Constructor.\'\'\'
        
        super('''
outPrologue2 = ', self).__init__(\''
outPrologue3 = '''\')
        self.actor = actor
'''


def createFSM(xmlfile, fsmFile='actorFSM_tmpl.py', className='ActorFSMTmpl',
              defaultTrans=False, inputEvents=True, debug=False):
    '''
    Creates a python module containing a declaration of a class derived from
    FSM, based on an xml specification.

    To create the actor's FSM template from xml specification, execute the
    following commands:

        actorFSMFile = createFSM('actorFSM_tmpl.xml',
                                fsmFile='actorFSM_tmpl.py', 
                                className='ActorFSMTmpl', defaultTrans=True, 
                                inputEvents=True, debug=True)

    To test actor's FSM template, execute the following commands:

        from direct.actor.Actor import Actor
        exec(open(actorFSMFile).read(), globals()) # or: from actorFSM_tmpl
                                                   # import ActorFSMTmpl
        actorFSMTmpl = ActorFSMTmpl(Actor())
        actorFSMTmpl.request('I')
        actorFSMTmpl.setupDebug(app, 't')

    where 'I' is the initial state, app is the application (ShowBase) and where
    't' key cycles among the inputs, and 'shift-'t key cycles among the states.  
    '''

    # create the output file
    outFile = open(str(fsmFile), 'w')
    # write prefixes
    if debug:
        outFile.write(outPrologueDebug)
    outFile.write(outPrologue0)
    outFile.write(str(className))
    outFile.write(outPrologue1)
    outFile.write(str(className))
    outFile.write(outPrologue2)
    outFile.write(str(className))
    outFile.write(outPrologue3)
    # read the xml file
    fsmTree = ET.parse(xmlfile)
    fsmRoot = fsmTree.getroot()
    # create the auxiliary transition dictionary
    transitionDict = {}
    for transition in fsmRoot.iter('transition'):
        state_from = transition.get('state_from')
        fsmInput = transition.get('input')
        state_to = transition.get('state_to')
        # update transitionDict
        if not (state_from in transitionDict):
            transitionDict[state_from] = []
        transitionDict[state_from].append((fsmInput, state_to))
    # create default transitions if requested
    if defaultTrans:
        # print prologue
        outFile.write('''
        # set default transitions
        self.defaultTransitions = {
            ''')
        # for each state do
        for state in transitionDict:
            outFile.write('\'' + state + '\' : [')
            for iS in transitionDict[state]:
                stateTo = iS[1]
                outFile.write('\'' + stateTo + '\' ,')
            outFile.write('''],
            ''')
        outFile.write('''}
        ''')
    # for each state create enter/exit/filter methods
    # for each state do
    for state in transitionDict:
        outFile.write('''
    # ''' + state)
        # enter
        enterName = 'enter' + state
        outFile.write('''
    def ''' + enterName + '''(self):
        \'\'\'''' + enterName + '''\'\'\'
        
        print(\'''' + enterName + '''\')
    ''')
        # exit
        exitName = 'exit' + state
        outFile.write('''
    def ''' + exitName + '''(self):
        \'\'\'''' + exitName + '''\'\'\'
        
        print(\'''' + exitName + '''\')
    ''')
        # filter
        filterName = 'filter' + state
        outFile.write('''
    def ''' + filterName + '''(self, request, args):
        \'\'\'''' + filterName + '''\'\'\'
         
        print(\'''' + filterName + '''\')
        ''')
        if not transitionDict[state]:
            outFile.write('''return None
    ''')
        else:
            ELIF = 'if '
            for fsmInput, state_to in transitionDict[state]:
                outFile.write(ELIF + '''request == \'''' + fsmInput + '''\':
            return \'''' + state_to + '''\'
        ''')
                ELIF = 'elif '
            outFile.write('''else:
            return None
        ''')
    # setup input events
    if inputEvents:
        # create input events stuff
        outFile.write('''
    # input events
    def setupInputEvents(self, app, eventInputMap):
        \'\'\'setupInputEvents\'\'\'
    
        self.eventInputMap = eventInputMap
        for ev in eventInputMap:
            app.accept(ev, self.inputEventsClbk, [ev])
        
    
    def inputEventsClbk(self, ev):
        \'\'\'inputEventsClbk\'\'\'
        
        self.request(self.eventInputMap[ev])
        ''')
    # debug FSM
    if debug:
        # create debug stuff
        outFile.write('''
    # debug
    def setupDebug(self, app, keyEv):
        \'\'\'setupDebug\'\'\'
        
        self.inputKey = keyEv
        self.stateKey = \'shift-'+ keyEv
        self.inputList = [''')
        for fsmInput in fsmRoot.iter('input'):
            outFile.write('\'' + fsmInput.get('name') + '\',')
        outFile.write(''']
        self.stateList = [''')
        for fsmState in fsmRoot.iter('state'):
            outFile.write('\'' + fsmState.get('name') + '\',')
        outFile.write(''']
        self.currInput = 0
        self.currState = self.stateList.index(self.state)
        app.accept(self.inputKey, self.cycleDebug, [self.inputKey])
        app.accept(self.stateKey, self.cycleDebug, [self.stateKey])
        
    def cycleDebug(self, what):
        \'\'\'cycleDebug\'\'\'
        
        if what == self.inputKey:
            fsmInput = self.inputList[self.currInput]
            print('input: \\'' + fsmInput + '\\'')
            self.request(self.inputList[self.currInput])
            sys.stdout = open(os.devnull, 'w')
            self.forceTransition(self.stateList[self.currState])
            sys.stdout = sys.__stdout__
            print('\\n')
            self.currInput += 1
            if self.currInput >= len(self.inputList):
                self.currInput = 0
        elif what == self.stateKey:
            self.currState += 1
            if self.currState >= len(self.stateList):
                self.currState = 0
            print('\\n')
            self.forceTransition(self.stateList[self.currState])
            self.currInput = 0
    ''')

    # return outFile
    outFile.close()
    return fsmFile


def createCharacterController(model, physicsMgr,
                              shape=GamePhysicsManager.CAPSULE):
    '''createCharacterController'''

    # reset default parameters
    physicsMgr.set_parameters_defaults(GamePhysicsManager.GHOST)
    # create character's ghost (attached to the reference node)
    characterNP = physicsMgr.create_ghost(model.get_name() + "_CharGhost")
    # get a reference to the player's rigid_body
    characterGhost = characterNP.node()
    # set the ghost's shape
    characterGhost.shape_type = shape
    # set owner object and setup
    characterGhost.set_owner_object(model)
    characterGhost.setup()
    # create the character controller
    physicsMgr.set_parameters_defaults(GamePhysicsManager.CHARACTERCONTROLLER)
    characterController = physicsMgr.create_character_controller(
        model.get_name() + "_CharCtrl").node()
    # set the character controller's character (i.e. owner object)
    characterController.set_owner_object(characterNP)
    # setup the character controller
    characterController.setup()
    #
    return characterController

# find out speeds of the movement animations


def createBoxNP(name, dims):
    '''
    Creates a (width * depth * height) box shaped GeomNode.

    The box's vertices have normals and texture's coordinates.
                          z
           6-----7         ^   y
          /|    /|         |  ^
         / |   / | h       | /
        4-----5  |         |/
        |  2--|--3         o-----> x
        | /   | /
        |/    |/ d
        0-----1
           w
    '''

    width, depth, height = dims
    # create a box: width x depth x height
    # Vertex Format
    arrayFormat = GeomVertexArrayFormat()
    arrayFormat.add_column(InternalName.make('vertex'),
                           3, Geom.NT_float32, Geom.C_point)
    arrayFormat.add_column(InternalName.make('normal'),
                           3, Geom.NT_float32, Geom.C_point)
    arrayFormat.add_column(InternalName.make("texcoord"),
                           2, Geom.NT_float32, Geom.C_texcoord)
    vertexFormat = GeomVertexFormat()
    vertexFormat.add_array(arrayFormat)
    vertexFormatAdded = GeomVertexFormat.register_format(vertexFormat)
    # Vertex Data
    vertexData = GeomVertexData(
        'vertex_texcoord', vertexFormatAdded, Geom.UH_static)
    vertexData.set_num_rows(2)
    vertexW = GeomVertexWriter(vertexData, 'vertex')
    normalW = GeomVertexWriter(vertexData, 'normal')
    texcoordW = GeomVertexWriter(vertexData, 'texcoord')
    # compute coords according to z-up axis
    vertices = [None] * 8
    w2 = width / 2.0
    d2 = depth / 2.0
    h2 = height / 2.0
    for h in range(2):
        signH = -1 if (h % 2) == 0 else 1
        for d in range(2):
            signD = -1 if (d % 2) == 0 else 1
            for w in range(2):
                signW = -1 if (w % 2) == 0 else 1
                vertices[w + 2 * d + 4 * h] = LPoint3f(
                    signW * w2, signD * d2, signH * h2)
    # fill-up vertex data by face; every face has 4 vertices (defined in CCW
    # order)
    faces = ((0, 1, 5, 4), (1, 3, 7, 5), (3, 2, 6, 7),
             (2, 0, 4, 6), (2, 3, 1, 0), (4, 5, 7, 6))
    normals = ((0.0, -1.0, 0.0), (1.0, 0.0, 0.0), (0.0, 1.0, 0.0),
               (-1.0, 0.0, 0.0), (0.0, 0.0, -1.0), (0.0, 0.0, 1.0))
    uv = ((0.0, 0.0), (1.0, 0.0), (1.0, 1.0), (0.0, 1.0))
    for f in range(len(faces)):
        t = 0
        for i in faces[f]:
            vertexW.add_data3f(vertices[i])
            normalW.add_data3f(*normals[f])
            texcoordW.addData2f(*uv[t])
            t += 1
    # Creating the GeomPrimitive objects for box
    boxTriangles = GeomTriangles(Geom.UH_static)
    # add triangles
    for f in range(len(faces)):
        d = 4 * f
        boxTriangles.add_vertices(d + 0, d + 1, d + 3)
        boxTriangles.add_vertices(d + 2, d + 3, d + 1)
    # Putting your new geometry in the scene graph
    boxGeom = Geom(vertexData)
    boxGeom.add_primitive(boxTriangles)
    boxNode = GeomNode(name + 'PH')
    boxNode.add_geom(boxGeom)
    #
    return NodePath(boxNode)


def findOutSpeedsForMovementAnimation(app, physicsMgr, actor, planeNP,
                                      jointName, animName,
                                      playRate=1.0, ghostDims=(0.1, 0.1, 1.0),
                                      planeHeight=0.0, planeLiftRes=0.05,
                                      carpetRay=50.0, carpetHeight=0.5,
                                      carpetTex=None, carpetTexScale=1.0):
    '''Find out speeds for the movement (walk, run etc...) animations

    This function executes the given animation and prints out the speed and
    average speed at which the given joint (foot) travels the ground in one
    cycle. These speeds are computed by measuring the elapsed time and the
    length of the path made by a ghost, attached to the joint, when it is in
    contact with the ground. To detect the contact events a 'carpet' dynamic
    rigid body is used and it is located over a 'plane' static rigid body.
    Use 'arrow_up'/'arrow_down' keys to change the plane's (and carpet's)
    position height. Use 'p' key to restart the speeds' measurements.

    Attributes:
        app(ShowBase): the application object.
        physicsMgr(GamePhysicsManager): the physics manager.
        actor(ActorRagDoll): the actor object.
        planeNP(NodePath): the plane static rigid body.
        jointName(string): the name of the joint (usually a foot joint).
        animName(string): the name of the movement animation.
        playRate(float): the animation's play rate.
        ghostDims(3-tuple): the dimensions of the ghost attached to the joint.
        planeHeight,planeLiftRes(float): respectively the plane's starting
            position height and the value of the incremental steps used when
            changing the plane's position height by hitting the arrow keys.
        carpetRay(float),carpetHeight(float): the ray and height of the carpet
            rigid body, respectively.
        carpetTexture(string),carpetTexScale(float): the texture and texture
            scale of the carpet rigid body, respectively.
    '''

    def attachGhost(physicsMgr, actor, joint, dims):
        '''attach a ghost to an actor's joint'''

        ghostOwner = createBoxNP('ghostOwner', dims)
        ghost = physicsMgr.create_ghost('ghost').node()
        ghost.set_owner_object(ghostOwner)
        ghost.set_shape_type(GamePhysicsManager.CYLINDER)
        ghost.set_visible(False)
        ghost.setup()
        ghost.enable_throw_event(BT3Ghost.OVERLAP, True, 10e-6, 'Overlap')
        # 2: attach the ghost to a 'feet' joint
        actor.expose_joint(NodePath.any_path(ghost), 'modelRoot', joint)
        return ghost

    # find out speeds of the movement animations
    ghost = attachGhost(physicsMgr, actor, jointName, ghostDims)
    pos = [NodePath.any_path(ghost).get_pos()]
    startTime = [ClockObject.get_global_clock().get_real_time()]
    averageSpeed = [0.0, 0]

    def resetValues(pos, startTime, averageSpeed):
        pos[0] = NodePath.any_path(ghost).get_pos()
        startTime[0] = ClockObject.get_global_clock().get_real_time()
        averageSpeed[0], averageSpeed[1] = 0.0, 0

    app.accept('p', resetValues, [pos, startTime, averageSpeed])

    def genericEventNotify(ev, startTime, pos, averageSpeed, object0, object1):
        '''generic event notify'''

        #t0 = time()
        t0 = ClockObject.get_global_clock().get_real_time()
#             print ('got \'' + ev + '\' between \'' + object0.get_name() +
#                    '\' and \'' + object1.get_name() + '\'')
#             print (NodePath.any_path(object1).get_pos())
        if ev == 'Overlap':
            pos[0] = NodePath.any_path(object1).get_pos()
            startTime[0] = t0
        elif ev == 'OverlapOff':
            elapsedTime = t0 - startTime[0]
            dist = (NodePath.any_path(object1).get_pos() - pos[0]).length()
            speed = dist / elapsedTime * 10.0
            averageSpeed[0] += speed
            averageSpeed[1] += 1.0
            print('speed :' + str(speed))
            print('average speed:' + str(averageSpeed[0] / averageSpeed[1]))
            print('\n')

    app.accept('Overlap', genericEventNotify, [
               'Overlap', startTime, pos, averageSpeed])
    app.accept('OverlapOff', genericEventNotify, [
               'OverlapOff', startTime, pos, averageSpeed])

    def liftPlane(ev, model, res):
        if ev == 'arrow_up':
            model.set_z(model.get_z() + res)
        elif ev == 'arrow_down':
            model.set_z(model.get_z() - res)
        print('plane\'s location height:' + str(model.get_z()))

    planeNP.node().switch_body_type(BT3RigidBody.KINEMATIC)
    planeNP.set_z(planeHeight)
    app.accept('arrow_up', liftPlane, ['arrow_up', planeNP, planeLiftRes])
    app.accept('arrow_down', liftPlane, ['arrow_down', planeNP, planeLiftRes])

    carpetOwner = createBoxNP(
        'carpetOwner', (carpetRay, carpetRay, carpetHeight))
    carpetOwner.set_z(planeHeight + 0.5)
    carpet = physicsMgr.create_rigid_body('carpet').node()
    carpet.set_shape_type(GamePhysicsManager.BOX)
    carpet.set_mass(800.0)
    carpet.set_owner_object(carpetOwner)
    carpet.setup()
    if carpetTex:
        tex = app.loader.load_texture(carpetTex)
        carpetOwner.set_texture(tex)
        carpetOwner.set_tex_scale(
            TextureStage.default, carpetTexScale, carpetTexScale)
    else:
        carpetOwner.set_color(0.0, 0.5, 0.7, 1.0)
    # animate actor
    actor.set_play_rate(playRate, animName)
    actor.loop(animName)
    # add lighting
#     refNP = physicsMgr.get_reference_node_path()
#     dlight = DirectionalLight('dlight')
#     dlight.set_color(LVecBase4f(0.8, 0.8, 0.5, 1))
#     dlnp = refNP.attachNewNode(dlight)
#     dlnp.set_hpr(0, -60, 0)
#     refNP.set_light(dlnp)
#     plight = PointLight('plight')
#     plight.set_color(LVecBase4f(0.8, 0.9, 1.0, 1))
#     plnp = refNP.attachNewNode(plight)
#     plnp.set_pos(0, 0, 100)
#     refNP.set_light(plnp)
