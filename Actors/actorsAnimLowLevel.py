'''
Created on Apr 17, 2020

@author: consultit
'''

from panda3d.core import load_prc_file_data, LVector3f, WindowProperties,\
    Filename, NodePath, AnimControlCollection, LPoint3f
from actorsAnim_common import getDimensions, leftToRightBreadthFirstTraversal, \
    leftToRightDepthFirstTraversal, buildTree, auto_bind, retrievePartGroups
from actorsAnim_data import actorData
from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor
import argparse
import textwrap
import random

random.seed()


def main(argv):
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will test Actor animations
      
    Commands:
        - 'arrow' keys: to move character in 'driver'  mode.
    '''))
    # set up arguments
    parser.add_argument('model', type=str,
                        choices=[k for k in actorData.keys()],
                        help='the actor\'s model')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args(argv)

    # do actions
    # set model dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
    #load_prc_file_data('', 'window-type none')
    load_prc_file_data('', 'win-size 1024 768')
    # load_prc_file_data('', 'want-directtools #t')
    # load_prc_file_data('', 'want-tk #t')

    # get an actor
    app = ShowBase()
    actor = Actor(actorData[args.model]['fileName'],
                  {k: v[0] for k, v in actorData[args.model]['anims'].items()})

    actor.reparent_to(app.render)
    print(actor.get_actor_info())

    # actor's scene graph
    def getInfo(n, l):
        sep = ' ' * l
        print(sep, str(n).rstrip('\n'), n.get_type().get_name().rstrip('\n'),
              str(n.get_parent(0)).rstrip('\n'), sep=' | ')

    # left-to-right breadth-first tree traversal
#     leftToRightBreadthFirstTraversal(actor.node(), action=getInfo)

    # left-to-right depth-first tree traversal
    leftToRightDepthFirstTraversal(actor.node(), action=getInfo, preorder=True)
#     leftToRightDepthFirstTraversal(
#         actor.node(), action=getInfo, preorder=False)
    actor.ls()
    app.render.ls()

    # retrieve PartGroup(s) below actor
    retrievePartGroups(actor)

    # Alternative: retrieve PartGroup(s) using a model (and not an actor)
#     model = app.loader.loadModel(Filename(args.model))
#     # model's scene graph
#     def getInfo(n, l):
#         sep = ' ' * l
#         print(sep, str(n).rstrip('\n'), n.get_type().get_name().rstrip('\n'),
#               str(n.get_parent(0)).rstrip('\n') if n.get_parents() else 'NONE',
#               sep=' | ')
#
#     leftToRightDepthFirstTraversal(model.node(), action=getInfo, preorder=True)
#     # retrieve PartGroups below model
#     partGroups = retrievePartGroups(model, verbose=False)
#     print(partGroups)

    # test animation binding
    animModelBase = NodePath('animModelBase')
    modelFile = actorData[args.model]['fileName']
    animFiles = [value[0] for value in actorData[args.model]['anims'].values()]

    model = app.loader.load_model(Filename(modelFile))
    model.reparent_to(animModelBase)
    anims = [app.loader.load_model(Filename(animFile))
             for animFile in animFiles]
    for anim in anims:
        anim.reparent_to(animModelBase)
    # get infos
    model.ls()
    for anim in anims:
        anim.ls()
    # bind animations
    controls = AnimControlCollection()
    auto_bind(animModelBase.node(), controls, hierarchy_match_flags=0)
    # print animations
    boundAnims = []
    for i in range(controls.get_num_anims()):
        boundAnims.append(controls.get_anim_name(i))
    print(boundAnims)
    # play an animation
    model.reparent_to(app.render)
    model.set_pos(LPoint3f(10.0, 0, 0))
    animToPlay = boundAnims[random.randint(0, len(anims) - 1)]
    controls.find_anim(animToPlay).loop(True)

    # default title
    winTitle = ('Actor Anim low level API: ' + args.model)

    # default camera positioning
    # get 'tight' dimensions of actor
    _, xC, yC, zC, xL, yL, zL = getDimensions(actor)
    #
    app.trackball.node().set_origin(LVector3f(xL, yL, zL))
    app.trackball.node().set_pos(-xC + xL, -yC + yL, -zC + zL)
    app.trackball.node().set_hpr(0, 0, 0)

    # set window title
    props = WindowProperties()
    props.setTitle(winTitle)
    if app.win:
        app.win.requestProperties(props)
        # run
        app.run()


if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
