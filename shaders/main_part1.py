'''
Created on Jan 19, 2020

@author: consultit
'''

from basePandaApp import BasePandaApp
from panda3d.core import Shader, LVecBase3f, TextureStage, SamplerState


class Part1App(BasePandaApp):
    '''
    Part1App
    '''
    
    def __init__(self):
        '''
        Constructor
        '''

        super().__init__()

    def m06_panda3d_steep_parallax_maps(self):
        '''Steep parallax maps the panda3d way.
        
        Running 'egg-trans -tbnall' on a panda3d's egg model, will compute
        tangent and binormal for all texture coordinate sets.
        '''
        
        i = str(self.repeatCounter % self.terrainTexNum)
        self._normal_parallax_maps(
            vertex='part1/s06-panda3d-steep-parallax-maps-vert.glsl',
            fragment='part1/s06-steep-parallax-maps-frag.glsl',
            terrainDiffuseTexture='terrain' + i + '-diffusemap.png',
            terrainNormalTexture='terrain' + i + '-normalmap.png',
            terrainHeightTexture='terrain' + i + '-heightmap.png',
            modelDiffuseTexture='ogre-diffusemap.png',
            modelNormalTexture='ogre-normalmap.png',
            modelHeightTexture='ogre-heightmap.png',
            numLights=1, L=1.0, La=0.2,
            Ks=LVecBase3f(0.7, 0.7, 0.7), Shininess=40.0)

    def m06_steep_parallax_maps(self):
        i = str(self.repeatCounter % self.terrainTexNum)
        self._normal_parallax_maps(
            vertex='part1/s06-steep-parallax-maps-vert.glsl',
            fragment='part1/s06-steep-parallax-maps-frag.glsl',
            terrainDiffuseTexture='terrain' + i + '-diffusemap.png',
            terrainNormalTexture='terrain' + i + '-normalmap.png',
            terrainHeightTexture='terrain' + i + '-heightmap.png',
            modelDiffuseTexture='ogre-diffusemap.png',
            modelNormalTexture='ogre-normalmap.png',
            modelHeightTexture='ogre-heightmap.png',
            numLights=1, L=1.0, La=0.2,
            Ks=LVecBase3f(0.7, 0.7, 0.7), Shininess=40.0)

    def m05_panda3d_parallax_maps(self):
        '''Parallax maps the panda3d way.
        
        Running 'egg-trans -tbnall' on a panda3d's egg model, will compute
        tangent and binormal for all texture coordinate sets.
        '''
        
        i = str(self.repeatCounter % self.terrainTexNum)
        self._normal_parallax_maps(
            vertex='part1/s05-panda3d-parallax-maps-vert.glsl',
            fragment='part1/s05-parallax-maps-frag.glsl',
            terrainDiffuseTexture='terrain' + i + '-diffusemap.png',
            terrainNormalTexture='terrain' + i + '-normalmap.png',
            terrainHeightTexture='terrain' + i + '-heightmap.png',
            modelDiffuseTexture='ogre-diffusemap.png',
            modelNormalTexture='ogre-normalmap.png',
            modelHeightTexture='ogre-heightmap.png',
            numLights=3, L=1.0, La=0.2,
            Ks=LVecBase3f(0.7, 0.7, 0.7), Shininess=40.0)

    def m05_parallax_maps(self):
        i = str(self.repeatCounter % self.terrainTexNum)
        self._normal_parallax_maps(
            vertex='part1/s05-parallax-maps-vert.glsl',
            fragment='part1/s05-parallax-maps-frag.glsl',
            terrainDiffuseTexture='terrain' + i + '-diffusemap.png',
            terrainNormalTexture='terrain' + i + '-normalmap.png',
            terrainHeightTexture='terrain' + i + '-heightmap.png',
            modelDiffuseTexture='ogre-diffusemap.png',
            modelNormalTexture='ogre-normalmap.png',
            modelHeightTexture='ogre-heightmap.png',
            numLights=3, L=1.0, La=0.2,
            Ks=LVecBase3f(0.7, 0.7, 0.7), Shininess=40.0)

    def m04_panda3d_normal_maps(self):
        '''Normal maps the panda3d way.
        
        Running 'egg-trans -tbnall' on a panda3d's egg model, will compute
        tangent and binormal for all texture coordinate sets.
        '''
        
        i = str(self.repeatCounter % self.terrainTexNum)
        self._normal_parallax_maps(
            vertex='part1/s04-panda3d-normal-maps-vert.glsl',
            fragment='part1/s04-normal-maps-frag.glsl',
            terrainDiffuseTexture='terrain' + i + '-diffusemap.png',
            terrainNormalTexture='terrain' + i + '-normalmap.png',
            modelDiffuseTexture='ogre-diffusemap.png',
            modelNormalTexture='ogre-normalmap.png',
            numLights=2, L=1.0, La=0.2,
            Ks=LVecBase3f(0.2, 0.2, 0.2), Shininess=1.0)

    def m04_normal_maps(self):
        i = str(self.repeatCounter % self.terrainTexNum)
        self._normal_parallax_maps(
            vertex='part1/s04-normal-maps-vert.glsl',
            fragment='part1/s04-normal-maps-frag.glsl',
            terrainDiffuseTexture='terrain' + i + '-diffusemap.png',
            terrainNormalTexture='terrain' + i + '-normalmap.png',
            modelDiffuseTexture='ogre-diffusemap.png',
            modelNormalTexture='ogre-normalmap.png',
            numLights=2, L=1.0, La=0.2,
            Ks=LVecBase3f(0.2, 0.2, 0.2), Shininess=1.0)

    def _normal_parallax_maps(self, vertex, fragment,
                              terrainDiffuseTexture=None,
                              terrainNormalTexture=None,
                              terrainHeightTexture=None,
                              modelDiffuseTexture=None,
                              modelNormalTexture=None,
                              modelHeightTexture=None,
                              numLights=2, L=1.0, La=0.2,
                              Ks=LVecBase3f(0.2, 0.2, 0.2),
                              Shininess=50.0):
        self._reset_models()
        self.terrainModel = 'terrain-tbn.egg'
        self.modelName = 'ogre-tbn.egg'
        self.modelScale = 1.7
        self._restart()
        for c in self.cubes:
            c.set_z(1.3)
        # self._printRenderStateInfos(self.modelCube)
        
        TextureStage.get_default().set_sort(0)  # optional
        self.stage1.set_sort(1)  # needed
        self.stage2.set_sort(2)  # needed
        
        # setup terrain's textures
        terrainDiffuseMap = self.loader.load_texture(terrainDiffuseTexture)
        # terrainDiffuseMap.set_magfilter(SamplerState.FT_linear)
        terrainDiffuseMap.set_minfilter(SamplerState.FT_linear)
        if terrainNormalTexture:
            terrainNormalMap = self.loader.load_texture(terrainNormalTexture)
            # terrainNormalMap.set_magfilter(SamplerState.FT_linear)
            terrainNormalMap.set_minfilter(SamplerState.FT_linear)
        if terrainHeightTexture:
            terrainHeightMap = self.loader.load_texture(terrainHeightTexture)
            # terrainHeightMap.set_magfilter(SamplerState.FT_linear)
            terrainHeightMap.set_minfilter(SamplerState.FT_linear)        
        # setup terrain's stages
        self.terrainNP.set_texture(TextureStage.get_default(),
                                   terrainDiffuseMap, 1)
        if modelNormalTexture:
            self.terrainNP.set_texture(self.stage1, terrainNormalMap)
        if modelHeightTexture:
            self.terrainNP.set_texture(self.stage2, terrainHeightMap)
        
        # setup models' textures
        modelDiffuseMap = self.loader.load_texture(modelDiffuseTexture)
        # modelDiffuseMap.set_magfilter(SamplerState.FT_linear)
        modelDiffuseMap.set_minfilter(SamplerState.FT_linear)
        if modelNormalTexture:
            modelNormalMap = self.loader.load_texture(modelNormalTexture)
            # modelNormalMap.set_magfilter(SamplerState.FT_linear)
            modelNormalMap.set_minfilter(SamplerState.FT_linear)
        if modelHeightTexture:
            modelHeightMap = self.loader.load_texture(modelHeightTexture)
            # modelHeightMap.set_magfilter(SamplerState.FT_linear)
            modelHeightMap.set_minfilter(SamplerState.FT_linear)
        # setup models' stages
        for c in self.cubes:
            c.set_texture(TextureStage.get_default(), modelDiffuseMap, 1)
            if modelNormalTexture:
                c.set_texture(self.stage1, modelNormalMap)
            if modelHeightTexture:
                c.set_texture(self.stage2, modelHeightMap)
        
        # setup render states
        self.root.set_two_sided(True)
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL, vertex=vertex,
                                  fragment=fragment)
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = numLights
        maxL = L / n
        maxLa = La / n
        self._setupMultiLigths(n, L=LVecBase3f(maxL, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self.root.set_shader_input('directional', self.directional)
        self.root.set_shader_input('render', self.render)
        self._setupMaterial(self.root, Ks=Ks, Shininess=Shininess)

    def m03_alpha_maps(self):
        self._reset_models()
        self.modelName = 'red_car.egg'
        self.modelScale = 0.25
        self.textureName1 = 'maps/envir-reeds.png'
        self._restart()
        # self._printRenderStateInfos(self.modelCube)
        
        TextureStage.get_default().set_sort(0)  # optional
        self.stage1.set_sort(1)  # needed
        self.root.set_texture(self.stage1, self.texture1)
        self.texture1.set_magfilter(SamplerState.FT_linear)
        self.texture1.set_minfilter(SamplerState.FT_nearest)
        self.root.set_two_sided(True)
        
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part1/s03-alpha-maps-vert.glsl',
                                  fragment='part1/s03-alpha-maps-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 2
        maxL = 1.0 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, L=LVecBase3f(maxL, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self.root.set_shader_input('directional', self.directional)
        self.root.set_shader_input('render', self.render)
        self._setupMaterial(self.root,
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=480.0)

    def m02_multi_texture(self):
        self._reset_models()
        self.modelName = 'red_car.egg'
        self.modelScale = 0.25
        self.textureName1 = 'maps/envir-mountain1.png'
        self._restart()
      
        TextureStage.get_default().set_sort(0)  # optional
        self.root.set_texture(self.stage1, self.texture1)
        self.stage1.set_sort(1)  # needed
        self.texture1.set_magfilter(SamplerState.FT_linear)
        self.texture1.set_minfilter(SamplerState.FT_nearest)
        
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part1/s02-multi-texture-vert.glsl',
                                  fragment='part1/s02-multi-texture-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 2
        maxL = 1.0 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, L=LVecBase3f(maxL, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self.root.set_shader_input('directional', self.directional)
        self.root.set_shader_input('render', self.render)
        self._setupMaterial(self.root,
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=480.0)

    def m01_2D_texture(self):
        self._reset_models()
        self.modelName = 'red_car.egg'
        self.modelScale = 0.25
        self._restart()
        # self._printRenderStateInfos(self.modelCube)
        
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part1/s01-2D-texture-vert.glsl',
                                  fragment='part1/s01-2D-texture-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 2
        maxL = 1.0 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, L=LVecBase3f(maxL, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self.root.set_shader_input('directional', self.directional)
        self.root.set_shader_input('render', self.render)
        self._setupMaterial(self.root,
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=480.0)

    def m00_simple_texturing(self):
        #
        self._reset_models()
        self.textureName1 = 'arrow.png'
        self.textureName2 = 'circle.png'
        self.modelName = 'panda.egg'
        self._restart()
        
        # Texture Wrap Modes
        # self.texture1.set_wrap_u(Texture.WM_repeat)
        # self.texture1.set_wrap_v(Texture.WM_clamp)
        
        # Texture Replacement
        self.root.set_texture(self.texture1, 1)
        # self.root.set_texture(TextureStage.get_default(), self.texture1, 1)
        
        # Multitexture basic
        # self.root.set_texture(self.stage1, self.texture1, 1)
        # self.root.set_texture(self.stage2, self.texture2)
        
        # Texture Modes
        # self.stage1.set_sort(1)
        # self.stage2.set_sort(2)
        # self.stage1.set_mode(TextureStage.M_modulate)
        # self.stage1.set_mode(TextureStage.M_add)
        # self.stage1.set_mode(TextureStage.M_replace)
        # self.stage1.set_mode(TextureStage.M_decal)


if __name__ == '__main__':
    app = Part1App()
    app.testToRun()
    app.run()
