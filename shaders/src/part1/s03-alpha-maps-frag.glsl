#version 430

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;
layout(location=2) in vec2 TexCoord;

layout(location=0) out vec4 FragColor;

layout(binding=0) uniform sampler2D p3d_Texture0; // diffuse map
layout(binding=1) uniform sampler2D p3d_Texture1;// alpha map

const int NL = 2;
uniform struct LightInfo
{
	vec4 Position;  // Light position in eye coords.
	vec3 La;        // Ambient light intesity
	vec3 L;         // Diffuse and specular light intensity
} Light[NL];
uniform bool directional;
uniform mat4 trans_model_of_render_to_view; // world center transform wrt camera

uniform struct MaterialInfo
{
	vec3 Ks;      		// Specular reflectivity
	float Shininess; 	// Specular shininess factor
} Material;

vec3 blinnPhongModel(int light, vec3 position, vec3 n)
{
	vec3 texColor = texture(p3d_Texture0, TexCoord).rgb;

	vec3 ambient = Light[light].La * texColor;
	vec3 s = vec3(0);
	if (directional)
	{
		// Directional light
		vec4 renderPos = trans_model_of_render_to_view * vec4(0, 0, 0, 1);
		s = normalize(Light[light].Position.xyz - renderPos.xyz);
	}
	else
	{
		// Positional light
		s = normalize(Light[light].Position.xyz - position);
	}
	float sDotN = max(dot(s, n), 0.0);
	vec3 diffuse = texColor * sDotN;

	vec3 spec = vec3(0.0);
	if (sDotN > 0.0)
	{
		vec3 v = normalize(-position.xyz);
		vec3 h = normalize(v + s);
		spec = Material.Ks * pow(max(dot(h, n), 0.0), Material.Shininess);
	}

	return ambient + Light[light].L * (diffuse + spec);
}

void main()
{
	// All computations in view (i.e. camera) space
	vec4 alphaMap = texture(p3d_Texture1, TexCoord);

	if (alphaMap.a < 0.15)
	{
		discard;
	}
	else
	{
		// Evaluate the lighting equation, for each light
		vec3 Color = vec3(0);
		// renormalize Normal
		vec3 reNormal = vec3(0);
		if (gl_FrontFacing)
		{
			reNormal = normalize(Normal);
		}
		else
		{
			reNormal = normalize(-Normal);
		}

		for (int i = 0; i < NL; i++)
		{
			Color += blinnPhongModel(i, Position, reNormal);
		}

		FragColor = vec4(Color, 1.0);
	}
}
