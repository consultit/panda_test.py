#version 440

layout (location=0) in vec4 p3d_Vertex;
layout (location=1) in vec3 p3d_Normal;
layout (location=2) in vec2 p3d_MultiTexCoord0;
layout (location=3) in vec3 p3d_Tangent;
layout (location=4) in vec3 p3d_Binormal;

const int NL = 1;

layout(location=0) out vec3 LightDir[NL];
layout(location=NL) out vec2 TexCoord;
layout(location=NL+1) out vec3 ViewDir;

uniform struct LightInfo
{
	vec4 Position;  // Light position in cam. coords.
	vec3 L;         // D,S intensity
	vec3 La;        // Amb intensity
} Light[NL];
uniform bool directional;
uniform mat4 trans_model_of_render_to_view; // world center transform wrt camera

uniform mat4 trans_model_to_clip;
uniform mat4 trans_model_to_view;
uniform mat3 p3d_NormalMatrix;

void main()
{
	// All computations in view (i.e. camera) space
	// Compute system of reference: (t,b,n)
	vec3 norm = normalize(p3d_NormalMatrix * p3d_Normal);
	vec3 tang = normalize(mat3(trans_model_to_view) * p3d_Tangent);
	vec3 binormal = normalize(mat3(trans_model_to_view) * p3d_Binormal);

	// Matrix for transformation to tangent space
	mat3 toObjectLocal = mat3(tang.x, binormal.x, norm.x, tang.y, binormal.y,
			norm.y, tang.z, binormal.z, norm.z);

	// Transform light(s) direction(s) and view direction to tangent space
	vec3 pos = vec3(trans_model_to_view * p3d_Vertex);

	if (directional)
	{
		// Directional light
		vec4 renderPos = trans_model_of_render_to_view * vec4(0, 0, 0, 1);
		for (int i = 0; i < NL; i++)
		{
			LightDir[i] = toObjectLocal
					* (Light[i].Position.xyz - renderPos.xyz);
		}
	}
	else
	{
		// Positional light
		for (int i = 0; i < NL; i++)
		{
			LightDir[i] = toObjectLocal * (Light[i].Position.xyz - pos);
		}
	}

	ViewDir = toObjectLocal * normalize(-pos);

	TexCoord = p3d_MultiTexCoord0;

	gl_Position = trans_model_to_clip * p3d_Vertex;
}
