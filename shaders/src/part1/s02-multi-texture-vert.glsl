#version 430

layout (location=0) in vec4 p3d_Vertex;
layout (location=1) in vec3 p3d_Normal;
layout (location=2) in vec2 p3d_MultiTexCoord0;

layout(location=0) out vec3 Position;
layout(location=1) out vec3 Normal;
layout(location=2) out vec2 TexCoord;

uniform mat4 trans_model_to_clip;
uniform mat4 trans_model_to_view;
uniform mat3 p3d_NormalMatrix;

void main()
{
	TexCoord = p3d_MultiTexCoord0;
	
	// All computations in view (i.e. camera) space
	Position = (trans_model_to_view * p3d_Vertex).xyz;
	Normal = normalize(p3d_NormalMatrix * p3d_Normal);

	gl_Position = trans_model_to_clip * p3d_Vertex;
}
