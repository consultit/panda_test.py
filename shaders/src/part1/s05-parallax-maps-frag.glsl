#version 440

const int NL = 3;

layout(location=0) in vec3 LightDir[NL];
layout(location=NL) in vec2 TexCoord;
layout(location=NL+1) in vec3 ViewDir;

layout(location=0) out vec4 FragColor;

layout(binding=0) uniform sampler2D p3d_Texture0; // diffuse map
layout(binding=1) uniform sampler2D p3d_Texture1;// normal map
layout(binding=2) uniform sampler2D p3d_Texture2;// height/bump map

uniform struct LightInfo
{
	vec4 Position;  // Light position in cam. coords.
	vec3 L;         // D,S intensity
	vec3 La;        // Amb intensity
} Light[NL];

uniform struct MaterialInfo
{
	vec3 Ks;      		// Specular reflectivity
	float Shininess; 	// Specular shininess factor
} Material;

vec3 blinnPhong(int light)
{
	vec3 v = normalize(ViewDir);
	vec3 s = normalize(LightDir[light]);

	const float bumpFactor = 0.009;
	float height = 1 - texture(p3d_Texture2, TexCoord).r;
	vec2 delta = vec2(v.x, v.y) * height * bumpFactor / v.z;
	vec2 tc = TexCoord.xy - delta;
	//tc = TexCoord.xy;

	vec3 n = texture(p3d_Texture1, tc).xyz;
	n.xy = 2.0 * n.xy - 1.0;
	n = normalize(n);

	float sDotN = max(dot(s, n), 0.0);

	vec3 texColor = texture(p3d_Texture0, tc).rgb;
	vec3 ambient = Light[light].La * texColor;
	vec3 diffuse = texColor * sDotN;
	vec3 spec = vec3(0.0);
	if (sDotN > 0.0)
	{
		vec3 h = normalize(v + s);
		spec = Material.Ks * pow(max(dot(h, n), 0.0), Material.Shininess);
	}
	return ambient + Light[light].L * (diffuse + spec);
}

void main()
{
	vec3 Color = vec3(0);
	for (int i = 0; i < NL; i++)
	{
		vec3 c = blinnPhong(i);
		//c = pow(c, vec3(1.0 / 2.2));
		Color += c;
	}
	FragColor = vec4(Color, 1.0);
}
