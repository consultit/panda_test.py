#version 440

const int NL = 2;

layout(location=0) in vec3 LightDir[NL];
layout(location=NL) in vec2 TexCoord;
layout(location=NL+1) in vec3 ViewDir;

layout(location=0) out vec4 FragColor;

layout(binding=0) uniform sampler2D p3d_Texture0; // diffuse map
layout(binding=1) uniform sampler2D p3d_Texture1;// normal map

uniform struct LightInfo
{
	vec4 Position;  // Light position in cam. coords.
	vec3 L;         // D,S intensity
	vec3 La;        // Amb intensity
} Light[NL];

uniform struct MaterialInfo
{
	vec3 Ks;      		// Specular reflectivity
	float Shininess; 	// Specular shininess factor
} Material;

vec3 blinnPhong(int light, vec3 n)
{
	vec3 texColor = texture(p3d_Texture0, TexCoord).rgb;

	vec3 ambient = Light[light].La * texColor;
	vec3 s = normalize(LightDir[light]);
	float sDotN = max(dot(s, n), 0.0);
	vec3 diffuse = texColor * sDotN;

	vec3 spec = vec3(0.0);
	if (sDotN > 0.0)
	{
		vec3 v = normalize(ViewDir);
		vec3 h = normalize(v + s);
		spec = Material.Ks * pow(max(dot(h, n), 0.0), Material.Shininess);
	}
	return ambient + Light[light].L * (diffuse + spec);
}

void main()
{
	// Lookup the normal from the normal map

	vec3 norm = texture(p3d_Texture1, TexCoord).xyz;
	norm = 2.0 * texture(p3d_Texture1, TexCoord).xyz - 1.0;
//	norm.xy = 2.0 * norm.xy - 1.0;

	vec3 Color = vec3(0);
	for (int i = 0; i < NL; i++)
	{
		Color += blinnPhong(i, norm);
	}
	FragColor = vec4(Color, 1.0);
}
