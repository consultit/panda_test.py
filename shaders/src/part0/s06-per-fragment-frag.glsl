#version 430

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;

layout(location=0) out vec4 FragColor;

const int NL = 6;
uniform struct LightInfo
{
	vec4 Position;  // Light position in eye coords.
	vec3 La;        // Ambient light intesity
	vec3 L;         // Diffuse and specular light intensity
} Light[NL];

uniform struct MaterialInfo
{
	vec3 Ka;      		// Ambient reflectivity
	vec3 Kd;      		// Diffuse reflectivity
	vec3 Ks;      		// Specular reflectivity
	float Shininess; 	// Specular shininess factor
} Material;

vec3 phongModel(int light, vec3 position, vec3 n)
{
	vec3 ambient = Light[light].La * Material.Ka;
	vec3 s = normalize(Light[light].Position.xyz - position);
	float sDotN = max(dot(s, n), 0.0);
	vec3 diffuse = Material.Kd * sDotN;
	vec3 spec = vec3(0.0);
	if (sDotN > 0.0)
	{
		vec3 v = normalize(-position.xyz);
		vec3 r = reflect(-s, n);
		spec = Material.Ks * pow(max(dot(r, v), 0.0), Material.Shininess);
	}

	return ambient + Light[light].L * (diffuse + spec);
}

void main()
{
	// All computations in view (i.e. camera) space
	// Evaluate the lighting equation, for each light
	vec3 Color = vec3(0.0);
	// renormalize Normal
	vec3 reNormal = normalize(Normal);
	for (int i = 0; i < NL; i++)
	{
		Color += phongModel(i, Position, reNormal);
	}
	
	FragColor = vec4(Color, 1.0);
}
