#version 430

layout (location=0) in vec4 p3d_Vertex;
layout (location=1) in vec3 p3d_Normal;
layout (location=2) in vec2 p3d_MultiTexCoord0;

uniform mat4 trans_model_to_clip;
uniform mat4 trans_model_to_view;
uniform mat3 p3d_NormalMatrix;

uniform mat4 trans_model_of_LightBulb_to_view;
vec4 LightPosition; 	// Light position in eye coords.
uniform struct LightInfo
{
	vec4 Position; 		// Light position in eye coords. NOT USED
	vec3 La;       		// Ambient light intensity
	vec3 Ld;       		// Diffuse light intensity
	vec3 Ls;       		// Specular light intensity
} Light[1];

uniform struct MaterialInfo
{
	vec3 Ka;      		// Ambient reflectivity
	vec3 Kd;      		// Diffuse reflectivity
	vec3 Ks;      		// Specular reflectivity
	float Shininess; 	// Specular shininess factor
} Material;

layout(location=0) out vec3 FrontColor;
layout(location=1) out vec3 BackColor;
layout(location=2) out vec2 TexCoord;

// utility functions
void getCamSpace( out vec3 normal, out vec4 position )
{
	normal = normalize(p3d_NormalMatrix * p3d_Normal);
	position = trans_model_to_view * p3d_Vertex;
}

vec3 phongModel(in vec3 normal, in vec4 position)
{
	LightPosition = trans_model_of_LightBulb_to_view * vec4(0, 0, 0, 1);
	vec3 ambient = Light[0].La * Material.Ka;
	vec3 s = normalize(vec3(LightPosition - position));
	float sDotN = max(dot(s, normal), 0.0);
	vec3 diffuse = Light[0].Ld * Material.Kd * sDotN;
	vec3 spec = vec3(0.0);
	if (sDotN > 0.0)
	{
		vec3 v = normalize(-position.xyz);
		vec3 r = reflect(-s, normal);
		spec = Light[0].Ls * Material.Ks
		* pow(max(dot(r, v), 0.0), Material.Shininess);
	}

	return ambient + diffuse + spec;
}
void main()
{
	// All computations in view (i.e. camera) space
	TexCoord = p3d_MultiTexCoord0;
	// Get the position and normal in camera space
	vec3 camNorm;
	vec4 camPosition;
	getCamSpace(camNorm, camPosition);

	FrontColor = phongModel(camNorm, camPosition);
	BackColor = phongModel(-camNorm, camPosition);

	gl_Position = trans_model_to_clip * p3d_Vertex;
}
