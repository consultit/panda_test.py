#version 430

layout (location=0) in vec4 p3d_Vertex;
layout (location=1) in vec3 p3d_Normal;

layout(location=0) out vec3 Color;

const int NL = 6;
uniform struct LightInfo
{
	vec4 Position;  // Light position in eye coords.
	vec3 La;        // Ambient light intesity
	vec3 L;         // Diffuse and specular light intensity
} Light[NL];

uniform struct MaterialInfo
{
	vec3 Ka;      		// Ambient reflectivity
	vec3 Kd;      		// Diffuse reflectivity
	vec3 Ks;      		// Specular reflectivity
	float Shininess; 	// Specular shininess factor
} Material;

uniform mat4 trans_model_to_clip;
uniform mat4 trans_model_to_view;
uniform mat3 p3d_NormalMatrix;

vec3 phongModel(int light, vec3 position, vec3 n)
{
	vec3 ambient = Light[light].La * Material.Ka;
	vec3 s = normalize(Light[light].Position.xyz - position);
	float sDotN = max(dot(s, n), 0.0);
	vec3 diffuse = Material.Kd * sDotN;
	vec3 spec = vec3(0.0);
	if (sDotN > 0.0)
	{
		vec3 v = normalize(-position.xyz);
		vec3 r = reflect(-s, n);
		spec = Material.Ks * pow(max(dot(r, v), 0.0), Material.Shininess);
	}

	return ambient + Light[light].L * (diffuse + spec);
}

void main()
{
	// All computations in view (i.e. camera) space
	vec3 camNorm = normalize(p3d_NormalMatrix * p3d_Normal);
	vec3 camPosition = (trans_model_to_view * p3d_Vertex).xyz;

	// Evaluate the lighting equation, for each light
	Color = vec3(0.0);
	for (int i = 0; i < NL; i++)
	{
		Color += phongModel(i, camPosition, camNorm);
	}

	gl_Position = trans_model_to_clip * p3d_Vertex;
}
