#version 430

layout (location=0) in vec4 p3d_Vertex;
layout (location=1) in vec4 p3d_Color;

layout(location=0) out vec3 Color;

uniform mat4 trans_model_to_clip; //p3d_ModelViewProjectionMatrix;

uniform struct TestStruct
{
	float a;
	float b;
	vec3 c;
} testStruct;
uniform float testArrayf[3];
uniform TestStruct testStructArray[2];

void main()
{
	// p3d_Color
	///Color = p3d_Color.xyz;

	// testStruct
	///Color = vec3(testStruct.a, testStruct.b, 0.0);
	///Color = testStruct.c;

	// testArrayf
	///Color = vec3(testArrayf[0], testArrayf[1], testArrayf[2]);

	// testStructArray
	Color = vec3(0, 0, 0);
	for (int i = 0; i < 2; ++i)
	{
		Color += vec3(testStructArray[i].a, testStructArray[i].b, 0.0);
	}

	gl_Position = trans_model_to_clip * p3d_Vertex;
}
