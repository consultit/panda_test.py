#version 430

layout (location=0) in vec4 p3d_Vertex;

uniform mat4 trans_model_of_toNode_to_clip;

void main()
{
	gl_Position = trans_model_of_toNode_to_clip * p3d_Vertex;
}
