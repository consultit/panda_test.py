#version 430

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;

layout(location=0) out vec4 FragColor;

const int NL = 6;
uniform struct SpotLightInfo
{
	vec3 Position;  // Position in cam coords
	vec3 L;         // Diffuse/spec intensity
	vec3 La;        // Amb intensity
	vec3 Direction; // Direction of the spotlight in cam coords.
	float Exponent; // Angular attenuation exponent
	float Cutoff;   // Cutoff angle (between 0 and pi/2)
} Light[NL];

uniform struct MaterialInfo
{
	vec3 Ka;      		// Ambient reflectivity
	vec3 Kd;      		// Diffuse reflectivity
	vec3 Ks;      		// Specular reflectivity
	float Shininess; 	// Specular shininess factor
} Material;

vec3 blinnPhongSpotModel(int light, vec3 position, vec3 n)
{
	vec3 ambient = Light[light].La * Material.Ka, diffuse = vec3(0), spec = vec3(0);
	vec3 s = normalize(Light[light].Position - position);
	float cosAng = dot(-s, normalize(Light[light].Direction));
	float angle = acos(cosAng);
	float spotScale = 0.0;
	if (angle < Light[light].Cutoff)
	{
		spotScale = pow(cosAng, Light[light].Exponent);
		float sDotN = max(dot(s, n), 0.0);
		diffuse = Material.Kd * sDotN;
		if (sDotN > 0.0)
		{
			vec3 v = normalize(-position.xyz);
			vec3 h = normalize(v + s);
			spec = Material.Ks * pow(max(dot(h, n), 0.0), Material.Shininess);
		}
	}
	return ambient + spotScale * Light[light].L * (diffuse + spec);
}
void main()
{
	// All computations in view (i.e. camera) space
	// Evaluate the lighting equation, for each light
	vec3 Color = vec3(0.0);
	// renormalize Normal
	vec3 reNormal = normalize(Normal);
	for (int i = 0; i < NL; i++)
	{
		Color += blinnPhongSpotModel(i, Position, reNormal);
	}

	FragColor = vec4(Color, 1.0);
}
