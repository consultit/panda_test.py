#version 430

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;

layout(location=0) out vec4 FragColor;

const int NL = 2;
uniform struct LightInfo
{
	vec4 Position;  // Light position in eye coords.
	vec3 La;        // Ambient light intesity
	vec3 L;         // Diffuse and specular light intensity
} Light[NL];

uniform struct MaterialInfo
{
	vec3 Ka;      		// Ambient reflectivity
	vec3 Kd;      		// Diffuse reflectivity
	vec3 Ks;      		// Specular reflectivity
	float Shininess; 	// Specular shininess factor
} Material;

const int levels = 3;
const float scaleFactor = 1.0 / levels;

vec3 toonShadingModel(int light, vec3 n)
{
	vec3 s = normalize(Light[light].Position.xyz - Position);
	vec3 ambient = Light[light].La * Material.Ka;
	float sDotN = max(dot(s, n), 0.0);
	vec3 diffuse = Material.Kd * floor(sDotN * levels) * scaleFactor;

	return ambient + Light[light].L * diffuse;
}
void main()
{
	// All computations in view (i.e. camera) space
	// Evaluate the lighting equation, for each light
	vec3 Color = vec3(0.0);
	// renormalize Normal
	vec3 reNormal = normalize(Normal);
	for (int i = 0; i < NL; i++)
	{
		Color += toonShadingModel(i, reNormal);
	}

	FragColor = vec4(Color, 1.0);
}
