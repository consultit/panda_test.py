'''
Created on Jan 12, 2020

@author: consultit
'''

from basePandaApp import BasePandaApp
from panda3d.core import Shader, LVecBase3f, LVector3f
import math


class Part0App(BasePandaApp):
    '''
    Part0App
    '''
    
    def __init__(self):
        '''
        Constructor
        '''

        super().__init__()

    def m11_pbr(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s11-pbr-vert.glsl',
                                  fragment='part0/s11-pbr-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 6
        maxL = 1.5 / n
        self._setupMultiLigths(n, rotate=True, L=LVecBase3f(maxL, maxL, maxL))
        self.root.set_shader_input('directional', self.directional)
        self.root.set_shader_input('render', self.render)
        self._setupMaterial(self.root, Rough=0.5, Metal=False,
                                Color=LVecBase3f(0.3, 0.2, 0.1))
        # recreate the models and apply different materials
        for cube in self.cubes:
            cube.remove_node()
        self.cubes = []
        halfWidth = 5.0
        # create and draw dielectric models with varying roughness
        numD = 5
        halfDeltaD = halfWidth / numD
        deltaD = 2.0 * halfDeltaD
        scale = (0.18 * 3.0) / numD
        dielBaseColor = LVecBase3f(0.1, 0.33, 0.17)
        for i in range(numD):
            cube = self.modelCube.copy_to(self.root)
            cube.set_name('dielectric' + str(i))
            self.cubes += [ cube ]
            cubeX = -halfWidth + halfDeltaD + i * deltaD
            cube.set_pos(cubeX, 2.0, 0.0)
            cube.set_scale(scale)
            rough = (i + 1) * (1.0 / numD)
            self._setupMaterial(cube, Rough=rough, Metal=False,
                                Color=dielBaseColor)
        # create and add metallic models
        halfDeltaM = halfWidth / 5
        deltaM = 2.0 * halfDeltaM
        scale = (0.18 * 3.0) / 5
        for i in range(5):
            cube = self.modelCube.copy_to(self.root)
            cube.set_name('metallic' + str(i))
            self.cubes += [ cube ]
            cubeX = -halfWidth + halfDeltaM + i * deltaM
            cube.set_pos(cubeX, -2.0, 0.0)
            cube.set_scale(scale)
        # draw metal models
        metalRough = 0.43
        # Gold
        self._setupMaterial(self.cubes[numD], Rough=metalRough, Metal=True,
                            Color=LVecBase3f(1, 0.71, 0.29))
        # Copper
        self._setupMaterial(self.cubes[numD + 1], Rough=metalRough, Metal=True,
                            Color=LVecBase3f(0.95, 0.64, 0.54))
        # Aluminum
        self._setupMaterial(self.cubes[numD + 2], Rough=metalRough, Metal=True,
                            Color=LVecBase3f(0.91, 0.92, 0.92))
        # Titanium
        self._setupMaterial(self.cubes[numD + 3], Rough=metalRough, Metal=True,
                            Color=LVecBase3f(0.542, 0.497, 0.449))
        # Silver
        self._setupMaterial(self.cubes[numD + 4], Rough=metalRough, Metal=True,
                            Color=LVecBase3f(0.95, 0.93, 0.88))

    def m10_fog(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s10-fog-vert.glsl',
                                  fragment='part0/s10-fog-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 1
        maxL = 2.5 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, rotate=False, L=LVecBase3f(0.0, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self._setupMaterial(self.root,
                            Kd=LVecBase3f(0.4, 0.4, 0.4),
                            Ka=LVecBase3f(0.6, 0.6, 0.6),
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=480.0)
        # setup fog
        self.root.set_shader_input('Fog.MaxDist', [30.0])
        self.root.set_shader_input('Fog.MinDist', [1.0])
        self.root.set_shader_input('Fog.Color', LVecBase3f(0.21, 0.21, 0.21))

    def m09_toon_shading(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s09-toon-shading-vert.glsl',
                                  fragment='part0/s09-toon-shading-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 2
        maxL = 2.5 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, L=LVecBase3f(0.0, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self._setupMaterial(self.root,
                            Kd=LVecBase3f(0.4, 0.4, 0.4),
                            Ka=LVecBase3f(0.6, 0.6, 0.6),
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=180.0)

    def m08_spotlight(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s08-spotlight-vert.glsl',
                                  fragment='part0/s08-spotlight-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 6
        maxL = 3.5 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, isSpotlight=True, L=LVecBase3f(0.0, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0),
                               Exponent=50.0, Cutoff=math.radians(15))        
        self._setupMaterial(self.root,
                            Kd=LVecBase3f(0.4, 0.4, 0.4),
                            Ka=LVecBase3f(0.6, 0.6, 0.6),
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=480.0)

    def m07_blinn_phong(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s07-blinn-phong-vert.glsl',
                                  fragment='part0/s07-blinn-phong-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 6
        maxL = 2.5 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, L=LVecBase3f(0.0, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self._setupMaterial(self.root,
                            Kd=LVecBase3f(0.4, 0.4, 0.4),
                            Ka=LVecBase3f(0.6, 0.6, 0.6),
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=480.0)

    def m06_per_fragment(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s06-per-fragment-vert.glsl',
                                  fragment='part0/s06-per-fragment-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 6
        maxL = 2.5 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, L=LVecBase3f(0.0, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self._setupMaterial(self.root,
                            Kd=LVecBase3f(0.4, 0.4, 0.4),
                            Ka=LVecBase3f(0.6, 0.6, 0.6),
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=180.0)

    def m05_multi_lights(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s05-multi-lights-vert.glsl',
                                  fragment='part0/s05-multi-lights-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        n = 6
        maxL = 2.5 / n
        maxLa = 0.2 / n
        self._setupMultiLigths(n, L=LVecBase3f(0.0, maxL, maxL),
                               La=LVecBase3f(maxLa, maxLa, 0.0))
        self._setupMaterial(self.root,
                            Kd=LVecBase3f(0.4, 0.4, 0.4),
                            Ka=LVecBase3f(0.6, 0.6, 0.6),
                            Ks=LVecBase3f(0.9, 0.9, 0.9),
                            Shininess=180.0)

    def m04_discard(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s04-discard-vert.glsl',
                                  fragment='part0/s04-discard-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        self._setupMultiLigths(1, Ld=LVecBase3f(1.0, 1.0, 1.0),
                               La=LVecBase3f(0.4, 0.4, 0.4),
                               Ls=LVecBase3f(1.0, 1.0, 1.0))
        self.root.set_shader_input('LightBulb', self.LightBulbs[0])
        self._setupMaterial(self.root,
                            Kd=LVecBase3f(0.9, 0.5, 0.3),
                            Ka=LVecBase3f(0.9, 0.5, 0.3),
                            Ks=LVecBase3f(0.8, 0.8, 0.8),
                            Shininess=100.0)

    def m03_phong(self):
        self._reset_models()
        self._restart()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s03-phong-vert.glsl',
                                  fragment='part0/s03-phong-frag.glsl')
        self.root.set_shader(self.shader)
        # setup lights, materials
        self._setupMultiLigths(1, Ld=LVecBase3f(1.0, 1.0, 1.0),
                               La=LVecBase3f(0.4, 0.4, 0.4),
                               Ls=LVecBase3f(1.0, 1.0, 1.0))
        self.root.set_shader_input('LightBulb', self.LightBulbs[0])
        self.root.set_shader_input('directional', self.directional)
        self.root.set_shader_input('render', self.render)
        self._setupMaterial(self.root,
                            Kd=LVecBase3f(0.9, 0.5, 0.3),
                            Ka=LVecBase3f(0.9, 0.5, 0.3),
                            Ks=LVecBase3f(0.8, 0.8, 0.8),
                            Shininess=100.0)

    def m02_transform(self):
        self._reset_models()
        self._restart()
        # test about uniform transforms' parameters passing
        toNode = self.root.attach_new_node('toNode')
        toNode.set_pos(self.cubes[2].get_pos() + LVector3f(0, 0, 2))
        toNode.set_scale(self.cubes[2].get_scale())
        toNode.hide()
        # Load the shader from the file.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s02-transform-vert.glsl',
                                  fragment='part0/s02-transform-frag.glsl')
        # set toNode transform
        self.cubes[2].set_shader_input('toNode', toNode)
        self.cubes[2].set_shader(self.shader)

    def m01_shader_inputs(self):
        self._reset_models()
        self._restart()
        # Load the shader from the files.
        self.shader = Shader.load(Shader.SL_GLSL,
                                  vertex='part0/s01-vert.glsl',
                                  fragment='part0/s01-frag.glsl')
        # Assign the shader to work on the root node
        # If you remove the line below, you will see
        # that panda is actually rendering our scene.
        self.root.set_shader_input('testStruct.a', [1.0])
        self.root.set_shader_input('testStruct.b', [0.0])
        self.root.set_shader_input('testStruct.c', LVecBase3f(0.0, 1.0, 0.0))
        self.root.set_shader_input('testArrayf', [0.0, 0.0, 1.0])
        
        for i in range(2):
            element = 'testStructArray[' + str(i) + ']'
            self.root.set_shader_input(element + '.a', [0.0])
            self.root.set_shader_input(element + '.b', [0.5])
            self.root.set_shader_input(element + '.c',
                                       LVecBase3f(0.0, 0.0, 0.0))
        
        self.root.set_shader(self.shader)


if __name__ == '__main__':
    app = Part0App()
    app.testToRun()
    app.run()
