'''
Created on Jan 18, 2020

@author: consultit
'''

from panda3d.core import load_prc_file_data, WindowProperties, TextureStage, \
    NodePath, NodePathCollection, GeomNode, TextureAttrib, ColorAttrib, \
    LVector2f, LVector3f, LVecBase3f, LColorf, ConfigVariableManager
from direct.interval.LerpInterval import LerpFunc
from direct.showbase.ShowBase import ShowBase
import inspect, re, os, argparse, textwrap, sys, math


class BasePandaApp(ShowBase):
    '''
    BasePandaApp
    '''

    def __init__(self):
        '''
        Constructor
        '''

        pattern = 'm\d\d_'
        self.testChoises = {name[4:]:name for name, _ in inspect.getmembers(self,
                                            predicate=inspect.ismethod(self))
                                    if re.match(pattern, name)}        
        _myScriptName = os.path.basename(__file__)
        # create the arguments' parser
        parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description=textwrap.dedent('''
        This is the ''' + _myScriptName + ''' test program.
        
        Commands' keys:
            - 'n' to cycle among tests
            - 'escape' to exit
            - 'o' to toggle oobe
            - 'i' to toggle models' rotations
            - 'wasd' + 'e' + 'q' to move environment
            - 'f1' to toggle directional/point light (only some tests)
        '''))    
        # set up arguments
        parser.add_argument('--test', type=str, default='cycle tests',
                            choices=self.testChoises, help='the test to run')
        parser.add_argument('-d', '--data-dir', type=str, action='append',
                            help='the data dir(s)')
        # parse arguments
        args = parser.parse_args()
        # do actions
        # needed configuration
        # ConfigVariableManager.get_global_ptr().list_variables()
        load_prc_file_data('', 'win-size 1024 768')
        load_prc_file_data('', 'gl-debug #t')
        load_prc_file_data('', 'gl-debug-object-labels #t')
        load_prc_file_data('', 'gl-version 4 3')
        load_prc_file_data('', 'gl-coordinate-system default')
        load_prc_file_data('', 'max-texture-stages -1')
        load_prc_file_data('', 'cache-generated-shaders #f')
        load_prc_file_data('', 'model-cache-dir')
        load_prc_file_data('', 'model-cache-textures #f')
        
        # set data dir(s)
        if args.data_dir:
            for dataDir in args.data_dir:
                load_prc_file_data('', 'model-path ' + dataDir)
        
        # test to run
        self.testToRun = getattr(self, self.testChoises.get(args.test,
                                                       '_cycle_tests'))
        if self.testToRun == self._cycle_tests:
            self.tests = (test for test in self.testChoises.keys())
            self.accept('n', self._cycle_tests)
        
        super().__init__()
        self._setTitle(args.test)
        self.set_background_color(0.2, 0.2, 0.2)
        
        self.camLens.set_near_far(1.0, 200.0)
        self.camLens.set_fov(60.0)
        self.camera.set_pos(0.0, -10.0, 5.5)
        self.camera.look_at(0.0, 0.0, 1.0)
        self.disable_mouse()
        
        self.accept('escape', sys.exit)
        self.accept('o', self.oobe)
                
        self.interval = LerpFunc(self._animate, 10.0, 0.0, 360.0)
        self.accept('i', self._toggleInterval)
        
        self.accept('d', self._move, [1.0, 0.0, 0.0])
        self.accept('a', self._move, [-1.0, 0.0, 0.0])
        self.accept('w', self._move, [0.0, 1.0, 0.0])
        self.accept('s', self._move, [0.0, -1.0, 0.0])
        self.accept('e', self._move, [0.0, 0.0, 1.0])
        self.accept('q', self._move, [0.0, 0.0, -1.0])
        
        self.root = NodePath('root')
        self.lightRoot = NodePath('lightRoot')
        self.directional = [False]
        self.accept('f1', self._toggleDirectionalLight,
                    extraArgs=[self.directional])
        self.repeatCounter = 0
        self.terrainTexNum = 3
        self._reset_models()
    
    def _reset_models(self):
        i = str(self.repeatCounter % self.terrainTexNum)
        self.terrainModel = 'terrain.obj'
        self.terrainTextureName = 'terrain' + i + '-diffusemap.png'
        self.modelName = 'panda.egg'
        self.textureName1 = 'arrow.png'
        self.textureName2 = 'circle.png'
        self.modelScale = 0.18

    def _start(self):
        self.root = self.render.attach_new_node('Root')
        self.root.set_pos(0.0, 0.0, 0.0)
        self.lightRoot = self.render.attach_new_node('lightRoot')
        self.lightRoot.set_pos(0.0, 0.0, 0.0)
        
        self.modelCube = self.loader.load_model(self.modelName)
        self.cubes = []
        for x in [-3.0, 0.0, 3.0]:
            cube = self.modelCube.copy_to(self.root)
            cube.set_pos(x, 0.0, 0.0)
            cube.set_scale(self.modelScale)
            self.cubes += [ cube ]
            
        self.terrainNP = self.loader.load_model(self.terrainModel)
        self.terrainNP.set_name('terrain')
        self.terrainNP.set_scale(1.0)
        self.terrainTexture = self.loader.load_texture(self.terrainTextureName)
        self.terrainNP.set_texture(self.terrainTexture)
        self.terrainNP.set_z(0.0)
        self.terrainNP.reparent_to(self.root)
        
        # textures
        self.texture1 = self.loader.load_texture(self.textureName1)
        self.stage1 = TextureStage('Stage1')
        self.texture2 = self.loader.load_texture(self.textureName2)
        self.stage2 = TextureStage('Stage2')

    def _restart(self):
        self.root.remove_node()
        self.lightRoot.remove_node()
        self._start()

    def _setTitle(self, title):
        props = WindowProperties()
        props.set_title(title)
        self.win.request_properties(props)

    def _cycle_tests(self):
        try:
            currTest = self.testChoises[next(self.tests)]
        except StopIteration:
            delattr(self, 'tests')
            self.tests = (test for test in self.testChoises.keys())
            currTest = self.testChoises[next(self.tests)]
            self.repeatCounter += 1
        self._setTitle(currTest[4:])
        getattr(self, currTest)()

    def _animate(self, t):
        for i in range(len(self.cubes)):
            # self.cubes[i].set_h(t * (2.0 ** i))
            self.cubes[i].set_h(t * (2.0 + i))

    def _move(self, x, y, z):
        self.root.set_x(self.root.get_x() + x)
        self.root.set_y(self.root.get_y() + y)
        self.root.set_z(self.root.get_z() + z)
        self.lightRoot.set_z(self.root.get_z() + z)
        
    def _toggleInterval(self):
        if self.interval.isPlaying():
            self.interval.finish()
        else:
            self.interval.start()

    def _printRenderStateInfos(self, model):
        NumericType = {
            0:'NT_uint8',
            1:'NT_uint16',
            2:'NT_uint32',
            3:'NT_packed_dcba',
            4:'NT_packed_dabc',
            5:'NT_float32',
            6:'NT_float64',
            7:'NT_stdfloat',
            8:'NT_int8',
            9:'NT_int16',
            10:'NT_int32',
            11:'NT_packed_ufloat',
          }
        Contents = {
            0:'C_other',
            1:'C_point',
            2:'C_clip_point',
            3:'C_vector',
            4:'C_texcoord',
            5:'C_color',
            6:'C_index',
            7:'C_morph_delta',
            8:'C_matrix',
            9:'C_normal',
            }
        geomNodeCollection = NodePathCollection() 
        # check if model is GeomNode itself (i.e. when procedurally generated)
        if model.node().is_of_type(GeomNode.get_class_type()):
            geomNodeCollection.add_path(model)
        else:
            # get all GeomNodes for the hierarchy below model
            geomNodeCollection = model.find_all_matches('**/+GeomNode')
        sep0 = '  '
        sep1 = sep0 + sep0
        sep2 = sep1 + sep0
        sep3 = sep2 + sep0
        sep4 = sep3 + sep0
        sep5 = sep4 + sep0
        print(model.get_name() + 
              ': report of RenderState(s) applied to inner Geom(s)')
        # for each GeomNode
        for g in range(geomNodeCollection.get_num_paths()):
            geomNode = geomNodeCollection.get_path(g).node()
            print(sep0 + 'GeomNode ' + str(geomNode.get_name()) + ':')
            # for each Geom inside this GeomNode
            for i in range(geomNode.get_num_geoms()):
                # get vertex data infos
                geom = geomNode.get_geom(i)
                print(sep1 + 'Geom n. ' + str(i) + ':')
                vertexData = geom.get_vertex_data()
                vertexFormat = vertexData.get_format()
                print(sep2 + 'VertexFormat:')
                for k in range(vertexFormat.get_num_columns()):
                    column = vertexFormat.get_column(k)
                    print(sep3 + 'VertexColumn n. ' + str(k) + ' (repeated ' + 
                          str(column.get_num_elements()) + ' times):')
                    print(sep4 + 'Name: ' + column.get_name().get_name())
                    print(sep4 + 'Contents: ' + 
                          Contents[column.get_contents()])
                    print(sep4 + 'Components: ' + 
                          str(column.get_num_components()))
                    print(sep4 + 'Numeric Type: ' + 
                          NumericType[column.get_numeric_type()])
                # get the RenderState of this Geom
                geomRenderState = geomNode.get_geom_state(i)
                print(sep2 + 'RenderState:')
                # Analyze several RenderAttrib(s)
                # check if there is a TextureAttrib
                if geomRenderState.has_attrib(TextureAttrib.get_class_type()):
                    textureAttr = geomRenderState.get_attrib(
                        TextureAttrib.get_class_type())
                    print(sep3 + 'TextureAttrib:')
                    # get some info of this TextureAttrib
                    # for each 'on' TextureStage inside this TextureAttrib
                    # (in render order)
                    for j in range(textureAttr.get_num_on_stages()):
                        textureStage = textureAttr.get_on_stage(j)
                        tsName = textureStage.get_name()
                        textureName = textureAttr.get_on_texture(
                            textureStage).get_fullpath()  # or get_filename()
                        print(sep4 + str(j) + 
                              '-th rendered TextureStage (' + tsName + '):')
                        print(sep5 + 'Texture applied: ' + str(textureName))
                        print(sep5 + 'Priority: ' + 
                              str(textureStage.get_priority()))
                        print(sep5 + 'Sort: ' + str(textureStage.get_sort()))
                        print(sep5 + 'Texcoord name: ' + 
                              textureStage.get_texcoord_name().get_name())
                
                # check if there is a ColorAttrib
                if geomRenderState.has_attrib(ColorAttrib.get_class_type()):
                    print(sep3 + 'ColorAttrib:')               
                    colorAttr = geomRenderState.get_attrib(
                        ColorAttrib.get_class_type())
                    print(sep4 + 'Color applied: ' + str(colorAttr.get_color()))

    def _setupMaterial(self, root, **kwargs):
        for par in kwargs:
            val = kwargs[par]
            root.set_shader_input('Material.' + par, [val])        

    def _toggleDirectionalLight(self, directional):
        if directional[0]:
            self.lightRoot.show()
            directional[0] = False
        else:
            self.lightRoot.hide()            
            directional[0] = True
        self.root.set_shader_input('directional', directional)

    def _setupMultiLigths(self, n, rotate=True, isSpotlight=False, **kwargs):
        if n <= 0:
            return
        # light and material parameters
        LightBulbModel = self.loader.load_model('smiley.egg')
        LightBulbModel.set_scale(0.05)
        self.LightBulbs = []
        r = 3.0
        h = 3.5
        dn = (2 * math.pi / n)
        # All computations in view (i.e. camera) space
        for i in range(n):
            self.LightBulbs.append(
                self.lightRoot.attach_new_node('LightBulb' + str(i)))
            LightBulbModel.copy_to(self.LightBulbs[i])
            angle = dn * i - (math.pi / 2.0)
            rproj = LVector2f(r * math.cos(angle), r * math.sin(angle))
            self.LightBulbs[i].set_pos(rproj.get_x(), rproj.get_y(), h)
            self.LightBulbs[i].look_at(
                self.render, -LVector3f(rproj.get_x(), rproj.get_y() * 0.8, 0))
            light = 'Light[' + str(i) + '].'
            for j, par in enumerate(kwargs):
                I0 = (i + j) % 3
                I1 = (i + j + 1) % 3
                I2 = (i + j + 2) % 3
                val = kwargs[par]
                if isinstance(val, LVecBase3f):
                    val = LVecBase3f(val[I0], val[I1], val[I2])
                self.root.set_shader_input(light + par, [val])
                if str(par) == 'L':
                    self.LightBulbs[i].set_color(LColorf(val, 1.0))
        if rotate:
            # rotate lights
            lightInterval = LerpFunc(self._updateLightsPos, 30.0, 0.0, 360.0,
                                     extraArgs=[isSpotlight])
            lightInterval.loop()
        else:
            self._updateLightsPos(0, isSpotlight)

    def _updateLightsPos(self, t, isSpotlight):
        for i, l in enumerate(self.LightBulbs):
            light = 'Light[' + str(i) + '].'
            posCam = l.get_pos(self.cam)
            self.root.set_shader_input(light + 'Position', posCam)
            if isSpotlight:
                # all spotlight have direction in front of themselves
                dirCam = self.cam.get_relative_vector(l, LVector3f.forward())
                self.root.set_shader_input(light + 'Direction', dirCam)
        self.lightRoot.set_h(t)
