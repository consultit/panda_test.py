'''
Created on May 2, 2018

@author: consultit
'''

from panda3d.core import load_prc_file_data, CollisionSphere, CollisionNode, NodePath,\
    CollisionTraverser, CollisionHandlerEvent, LPoint3f
from direct.showbase.ShowBase import ShowBase
import argparse
import textwrap
from direct.interval.LerpInterval import LerpPosInterval
from direct.interval.IntervalGlobal import Sequence

load_prc_file_data('', 'window-type none')
# load_prc_file_data('', 'win-size 1024 768')
# load_prc_file_data('', 'show-frame-rate-meter #t')
# load_prc_file_data('', 'sync-video #t')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This script performs tests panda3d collisions.
    '''))
    
    # set up arguments
    parser.add_argument('-d','--data-dir', type=str, action='append', help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()
    # do actions
    # set model dir(s)
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)
        
    app = ShowBase()
    
    # ref node path
    refNP = NodePath('refNP')
#     refNP.reparent_to(app.render)
    # models
    pos1 = LPoint3f(-10, 0, 0)
#     np1 = app.loader.load_model("ralph")
#     np1.set_pos(pos1)
#     np1.reparent_to(refNP)
    pos2 = LPoint3f(10, 0, 0)
#     np2 = app.loader.load_model("ralph")
#     np2.set_pos(pos2)
#     np2.reparent_to(refNP)
    # Collision Solids
    r = 2.5
    h = 2.5
    cs1 = CollisionSphere(0,0,0,r)
#     cn1 = np1.attach_new_node(CollisionNode('cn1'))
    cn1 = refNP.attach_new_node(CollisionNode('cn1'))
    cn1.set_pos(0, 0, h)
    cn1.node().add_solid(cs1)
    cn1.show()
    cs2 = CollisionSphere(0,0,0,r)
#     cn2 = np2.attach_new_node(CollisionNode('cn2'))
    cn2 = refNP.attach_new_node(CollisionNode('cn2'))
    cn2.set_pos(0, 0, h)
    cn2.node().add_solid(cs2)
    cn2.show()
    # events
    intoEv1 = cn1.get_name() + '-into-' + cn2.get_name()
#     againEv1 = cn1.get_name() + '-again-' + cn2.get_name()
    outEv1 = cn1.get_name() + '-out-' + cn2.get_name()
    intoEv2 = cn2.get_name() + '-into-' + cn1.get_name()
#     againEv2 = cn2.get_name() + '-again-' + cn1.get_name()
    outEv2 = cn2.get_name() + '-out-' + cn1.get_name()
    # CollisionHandlerEvent
    handler = CollisionHandlerEvent()
    handler.add_in_pattern('%fn-into-%in')
    handler.add_again_pattern('%fn-again-%in')
    handler.add_out_pattern('%fn-out-%in')
    
    def evCallback(entry, ev):
        print(entry)
        print(ev)
     
    app.accept(intoEv1, evCallback, [intoEv1])
#     app.accept(againEv1, evCallback, [againEv1])
    app.accept(outEv1, evCallback, [outEv1])
    app.accept(intoEv2, evCallback, [intoEv2])
#     app.accept(againEv2, evCallback, [againEv2])
    app.accept(outEv2, evCallback, [outEv2])
        
    # CollisionTraverser
    traverser = CollisionTraverser('traverser')
    traverser.add_collider(cn1, handler)
    traverser.add_collider(cn2, handler)
    
    def traverse(traverser, root, task):
        traverser.traverse(root)
        return task.cont
    
    app.task_mgr.add(traverse, "traverse", sort=10, extraArgs=[traverser, refNP], appendTask=True)
    traverser.showCollisions(refNP)
    
#     app.cTrav = traverser
#     app.cTrav.showCollisions(app.render)
    
    # move model
    lerpPosInt1 = LerpPosInterval(cn1, 5, pos2, startPos=None, 
                                 other=None, blendType='easeInOut', 
                                 bakeInStart=0, fluid=0, name='lerpPosInt')
    lerpPosInt2 = LerpPosInterval(cn1, 5, pos1, startPos=None, 
                                 other=None, blendType='easeInOut', 
                                 bakeInStart=0, fluid=0, name='lerpPosInt')
    i1 = Sequence(lerpPosInt1,lerpPosInt2)
    i1.loop()    

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 50.0, -2.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
